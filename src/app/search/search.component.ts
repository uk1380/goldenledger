import { Component, OnInit, ElementRef, EventEmitter } from '@angular/core';
import { CoinService } from '../coin.service';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { HttpModule } from '@angular/http';
import { resource } from 'selenium-webdriver/http';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  alldata: any;
  exchangeName:any;
  csvfile: any;
  jsonfile: any;
  errorMessage:any;
  hasError = false;
  finishMessage:any;
  flag = false;
  constructor(private http: Http, private service: CoinService) { }
  ngOnInit() {

  }
  csvfileChanged(e) {
    console.log(e.target.files[0]);
    this.csvfile = e.target.files[0];
  }
  jsonfileChanged(e) {
    console.log(e.target.files[0]);
    this.jsonfile = e.target.files[0];
  }

  uploadJson(file) {
    let res;
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      console.log(fileReader.result);
      // this.csvJSON(fileReader.result);
    }
    fileReader.readAsText(this.jsonfile);
  }

  uploadDocument(file,exchangeName) {
    let res;
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      // console.log(fileReader.result);
      this.csvJSON(fileReader.result,exchangeName);
    }
    fileReader.readAsText(this.csvfile);
  }

  extractData(res: Response) {

    let csvData = res['_body'] || '';
    let allTextLines = csvData.split(/\r\n|\n/);
    let headers = allTextLines[0].split(',');
    let lines = [];

    for (let i = 0; i < allTextLines.length; i++) {
      // split content based on comma
      let data = allTextLines[i].split(',');
      if (data.length == headers.length) {
        let tarr = [];
        for (let j = 0; j < headers.length; j++) {
          tarr.push(data[j]);
        }
        lines.push(tarr);
      }
    }
  }

  handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return errMsg;
  }

  csvJSON(csv,exchangeName) {

    var lines = csv.split("\n");
    var result = [];
    var transferId = "transferId"
    var headers_1 = lines[0].split(",");
    headers_1.push(transferId);
    var headers = headers_1.map(header =>
      header = header.trim()
    );
    console.log(headers);
    for (var i = 1; i < lines.length; i++) {
      var obj = {};
      var currentline = lines[i].split(",");
      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }
      result.push(obj);
    }
    console.log(result);
    this.rebuild(result,exchangeName);

    //return result; //JavaScript object
    return JSON.stringify(result); //JSON
  }

  rebuild(array,exchangeName) {
    this.alldata = [];

    var i = 0;
    while (i < array.length) {
      let data = <any>{};
      console.log(i);
      //trade
      // console.log(array[i]);
      var m = i + 1;
      var n = i + 2;
      if (array[i]["action"] === "Buy Order" || array[i]["action"] === "Sell Order" && array[i]["action"] == array[m]["action"]) {
        if (array[n]["action"] === "Trading Fee") {
          console.log(array[i]);
          console.log(array[i]["referenceId"]);
          console.log(parseFloat(array[i]["referenceId"]));
          // if(array[i][" referenceId"] === array[m][" referenceId"] && array[m][" referenceId"] === array[n][" referenceId"] 
          // && array[i].creationTime === array[m].creationTime && array[m].creationTime === array[n].creationTime){ 
          var time = new Date(array[i].creationTime);
          var ts = time.getTime();
          data.date = ts;
          data.id = parseFloat(array[i]["referenceId"]);
          data.output_currency = array[i]["currency"].toLowerCase();
          data.input_currency = array[m]["currency"].toLowerCase();
          data.fee_currency = array[n]["currency"].toLowerCase();
          data.fee_amount = Math.abs(parseFloat(array[n]["amount"]));
          data.exchange = exchangeName;
          if (array[i]["action"] === "Buy Order") {
            data.side = "bid";
            data.output_amount = Math.abs(parseFloat(array[i]["amount"])) + data.fee_amount;
            data.input_amount = Math.abs(parseFloat(array[m]["amount"]));
          }
          if (array[i]["action"] === "Sell Order") {
            data.side = "ask";
            data.output_amount = Math.abs(parseFloat(array[i]["amount"]));
            data.input_amount = Math.abs(parseFloat(array[m]["amount"])) - data.fee_amount;
          }
          console.log(data);
          this.alldata.push(data);
          // }
          this.hasError = false;
          i = i + 3;
        }else{
          this.hasError = true;
          this.errorMessage = "The input csv file is incorrect";
          i = array.length;
        }
      } else if (array[i]["action"] === "Deposit") {
        console.log(array[i]);
        var time = new Date(array[i].creationTime);
        var ts = time.getTime();
        data.date = ts;
        data.id = parseFloat(array[i].transferId);
        data.side = "deposit";
        data.input_currency = array[i]["currency"].toLowerCase();
        data.input_amount = Math.abs(parseFloat(array[i]["amount"]));
        data.exchange = exchangeName;
        i = i + 1;
        this.alldata.push(data);
      } else if (array[i]["action"] === "Withdraw") {
        // console.log(array[i][" description"]);
        var time = new Date(array[i].creationTime);
        var ts = time.getTime();
        data.date = ts;
        data.id = parseFloat(array[i]["referenceId"]);
        data.side = "withdraw";
        data.output_currency = array[i]["currency"].toLowerCase();
        data.output_amount = Math.abs(parseFloat(array[i]["amount"]));
        data.exchange = exchangeName;
        console.log(parseFloat(array[i].description.split(':')["1"].replace(/\s/g, '')));
        data.txId = array[i].description.split(':')["1"].replace(/\s/g, '');
        console.log("txid" + data.txId);
        i = i + 1;
        this.alldata.push(data);
      } else {
        i = i + 1;
      }
    }
    console.log(this.alldata);
    while (this.flag === false) {
      this.flag = true;
      this.alldata = this.custom_compare(this.alldata);
    }
    this.alldata.sort(this.compare);

    // this.alldata.forEach(element => {
    //   this.service.addMany(element);
    // });

    if (this.hasError == false) {
      this.service.addMany(this.alldata);
      console.log("success")
      this.finishMessage = "success";
    }
    else {
      this.finishMessage = "error occured";
      console.log("data is too many")
    }
  }


  custom_compare(data) {
    for (let i = 0; i < data.length - 1; i++) {
      try {
        if (data[i].date >= data[i + 1].date) {
          this.flag = false;
          data[i + 1].date = data[i].date + 1;
        }
      } catch (err) {
        console.log(err + data[i].id);
      }
    }
    return data;
  }




  compare(a, b) {
    if (a.date < b.date)
      return -1;
    if (a.date > b.date)
      return 1;
    if (a.date == b.date) {
      if (a.id < b.id) {
        a.date -= 1;
        return -1
      } else {
        a.date += 1;
        return 1;
      }
    }
    return 0;
  }
}





