import { Routes } from '@angular/router';
import { ViewComponent } from './view/view.component';
import { AddComponent } from './add/add.component';
import { AlldataComponent } from './alldata/alldata.component';
import { ReportComponent } from './report/report.component';
import { SearchComponent } from './search/search.component';
import { SettingComponent } from './setting/setting.component';


export const appRoutes: Routes = [
    {
        path: 'view',
        component: ViewComponent
    },
    {
        path: 'add',
        component: AddComponent
    },
    {
        path: 'alldata',
        component: AlldataComponent
    },
    {
        path: 'report',
        component: ReportComponent
    },
    {
        path: 'search',
        component: SearchComponent
    },
    {
        path: 'setting',
        component: SettingComponent
    }
];
