import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(coins: any, term: any): any {
    //check if is unfined
    if (term === undefined) return coins;
    //return updated array
    return coins.filter(function(coin){
      console.log(coin);
      return coin.exchange.toLowerCase().includes(term.toLowerCase());
    })
  }
}
