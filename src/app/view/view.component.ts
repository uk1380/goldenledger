import { CoinService } from '../coin.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  depositCoins: any;
  withdrawCoins: any;
  coinList = [];
  depositCoinsTxid = [];
  withdrawCoinsTxid = [];
  widthdrawMatchedCoins: any;
  depositMatchedCoins: any;
  depositMatchedCoinsWithWithdraw: any;
  MatchedCoins: any;
  setasides: any;
  gifts:any;
  fees:any;
  timeDepositWithdraw: any;
  timeDuplicatie: any;
  constructor(private http: HttpClient, private service: CoinService) { }

  ngOnInit() {
    this.getCoins();
    this.getWithdrawCoins();
  }

  getAllMatchTxId() {
    this.depositCoinsTxid = [];
    this.withdrawCoinsTxid = [];
    for (let i in this.depositCoins) {
      if (this.depositCoins[i].hasOwnProperty('txId') &&
        this.depositCoins[i].txId !== null) {
        for (let k in this.withdrawCoins) {

          if (this.withdrawCoins[k].txId !== "" && this.withdrawCoins[k].txId === this.depositCoins[i].txId) {
            console.log("11111111111111");
            this.withdrawCoinsTxid.push(this.withdrawCoins[k]);
            this.depositCoinsTxid.push(this.depositCoins[i]);
          }
        }

      };
    }
    console.log(this.withdrawCoinsTxid);
    console.log(this.depositCoinsTxid);
  }


  getCoins() {
    const options = {
      side: 'deposit'
    };
    this.service.getCoin(options).subscribe(res => {
      console.log(res);
      this.depositCoins = res;
    });
  }

  getWithdrawCoins() {
    const options = {
      side: 'withdraw'
    };
    this.service.getCoin(options).subscribe(res => {
      console.log(res);
      this.withdrawCoins = res;
    });
  }

  gift(coin) {
    this.service.updateById({ '_id': coin._id }, { 'side': "gift" }, () => { this.getCoins(); this.getWithdrawCoins(); });
  }

  resetDeposit(coin){
    this.service.updateById({ '_id': coin._id }, { 'side': "deposit" }, () => { this.getCoins(); this.getWithdrawCoins();this.getGiftList(); });

  }

  resetWithdraw(coin){
    this.service.updateById({ '_id': coin._id }, { 'side': "withdraw" }, () => { this.getCoins(); this.getWithdrawCoins();this.getFeeList();this.getSetasideList(); });

  }

  fee(coin) {
    this.service.updateById({ '_id': coin._id }, { 'side': "fee" }, () => { this.getCoins(); this.getWithdrawCoins(); });
  }

  setaside(coin) {
    this.service.updateById({ '_id': coin._id }, { 'side': "setaside" }, () => { this.getCoins(); this.getWithdrawCoins(); });
  }

  impute(coin) {
    this.service.updateById({ '_id': coin._id }, { 'side': "impute" }, () => { this.getCoins(); this.getWithdrawCoins(); });
  }

  confirm(list, index) {
    var d = new Date();
    var nowTime = d.getTime();
    console.log(list);
    console.log('index: ' + index);
    console.log(this.withdrawCoins[index]);

    const withdrawCoin_fee_amount = this.withdrawCoins[index].output_amount - list.input_amount;
    const withdrawCoin_fee_currency = list.input_currency;

    this.service.updateById({ '_id': list._id }, { 'source': nowTime }, () => { this.getCoins(); });
    this.service.updateById({ '_id': this.withdrawCoins[index]._id },
      { 'source': nowTime, 'fee_amount': withdrawCoin_fee_amount, 'fee_currency': withdrawCoin_fee_currency },
      () => { this.getWithdrawCoins(); });
    // this.getCoins();
  }
  confirmMatch(list, index) {
    var d = new Date();
    var nowTime = d.getTime();
    console.log(list);
    console.log('index: ' + index);
    console.log(this.withdrawCoins[index]._id);

    const withdrawCoin_fee_amount = this.withdrawCoinsTxid[index].output_amount - list.input_amount;
    const withdrawCoin_fee_currency = list.input_currency;

    this.service.updateById({ '_id': list._id }, { 'source': nowTime }, () => { this.getCoins(); this.getAllMatchTxId(); });
    this.service.updateById({ '_id': this.withdrawCoinsTxid[index]._id },
      { 'source': nowTime, 'fee_amount': withdrawCoin_fee_amount, 'fee_currency': withdrawCoin_fee_currency },
      () => { this.getWithdrawCoins(); this.getAllMatchTxId(); });
    // this.getCoins();
  }

  showAllMatched() {
    this.widthdrawMatchedCoins = [];
    this.depositMatchedCoins = [];
    this.depositMatchedCoinsWithWithdraw = [];
    let depositList: any
    depositList = [];
    this.MatchedCoins = []
    let query = {
      "source": { "$exists": true }
    };
    this.service.searchCoin(query).subscribe(res => {
      console.log(res);
      this.MatchedCoins = res;
      for (var k in this.MatchedCoins) {
        if (this.MatchedCoins[k].side === "withdraw") {
          this.widthdrawMatchedCoins.push(this.MatchedCoins[k]);
        }
      }
      for (var k in this.MatchedCoins) {
        if (this.MatchedCoins[k].side === "deposit") {
          this.depositMatchedCoins.push(this.MatchedCoins[k]);
        }
      }
      for (var k in this.widthdrawMatchedCoins) {
        for (var i in this.depositMatchedCoins) {
          if (this.widthdrawMatchedCoins[k].source === this.depositMatchedCoins[i].source) {
            this.depositMatchedCoinsWithWithdraw.push(this.depositMatchedCoins[i]);
          }
        }
      }
      console.log(this.widthdrawMatchedCoins);
      console.log(this.depositMatchedCoins);
      console.log(this.depositMatchedCoinsWithWithdraw);
    });

  }
  //这边存在一个问题是，source 不匹配的 deposit 或者withdraw，就不会显示

  unMatch(list, index) {
    console.log(list);
    console.log('index: ' + index);
    // console.log(this.withdrawCoins[index]);

    let withdrawCoin_fee_amount = this.widthdrawMatchedCoins[index].output_amount - list.input_amount;
    let withdrawCoin_fee_currency = list.input_currency;
    this.service.unsetById({ '_id': list._id }, { '$unset': { "source": 1 } },
      () => { this.getCoins(); this.showAllMatched(); this.getAllMatchTxId() });
    this.service.unsetById({ '_id': this.depositMatchedCoinsWithWithdraw[index]._id },
      { '$unset': { "source": 1, "fee_amount": 1, "fee_currency": 1 } },
      () => { this.getWithdrawCoins(); this.getAllMatchTxId() });

  }
  getSetasideList() {
    this.setasides = [];
    let query = {
      "side": "setaside"
    };
    this.service.searchCoin(query).subscribe(res => {
      console.log(res);
      this.setasides = res;
    });
  }
  getGiftList() {
    this.gifts = [];
    let query = {
      "side": "gift"
    };
    this.service.searchCoin(query).subscribe(res => {
      console.log(res);
      this.gifts = res;
    });
  }
  getFeeList() {
    this.gifts = [];
    let query = {
      "side": "fee"
    };
    this.service.searchCoin(query).subscribe(res => {
      console.log(res);
      this.fees = res;
    });
  }
  dataverify() {
    this.service.dataverify().subscribe(res => {
      console.log(res);
      this.timeDepositWithdraw = res;
    })
  }
  duplicateTime() {
    this.service.duplicateTime().subscribe(res => {
      console.log(res);
      this.timeDuplicatie = res;
    })
  }


}

