import { Component, OnInit } from '@angular/core';
import { CoinService } from '../coin.service';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { last } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  polclick = false;
  bsclick = true;
  balances:any;
  poldatas:any;
  rglsum = [];
  filteredpoldatas = [];
  summarys = [];
  inputDate:any;
  lastoneBalance:any;
  poloss = {
    fromdate:"",
    todate:""
  };
  constructor(private service:CoinService) { }

  ngOnInit() {
    this.getAllBalance();
    this.setInputDate();
    this.getallpol();
  }
  setInputDate() {
    let today = new Date();
    this.inputDate = today.getTime() /1000 | 0;
    console.log(this.inputDate);
  }

  pol(){
    this.polclick = true;
    this.bsclick = false;
  }
  bs(){
    this.bsclick = true;
    this.polclick = false;
  }

  getAllBalance(){
    this.service.getAllBalance().subscribe(res => {
      console.log(res);
      this.balances = res;
    })
  }
  getallpol(){
    this.service.getAllCoins().subscribe(res => {
      this.poldatas = res;
      console.log(this.poldatas);
    });

  }

  lastBalance() {
    this.lastoneBalance = [];
    let lastDate = this.balances[0].date;
    
    for (let k in this.balances){
      console.log(this.balances[k]);
      if (this.balances[k].date === lastDate){
        console.log(this.balances[k]);
        this.lastoneBalance.push(this.balances[k])
      }
    }
  }

  inputComfirm(inputDate){
    this.lastoneBalance = [];
    console.log("ddddddddddddd" + inputDate);
    let fromtime = new Date(inputDate).getTime();
    console.log(fromtime);
    let totime = fromtime - 24*60*60*1000;
    console.log(totime);
    for (let k in this.balances){
      console.log(this.balances[k]);
      if (this.balances[k].date > totime && this.balances[k].date <= fromtime){
        console.log(this.balances);
        this.lastoneBalance.push(this.balances[k]);
      }
      else {
        console.log("22222222222222222222");
      }
    };
    console.log("00000000000000000000000000000");
    console.log(this.lastoneBalance);
  }

  exportBalance(){
    let time  = new Date();
    let reportName = time.getTime() + "balance_report";
    new Angular5Csv(this.balances,reportName);
  }

  exportpol(){
    let time  = new Date();
    let reportName = time.getTime() + "profit_or_loss_report";
    new Angular5Csv(this.filteredpoldatas,reportName);
  }

  exportpolsum(){
    let time  = new Date();
    let reportName = time.getTime() + "profit_or_loss_summary_report";
    new Angular5Csv(this.rglsum,reportName);
  }


  exportSummary(){
    let time  = new Date();
    let reportName = time.getTime() + "summary_report";
    new Angular5Csv(this.summarys,reportName);
  }

  balanceSummary(){
    let coinFilter = this.balances.map(balance =>balance.name)
    .filter((value, index, self) => self.indexOf(value) === index);
    console.log(coinFilter.length);

    console.log("111111111111111 " + this.balances[0].date);
    let lastDate = this.balances[0].date;
    let lastBalance = [];
    for (let k in this.balances){
      console.log(this.balances[k]);
      if (this.balances[k].date === lastDate){
        console.log(this.balances[k]);
        lastBalance.push(this.balances[k])
      }
    }
    console.log(lastBalance);

    let sumbalance = [];
    for (var i = 0; i < coinFilter.length;i++){
      let sumCoin = {}  
        let sumba = [];
        let sumbv = [];
        let summv = [];
        let balanceValue;
        let marketValue;


        lastBalance.forEach(function(balance){
          var ba;
          var bv;
          var mv;
          if(coinFilter[i] === balance.name && balance.balance_amount){
          console.log(balance.market + " " + balance.name + " " + balance.balance_amount);
            ba = balance.balance_amount;
            bv = balance.balance_value;
            mv = balance.market_value;
          }
          if (ba !== undefined && bv !== undefined && mv !==undefined){
          console.log("bbbbbbbbb" + ba);
          sumba.push(ba); 
          sumbv.push(bv); 
          summv.push(mv); 
          sumba = sumba.filter(function(n){ return n != undefined }); 
          sumbv = sumbv.filter(function(n){ return n != undefined }); 
          summv = summv.filter(function(n){ return n != undefined }); 
          // 
          }
      });
      console.log(sumba.reduce((a, b) => a + b, 0));
      console.log(sumbv.reduce((a, b) => a + b, 0));
      console.log(summv.reduce((a, b) => a + b, 0));

      sumCoin["amount"] = sumba.reduce((a, b) => a + b, 0);
      sumCoin["balance_value"] = sumbv.reduce((a, b) => a + b, 0);
      sumCoin["market_value"] = summv.reduce((a, b) => a + b, 0);
      sumCoin["name"] = coinFilter[i];
      sumCoin["unrealized_gain_or_loss"] = summv.reduce((a, b) => a + b, 0) - sumbv.reduce((a, b) => a + b, 0);
      console.log(coinFilter[i]);
      console.log(sumCoin);

      sumbalance.push(sumCoin);
      this.summarys = sumbalance;
      console.log(this.summarys);
  };
  }

  polfilter(poloss){
    console.log(poloss.fromdate);
    console.log(poloss.todate);
    // let ftime = new Date(poloss.fromdate).getTime();
    // let ttime = new Date(poloss.todate).getTime();
    let ftime = 0;
    let ttime = 1530748800000;
    console.log(ftime);
    console.log(ttime);
    let polossdata = [];
    for (let k in this.poldatas){
      if (this.poldatas[k].date >= ftime && this.poldatas[k].date <= ttime){
        if (this.poldatas[k].realize_gain_loss !== undefined){
        console.log(this.poldatas[k].realize_gain_loss);
        polossdata.push(this.poldatas[k]);
        }
      }
    }
    console.log(polossdata);
    this.filteredpoldatas = polossdata;
    console.log(this.filteredpoldatas);
    
    let polFilter = this.filteredpoldatas.map(balance =>balance.name)
    .filter((value, index, self) => self.indexOf(value) === index);

  }
  polsum(){
    let polFilterCurrency = this.filteredpoldatas.map(filteredpoldata =>filteredpoldata.output_currency)
    .filter((value, index, self) => self.indexOf(value) === index);
    console.log(polFilterCurrency);

    let polFilterExchange = this.filteredpoldatas.map(filteredpoldata =>filteredpoldata.exchange)
    .filter((value, index, self) => self.indexOf(value) === index);
    console.log(polFilterExchange);
    let a = 0;
    
    for(var i =0; i< polFilterCurrency.length; i++){
      for(var n = 0; n < polFilterExchange.length;n++){
          let filteredsum = this.filteredpoldatas.filter(item =>
            item.output_currency === polFilterCurrency[i]
            && item.exchange === polFilterExchange[n]);
            console.log(filteredsum);
            let a = 0;
            let rgldata = {};
            for (var k in filteredsum){
              filteredsum[k].realize_gain_loss
              a  = a + filteredsum[k].realize_gain_loss


            }
            rgldata["realized_gain_loss"] = a;
            rgldata["currency"] = polFilterCurrency[i];
            rgldata["exchange"] = polFilterExchange[n];
            console.log(rgldata);
            console.log(a);
            this.rglsum.push(rgldata);
      };
    };
    console.log(this.rglsum);
  }




}
