import { Component, OnInit } from '@angular/core';
import { CoinService } from '../coin.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  cancelAddRecord = true;
  exchanges = [
    {value:1, name:"acx"},
    {value:2, name:"btcmarket"},
    {value:4, name:"huobiau_1"},
    {value:5, name:"huobiau_2"},
    {value:6, name:"huobiau_3"},
    {value:7, name:"huobipro"},
    {value:8, name:"conversion"},
    {value:9, name:"otc"},
    {value:10,name:"ir"},
    {value:11,name:"intersection"}
  ];  
  sides = [
    {value:1, name:"withdraw"},
    {value:2, name:"deposit"},
    {value:3, name:"ask"},
    {value:4, name:"bid"},
    {value:5, name:"gift"},
    {value:6, name:"impute"},
    {value:7, name:"setaside"},
    {value:8, name:"fee"}
  ]; 
  data = {
    id: '',
    date: '',
    price: '',
    exchange: 'acx',
    side: 'bid',
    address: '',
    txId: '',
    status: '',
    input_amount: '',
    input_currency: '',
    input_balance_amount: '',
    input_balance_value: '',
    output_amount: '',
    output_currency: '',
    output_balance_amount: '',
    output_balance_value: '',
    fee_amount: '',
    fee_currency: '',
    source: ''
  };

  depositFiatData = {
    id: '',
    date: '',
    // price: '',
    exchange: '',
    side: 'DEPOSIT',
    // cryptoPaymentDetail: {
    //   address: '',
    //   txId: ''
    // },
    status: '',
    input_amount: '',
    input_currency: '',
    input_balance_amount: '',
    input_balance_value: '',
    // output_amount: '',
    // output_currency: '',
    fee_amount: '',
    fee_currency: '',
    // source: ''
  };

  depositCrypto = {
    id: '',
    date: '',
    price: '',
    exchange: '',
    side: 'DEPOSIT',
    cryptoPaymentDetail: {
      address: '',
      txId: ''
    },
    status: '',
    input_amount: '',
    input_currency: '',
    input_balance_amount: '',
    input_balance_value: '',
    // output_amount: '',
    // output_currency: '',
    fee_amount: '',
    fee_currency: '',
    source: ''
  };

  withdrawCrypto = {
    id: '',
    date: '',
    price: '',
    exchange: '',
    side: 'WITHDRAW',
    address: '',
    txId: '',
    status: '',
    // input_amount: '',
    // input_currency: '',
    output_amount: '',
    output_currency: '',
    output_balance_amount: '',
    output_balance_value: '',
    fee_amount: '',
    fee_currency: '',
    source: ''
  };

  submitted = false;

  constructor(private http: HttpClient, private service: CoinService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.data);
    this.add(this.data);
    alert("successful added!");
  }

  add(data) {
    console.log(data.date);
    data.date = new Date(data.date).getTime();
    console.log(data.date);
    var datakeys = Object.getOwnPropertyNames(data);
    for (var i = 0;i < datakeys.length;i++){
      var datakey = datakeys[i];
      if(data[datakey] === ""){
        delete data[datakey];
      }
    }

    console.log(data);
    this.service.add(data);
  }
  
  cancellAdd() {
    this.cancelAddRecord = false;
  }

}
