import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
// const ura = 'http://localhost:8081';
const ura = 'https://accback.goldenalpha.com.au';
@Injectable()
export class CoinService {

  constructor(private http: HttpClient) { }

  getCoin(obj) {
    // console.log(obj);
    const uri = ura + '/test1.htm';
    const options = { params: obj, responseType: 'json' as 'json'};
    return this
            .http
            .get(uri, options)
            .map(res => {
              return res;
            });
  }

  getAllCoins() {
    const uri = ura + '/test.htm';
    return this
            .http
            .get(uri)
            .map(res => {
              return res;
            });
  }

  getDbName() {
    const uri = ura + '/getDbName.htm';
    return this.http.get(uri).map(res => {
      return res;
    }); 
  }

  calculation(fromts,tots,callback){
    const uri = ura + '/calculation.htm';
    const param = {
      'fromTime':fromts,
      'toTime': tots
    }
    this.http.post(uri,param).subscribe(res =>{
      console.log(res);
      callback(res);
    }) 
  }

  report(fromts,tots,callback){
    const uri = ura + '/generateReport.htm';
    const param = {
      'fromTime':fromts,
      'toTime': tots
    }
    this.http.post(uri,param).subscribe(res =>{
      console.log(res);
      callback(res);
    }) 
  }

  chooseDatabase(database,callback) {
    const uri = ura + '/dta.htm';
    const param = {
      'database':database,
    }
    console.log(uri,param);
    // return this.http.post(uri,param); 
    this.http.post(uri, param).subscribe(res => {
      console.log(res);
      callback(res);
    });
  }

  getAllBalance() {
    const uri = ura + '/listtestcollection.htm';
    return this.http.get(uri).map(res => {
              return res;
            });
  }
  getLastReportDate() {
    const uri = ura + '/getLastReportDate.htm';
    return this.http.get(uri).map(res => {
              return res;
            });
  }

  dataverify() {
    const uri = ura + '/dataverify.htm';
    return this.http.get(uri).map(res => {
              return res;
            });
  }
  duplicateTime() {
    const uri = ura + '/duplicateTime.htm';
    return this.http.get(uri).map(res => {
              return res;
            });
  }


  searchBalance(obj) {
    const uri = ura + '/searchbalance.htm';
    const param = {
      'obj':obj,
    }
    console.log(this.http.post(uri,param));
        return this.http.post(uri,param);
  }

  deleteById(query) {
    const uri = ura + '/deleteById.htm';
    const param = {
      'query':query,
    }
    console.log(this.http.post(uri,param));
        return this.http.post(uri,param);
  }
  updateCoin(obj) {
    const uri = ura + '/updateOne.htm';
    const param = {
      'obj':obj,
    }
    console.log(this.http.post(uri,param));
        return this.http.post(uri,param);
  }

  searchCoin(obj) {
    const uri = ura + '/searchCoin.htm';
    const param = {
      'obj':obj,
    }
    console.log(this.http.post(uri,param));
        return this.http.post(uri,param);
  }


  updateById(query, obj, callback) {
    const uri = ura + '/updateById.htm';
    const param = {
      'query': query,
      'obj': obj
    };
    // const options = { params: param, responseType: 'json' as 'json'};
    this.http.post(uri, param).subscribe(res => {
      console.log(res);
      callback();
    });
  }

  unsetById(query, obj, callback) {
    const uri = ura + '/unsetById.htm';
    const param = {
      'query': query,
      'obj': obj
    };
    // const options = { params: param, responseType: 'json' as 'json'};
    this.http.post(uri, param).subscribe(res => {
      console.log(res);
      callback();
    });
  }

  add(data) {
    const uri = ura + '/addNewRecord.htm';
    const param = {
      'obj': data
    };
    this.http.post(uri, param).subscribe(res => {
      console.log(res);
    });
  }
  addMany(data) {
    const uri = ura + '/addManyRecord.htm';
    // const param = {
    //   'obj': data
    // };
    const param = data;
    this.http.post(uri, param).subscribe(res => {
      console.log(res);
    });
  }

  getCurrencyList(colume_name) {
    const uri = ura + '/getCoinList.htm';
    const options = { params: colume_name, responseType: 'json' as 'json'};
    return this.http.get(uri, options);
  }
}
