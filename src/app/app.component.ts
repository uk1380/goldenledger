import { Component } from '@angular/core';
import { CoinService } from './coin.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Welcome to crypto world';
  selectedDatabase: any;
  databases = [
    {value:1, name:"testone"},
    {value:2, name:"acx_liquidity"}
  ]; 
  database:any;
  constructor(private http: HttpClient, private service: CoinService) { }


  ngOnInit() {
    this.getDatabaseName();
  }

  chooseDatabase(database){
    this.selectedDatabase = database;
    
    this.service.chooseDatabase(database,res => {
      console.log(res);
      // this.selectedDatabase = res;
    });
    // this.ngOnInit();
  }

  getDatabaseName(){
    this.service.getDbName().subscribe(res => {
      console.log(res["name"]);
      this.selectedDatabase = res["name"];
      console.log(this.selectedDatabase);
    })

  }


}
