import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppComponent } from './app.component';
import { ViewComponent } from './view/view.component';

import { RouterModule } from '@angular/router';
import { appRoutes } from './routerConfig';
import {HttpClientModule} from '@angular/common/http';
import { CoinService } from './coin.service';

import { SortablejsModule } from 'angular-sortablejs';
import { AddComponent } from './add/add.component';
import { FormsModule } from '@angular/forms';
import { PriceInputComponent } from './price-input/price-input.component';
import { AlldataComponent } from './alldata/alldata.component';
import { SearchComponent } from './search/search.component';
import { FilterPipe } from './filter.pipe';
import { ReportComponent } from './report/report.component';
import { SettingComponent } from './setting/setting.component';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    AddComponent,
    PriceInputComponent,
    AlldataComponent,
    SearchComponent,
    FilterPipe,
    ReportComponent,
    SettingComponent
  ],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes,{useHash:true}),
    HttpClientModule,
    SortablejsModule.forRoot({
      animation: 200,
    }),
    FormsModule,
    HttpModule

  ],
  providers: [CoinService],
  bootstrap: [AppComponent]
})
export class AppModule { }
