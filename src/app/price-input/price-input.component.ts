import { Component, OnInit } from '@angular/core';
import { CoinService } from '../coin.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-price-input',
  templateUrl: './price-input.component.html',
  styleUrls: ['./price-input.component.css']
})
export class PriceInputComponent implements OnInit {

  coinList = [];
  coins = {};
  constructor(private http: HttpClient, private service: CoinService) { }
  imputeList:any;
  selected:any;
  conv = false;
  ngOnInit() {
  }

  getImputeList(){
    let obj = {"side" :"impute"};
    console.log(obj);
    this.service.searchCoin(obj).subscribe(res => {
      console.log(res);
      this.imputeList = res;
    });
  }
  resetDeposit(coin){
    this.service.updateById({ '_id': coin._id }, { 'side': "deposit" }, () => { this.getImputeList();});
  }

  edit(impute){
    this.selected = impute;
  }

  update(balance){
    console.log(balance);
    this.service.updateCoin(balance).subscribe(res =>{
      console.log("success");
    });

  }

  getCurrencyList () {
    this.service.getCurrencyList({query : 'input_currency'}).subscribe( res => {
      const temp = res;
      for (const each of Object.keys(res)) {
        // console.log(this.coinList.includes(res[each]));
        if (this.coinList === undefined ||
           !this.coinList.includes(res[each]) && res[each].toLowerCase() !== 'aud' && res[each].toLowerCase() !== '') {
          console.log(res[each]);
          this.coinList.push(res[each]);
        }
      }
      // this.coinList = res;
    });

    this.service.getCurrencyList({query : 'output_currency'}).subscribe( res => {
      const temp = res;
      // console.log(typeof(res));
      // console.log(Object.keys(res));
      for (const each of Object.keys(res)) {
        // console.log(this.coinList.includes(res[each]));
        if (this.coinList === undefined ||
           !this.coinList.includes(res[each]) && res[each].toLowerCase() !== 'aud' && res[each].toLowerCase() !== '') {
          console.log(res[each]);
          this.coinList.push(res[each]);
        }
      }
      // this.coinList = res;
    });
    console.log(this.coinList);
    console.log(this.coins);
  }
}
