import { Component, OnInit } from '@angular/core';
import { CoinService } from '../coin.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  selectedDatabase: any;
  databases = [
    { value: 1, name: "testone" },
    { value: 2, name: "acx_liquidity" }
  ];
  lastDate: any;
  status = "loading...";
  isclick = false;
  isreportclick = false;
  fromTime: any;
  toTime: any;
  ft: any;
  tt: any;
  lastReportDate: any;
  lastReportTs = 1531965600000;//19072018 12:00
  reportstatus = "loading...";
  constructor(private http: HttpClient, private service: CoinService) { }


  ngOnInit() {
    this.getLastReportDate();
  }

  chooseDatabase(database) {
    this.selectedDatabase = database;

    this.service.chooseDatabase(database, () => console.log);
  }

  calculation(fromTime, toTime) {
    this.isclick = true;
    let fts = new Date(fromTime).getTime();
    let tts = new Date(toTime).getTime()
    console.log(fts);
    console.log(tts);
    this.status = "loading...";
    this.service.calculation(fts, tts, res => {
      console.log(res.status);
      this.status = res.status;
    })
  }
  report(ft, tt) {
    this.getLastReportDate();
    this.isreportclick = true;
    console.log(ft);
    ft = new Date(ft).getTime();
    tt = new Date(tt).getTime();
    console.log(ft);
    console.log(tt);
    console.log(this.lastReportTs);
    if (ft <= this.lastReportTs + 24 * 60 * 60 * 1000 && ft <= tt) {
      this.service.report(ft, tt, res => {
        this.reportstatus = res.status;
      })
    } else {
      this.reportstatus = "please enter valid dates";
    }
  }
  getLastReportDate() {
    this.service.getLastReportDate().subscribe(res => {
      console.log(res);
      if (!this.isEmpty(res)){
        console.log("res has data");
        this.lastReportTs = res["0"].date;
        let time = new Date(res["0"].date);

        this.lastReportDate = time;
      }

    })
  }

  isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}
}
