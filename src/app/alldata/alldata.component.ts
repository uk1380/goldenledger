import { CoinService } from '../coin.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { FilterPipe } from '../filter.pipe';
import { ChangeDetectionStrategy, Input } from "@angular/core";


@Component({
  selector: 'app-alldata',
  templateUrl: './alldata.component.html',
  styleUrls: ['./alldata.component.css'],
})
export class AlldataComponent implements OnInit {
  // @Input('data') coins:any;
  // page: number = 1;
  // collection = [];
  p1: any;
  p2:any;
  coins: any;
  noselectCoin = true;
  selectCoin = false;
  selectedCoin: any;
  selectedCoinRecord: any;
  addNewRecord = false;
  ifSearch = false;
  datas: any;

  inputData = {
    id: '',
    from_date: '',
    to_date: '',
    exchange: '',
    output_amount_minimum: '',
    output_amount_maximum: '',
    output_currency: '',
    input_amount_minimum: '',
    input_amount_maximum: '',
    input_currency: '',
    side: ''
  };
  data = {
    _id:'',
    id: '',
    date: '',
    price: '',
    exchange: '',
    address: '',
    txId: '',
    status: '',
    input_amount: '',
    input_currency: '',
    input_balance_amount: '',
    input_balance_value: '',
    output_amount: '',
    output_currency: '',
    output_balance_amount: '',
    output_balance_value: '',
    fee_amount: '',
    fee_currency: '',
    source: ''
  };
  constructor(private service: CoinService) {
    // for (let i = 1; i <= 10; i++) {
    //   this.collection.push(`coin ${i}`);
    // }
  }

  ngOnInit() {
    this.getAllCoins();

  }

  exportBalance() {
    let time = new Date();
    let reportName = time.getTime() + "search_result";
    var options = {
      showTitle: true,
      showLabels: true,
      headers: ["id", "date", "input_currency",
        "input_amount", "output_currency", "output_amount",
        "fee_currency", "fee_amount", "exchange",
        "fee_value", "price", "source", "txId","side", "output_balance_amount",
        "output_balance_amount_left", "output_balance_value", "output_balance_value_left",
        "input_balance_amount", "input_balance_amount_left", "input_balance_value", "input_balance_value_left"]
    };
    new Angular5Csv(this.datas, reportName, options);
  }

  getAllCoins() {
    this.service.getAllCoins().subscribe(res => {
      console.log(res);
      this.coins = res;
    })
  }

  deleteCoin(id) {
    console.log(id);
    this.service.deleteById(id).subscribe(res => {
      console.log('Deleted');
      this.getAllCoins();
      alert("successful deleted!");
    });
  }
  edit(coin) {
    this.selectedCoin = coin;
    // this.noselectCoin = false;
    // this.selectCoin = true;

  }
  confirm() {
    this.noselectCoin = true;
    this.selectCoin = false;
  }
  updateCoin(data) {
    console.log(data._id);
    console.log(data.date);
    data.date = new Date(data.date).getTime();
    console.log(data.date);
    this.service.updateCoin(data).subscribe(res => {
      console.log('updated');
      this.getAllCoins();
    });
  }
  cancelUpdate() {
    this.getAllCoins();
  }

  addNew(coin) {
    this.addNewRecord = true;
    this.selectedCoinRecord = coin;
    console.log(coin);
  }

  searchData(inputData) {
    this.datas = [];
    this.ifSearch = true;

    console.log(inputData);
    let testdata = {};
    let output_amount = {};
    let input_amount = {};
    let date = {};
    Object.keys(inputData).forEach(k => {
      if (inputData[k] != "") {
        console.log(k);

        if (k == "from_date") {
          let fromDate = new Date(inputData.from_date).getTime();
          date['$gt'] = fromDate;
        }
        if (k == "to_date") {
          let toDate = new Date(inputData.to_date).getTime();
          date['$lt'] = toDate;
        }
        testdata['date'] = date;



        if (k == "output_amount_minimum" && inputData.output_amount_minimum != null) {
          output_amount['$gte'] = inputData.output_amount_minimum;
        }
        if (k == "output_amount_maximum" && inputData.output_amount_maximum != null) {
          output_amount['$lte'] = inputData.output_amount_maximum;
        }
        console.log(output_amount);
        testdata['output_amount'] = output_amount;

        if (k == "input_amount_minimum" && inputData.input_amount_minimum != null) {
          input_amount['$gte'] = inputData.input_amount_minimum;
        }
        if (k == "input_amount_maximum" && inputData.input_amount_maximum != null) {
          input_amount['$lte'] = inputData.input_amount_maximum;
        }
        testdata['input_amount'] = input_amount;
        if (testdata['input_amount'].equals) {
          console.log("testing11111111111111");
        }
        if (k != "output_amount_minimum" && k != "output_amount_maximum"
          && k != "input_amount_minimum" && k != "input_amount_maximum" && k != "from_date"
          && k != "to_date")
          testdata[k] = inputData[k];

      }
    });
    console.log(output_amount);
    for (var k in testdata) {
      if (Object.keys(testdata[k]).length === 0) {
        console.log(k);
        delete testdata[k]; // The object had no properties, so delete that property
      }
      console.log(testdata);
    }
    this.service.searchCoin(testdata).subscribe(res => {
      console.log(res);
      var array = this.json2array(res);
      console.log(array);
      array.forEach(element => {
        // console.log(element);
        let filterdata = {};
        filterdata['_id'] = element._id;
        filterdata['id'] = element.id;
        filterdata['date'] = element.date;
        filterdata['input_currency'] = element.input_currency;
        filterdata['input_amount'] = element.input_amount;
        filterdata['output_currency'] = element.output_currency;
        filterdata['output_amount'] = element.output_amount;
        filterdata['fee_currency'] = element.fee_currency;
        filterdata['fee_amount'] = element.fee_amount;
        filterdata['exchange'] = element.exchange;
        filterdata['fee_value'] = element.fee_value;
        filterdata['price'] = element.price;
        filterdata['source'] = element.source;
        filterdata['txId'] = element.txId;
        filterdata['side'] = element.side;
        filterdata['output_balance_amount'] = element.output_balance_amount;
        filterdata['output_balance_amount_left'] = element.output_balance_amount_left;
        filterdata['output_balance_value'] = element.output_balance_value;
        filterdata['output_balance_value_left'] = element.output_balance_value_left;
        filterdata['input_balance_amount'] = element.input_balance_amount;
        filterdata['input_balance_amount_left'] = element.input_balance_amount_left;
        filterdata['input_balance_value'] = element.input_balance_value;
        filterdata['input_balance_value_left'] = element.input_balance_value_left;// console.log(filterdata);
        this.datas.push(filterdata);
      });
      console.log(this.datas);
      // this.datas = res;
    });
  }

  json2array(json) {
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function (key) {
      result.push(json[key]);
    });
    return result;
  }

}
