const config = require('config');
var ObjectId = require('mongodb').ObjectID;
let baseCurrency = 'aud';
// let database_name = config.mongodb.database_name;
let database_name = config.mongodb.database_name;
// let database_name = config.mongodb.database_liquidity;

let config_database = "config_database";

var MongoClient = require('mongodb').MongoClient;
var url = config.mongodb.url;
let priceDateList = [];
let priceResult = [];
// launch();
var lastDate;

// getLastCalDate();

// updateDate();

// async function launch() {
//     await insertManyMongodb(data).then(datas => {
//         console.log(datas);
//     });
// }
class data_calculation {
    constructor(dbo, collection,fromtime,totime) {
        this.database_name = collection;
        this.dbo = dbo;
        this.fromtime = fromtime;
        this.totime = totime;
    }

    async get_cal() {
        return new Promise((resolve, reject) => {
            this.getAllPrice().then(async (data) => {
                // console.log(data);
                this.processs().then(() =>{
                    resolve("done 1");
                });
            });
            // resolve("calculation done");
        })
    }

    async processs() {
        let allDb;
        let self = this;
        // MongoClient.connect(url, async function (err, db) {
        //     if (err) throw err;
        // self.dbo = db.db(config.mongodb.db);
        // await getLastCalDate().then(async data => {
        //     lastDate = data;
        // });
        await self.getAllDb().then(async (data) => {
            allDb = data.sort(self.compare);
            // console.log(allDb);
            for (let each of allDb) {
                await self.calculate(each).then(res => {
                    console.log(res);
                });
                console.log('done 1');
            }

        });
        // updateDate();
        // db.close();
        // });
    }

    updateDate() {
        let self = this;
        return new Promise((resolve, reject) => {
            console.log("last date" + lastDate);
            if (isNaN(lastDate)) {
                process.exit()
            }
            var myquery = { "id": self.database_name };
            var set = { "$set": { "lastDate": lastDate, "database": self.database_name } }

            self.dbo.collection(config_database).update(myquery, set, function (err, res) {
                if (err) throw err;

            });
            // console.log(res);
            resolve();
        })

    }

    compare(a, b) {
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
        return 0;
    }

    insertManyMongodb(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            console.log("thrid steps");

            self.dbo.collection(self.database_name).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                resolve('done 1 time');
            });
        });
    }

    updateById(query, set) {
        let self = this;
        console.log('set', set.$set);
        if (typeof set.$set.output_balance_value_left !== "undefined") {
            if (isNaN(set.$set.output_balance_value_left)) {
                console.log('failed', set);
                // process.exit();
            } else {
                console.log('passed', set.$set.output_balance_value_left)
            }
        } else {
            console.log('value', set.$set.output_balance_value_left)
        }

        if (set.$set.input_balance_value_left) {
            if (isNaN(set.$set.input_balance_value_left)) {
                console.log('failed', set);
                // process.exit();
            } else {
                console.log('passed', set.$set.input_balance_value_left)
            }
        } else {
            console.log('value', set.input_balance_value_left)
        }

        return new Promise((resolve, reject) => {
            self.dbo.collection(this.database_name).updateOne({
                '_id': new ObjectId(query)
            }, set, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
                resolve(res);
            });
        })
    }
    getBalance(exchangeName, coin, time) {
        return new Promise((resolve, reject) => {
            console.log("second step");
            let query1_result;
            let balance_amount = null;
            let balance_value = null;
            let query1 = {
                exchange: exchangeName,
                date: {
                    $lt: time
                },
                $or: [{
                    input_currency: coin
                }, {
                    output_currency: coin
                }]
            };

            this.dbo.collection(this.database_name).find(query1).limit(1).sort({
                date: -1
            }).toArray(function (err, result) {
                if (err) throw err;
                query1_result = result;
                console.log(result);

                if (query1_result.length == 0) {
                    console.log('0000000');
                } else {

                    if (query1_result[0].input_currency == coin) {
                        // last_balance_value = query1_result[0].input_balance_value;
                        balance_amount = query1_result[0].input_balance_amount_left;
                        balance_value = query1_result[0].input_balance_value_left;
                    } else if (query1_result[0].output_currency == coin) {
                        // last_balance_value = query1_result[0].output_balance_value;
                        balance_amount = query1_result[0].output_balance_amount_left;
                        balance_value = query1_result[0].output_balance_value_left;
                    }

                }

                if (isNaN(balance_value) || isNaN(balance_amount)) {
                    console.log("balance value at get balance " + balance_value);
                    console.log(balance_amount);
                }

                balance_amount == null ? balance_amount = 0 : balance_amount;
                balance_value == null ? balance_value = 0 : balance_value;
                // balance_amount = balance_amount || 0;

                resolve({
                    'balance_amount': balance_amount,
                    'balance_value': balance_value
                });
            });

        });
    }

    getDataById(time) {
        return new Promise((resolve, reject) => {
            this.dbo.collection(this.database_name).find({ 'source': time, "side": "withdraw" }).toArray(function (err, result) {
                if (err) throw err;
                resolve(result);
            })
        })
    }

    getAllDb() {
        let self = this;
        return new Promise((resolve, reject) => {
            this.dbo.collection(this.database_name).find({ "date": {"$gte": self.fromtime, "$lte":self.totime}}).toArray(function (err, result) {
                if (err) throw err;
                console.log(result);
                resolve(result);
            })
        })
    }

    calculate(data) {
        let self = this;
        console.log('date', data.id);
        return new Promise((resolve, reject) => {
            console.log(data.date);
            if (data.date === 1532918522000) {
                console.log('stop');
            }
            lastDate = data.date;

            switch (data.side.toLowerCase()) {
                case 'deposit':
                    // console.log(data.side);
                    self.deposit(data).then(res => {
                        resolve(res);
                        // console.log(res);
                    });

                    break;
                case 'withdrawal':
                case 'withdraw':
                    self.withdraw(data).then(res => {
                        resolve(res);
                    })
                    // console.log(data.side);
                    // resolve(data.side);
                    break;
                case 'lost':
                    self.withdraw(data).then(res => {
                        resolve(res);
                    })
                    break;
                case 'trade':
                case 'bid':
                case 'ask':
                    self.trade(data).then(res => {
                        resolve(data.side);
                    })
                    // console.log(data.side);
                    break;
                case 'gift':
                    self.gift(data).then(res => {
                        resolve(res);
                    })
                    break;
                case 'impute':
                    self.impute(data).then(res => {
                        resolve(res);
                    })
                    break;
                case 'fee':
                    self.fee(data).then(res => {
                        resolve(res);
                    })
                    break;
                case 'setaside':
                    self.withdraw(data).then(res => {
                        resolve(res);
                    })
                    break;
                default:
                    console.log('unknow case ' + data.side);
                    resolve(data.side);
            }
        })
    }



    async deposit(data) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            // console.log(data.date);
            if (data.input_currency == baseCurrency) {
                console.log(data._id);

                let input_temp;
                await self.getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                    console.log("deposit base currency");
                    input_temp = balancedata;
                });

                let param = {
                    '$set': {
                        input_balance_amount: data.input_amount,
                        input_balance_value: data.input_amount,
                        input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                        input_balance_value_left: data.input_amount + input_temp.balance_value,
                    }
                }

                param.$set.fee_value = data.fee_amount;

                await self.updateById(data._id, param).then(res => {
                    console.log('final step');
                });
                resolve('success');
            } else {
                let withdrawData;
                if (data.source) {

                    await self.getDataById(data.source).then(res => {
                        withdrawData = res;
                    });
                    let input_temp;
                    await self.getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                        console.log("deposit non base currency");
                        input_temp = balancedata;
                    });

                    let param = {
                        '$set': {
                            input_balance_amount: data.input_amount,
                            input_balance_value: withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.input_amount,
                            input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                            input_balance_value_left: withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.input_amount + input_temp.balance_value,
                        }
                    }

                    param.$set.fee_value = withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.fee_amount;

                    await self.updateById(data._id, param).then(res => {
                        console.log('final step');
                    });
                    resolve('success');
                } else {

                    let input_temp;
                    await self.getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                        console.log("deposit non base currency");
                        input_temp = balancedata;
                    });

                    let param = {
                        '$set': {
                            input_balance_amount: data.input_amount,
                            input_balance_value: 0,
                            input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                            input_balance_value_left: input_temp.balance_value,
                        }
                    }

                    // param.$set.fee_value = withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.fee_amount;

                    await self.updateById(data._id, param).then(res => {
                        console.log('final step');
                    });
                    resolve('not match');
                }
            }
        })
    }

    async withdraw(data) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            let output_temp;

            await self.getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
                console.log("withdraw");
                output_temp = balancedata;
            });

            let param = {
                '$set': {
                    output_balance_amount: data.output_amount,
                    output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                    output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                    output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                }
            }

            param.$set.fee_value = output_temp.balance_value / output_temp.balance_amount * data.fee_amount;

            await self.updateById(data._id, param).then(res => {
                console.log('final step');
            });
            resolve('success');
        })
    }

    trade(data) {
        let self = this;
        let marketprice;
        return new Promise(async (resolve, reject) => {
            priceDateList = [];
            priceResult.forEach(element => {
                priceDateList.push(element.date);
            });
            console.log(priceDateList);
            let marketpriceDate = self.getLatestDate(priceDateList, data.date);
            console.log("marketpricedate " + marketpriceDate);
            if (data.output_currency === baseCurrency) {
                marketprice = 1;
            } else if (data.output_currency === "btc") {
                marketprice = priceResult.find(x => x.date === marketpriceDate && x.name === data.output_currency).price
            } else {
                marketprice = priceResult.find(x => x.date === marketpriceDate && x.name === data.output_currency).price *
                    priceResult.find(x => x.date === marketpriceDate && x.name === "btc").price;
            }
            console.log("marketprice " + marketprice);
            // await getAllPrice().then(async (result) =>{
            //     priceDateList = [];
            //     result.forEach(element => {
            //         priceDateList.push(element.date);
            //     });
            //     console.log(priceDateList);
            //     marketpriceDate = getLatestDate(priceDateList,data.date);
            //     console.log("marketprice " + marketpriceDate);
            //     if(data.output_currency === baseCurrency){
            //         marketprice = 1;
            //     }else{
            //         marketprice = result.find(x => x.date === marketpriceDate && x.name === data.output_currency).price;
            //     }
            //     console.log("marketprice " + marketprice);
            //     });
            // marketprice = 1;

            let input_temp, output_temp;
            await self.getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                console.log("trade get previous input");
                input_temp = balancedata;
            });

            await self.getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
                console.log("trade get previous output");
                output_temp = balancedata;
            });
            let param;
            // if(data.fee_currency == data.input_currency || data.fee_currency == data.output_currency ){//could be eliminated
            if (data.input_currency == baseCurrency) {
                console.log("market price is in aud " + marketprice);
                param = {
                    '$set': {
                        input_balance_amount: data.input_amount,
                        input_balance_value: data.input_amount,
                        input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                        input_balance_value_left: input_temp.balance_value + data.input_amount,

                        output_balance_amount: data.output_amount,
                        output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                        output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                        output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,

                        realize_gain_loss: data.input_amount - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                    }
                }
            } else {// decide whether outputcurrency equals base currency, if so than go as it is else add realized gain/loss
                if (data.fee_currency == data.output_currency) {
                    console.log("market price for alt is " + marketprice);//fee and output currency 可以直接加减
                    param = {
                        '$set': {
                            input_balance_amount: data.input_amount,
                            input_balance_value: (data.output_amount - data.fee_amount) * output_temp.balance_value / output_temp.balance_amount,
                            input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                            input_balance_value_left: input_temp.balance_value + (data.output_amount - data.fee_amount) * output_temp.balance_value / output_temp.balance_amount,

                            output_balance_amount: data.output_amount,
                            output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                            output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                            output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,

                            realize_gain_loss: data.output_amount * marketprice - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                        }
                    }
                } else {
                    console.log("market price for alt2 is " + marketprice);
                    param = {
                        '$set': {
                            input_balance_amount: data.input_amount,
                            input_balance_value: (data.output_amount) * output_temp.balance_value / output_temp.balance_amount * data.input_amount / (data.input_amount + data.fee_amount),
                            input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                            input_balance_value_left: input_temp.balance_value + (data.output_amount) * output_temp.balance_value / output_temp.balance_amount * data.input_amount / (data.input_amount + data.fee_amount),

                            output_balance_amount: data.output_amount,
                            output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                            output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                            output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                            realize_gain_loss: data.output_amount * marketprice - data.output_amount * output_temp.balance_value / output_temp.balance_amount,

                        }
                    }
                }
            }



            if (data.fee_currency) {
                // data.fee_amount? data.fee_amount: data.fee_amount = 0;
                if (data.fee_currency == data.input_currency) {
                    param.$set.fee_value = param.$set.input_balance_value / param.$set.input_balance_amount * data.fee_amount;
                } else if (data.fee_currency == data.output_currency) {
                    param.$set.fee_value = param.$set.output_balance_value / param.$set.output_balance_amount * data.fee_amount;
                } else {
                    let temp;
                    await self.getBalance(data.exchange, data.fee_currency, data.date).then(balancedata => {
                        temp = balancedata;
                    })
                    param.$set.fee_value = temp.balance_value / temp.balance_amount * data.fee_amount;
                }

                if (param.$set.fee_value == null) {
                    console.log('stop');
                }
            }




            await self.updateById(data._id, param).then(res => {
                console.log('final step');
            });
            resolve('success');
            // }else {

            //     let input_temp, output_temp;
            //     await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
            //         console.log("third step");
            //         input_temp = balancedata;
            //     });

            //     await getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
            //         console.log("third step");
            //         output_temp = balancedata;
            //     });

            //     let param = {
            //         '$set': {
            //             input_balance_amount: data.input_amount,
            //             input_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
            //             input_balance_amount_left: data.input_amount + input_temp.balance_amount,
            //             input_balance_value_left: input_temp.balance_value + data.output_amount * output_temp.balance_value / output_temp.balance_amount,

            //             output_balance_amount: data.output_amount,
            //             output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
            //             output_balance_amount_left: output_temp.balance_amount - data.output_amount,
            //             output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
            //         }
            //     }

            //     await updateById(data._id, param).then(res => {
            //         console.log('final step');
            //     });
            //     resolve('success');
            // }
        })
    }

    gift(data) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            let input_temp;
            await self.getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                input_temp = balancedata;
            });
            let param = {
                '$set': {
                    input_balance_amount: data.input_amount,
                    input_balance_value: 0,
                    input_balance_amount_left: input_temp.balance_amount + data.input_amount,
                    input_balance_value_left: input_temp.balance_value,
                }
            }
            await self.updateById(data._id, param).then(res => {
                console.log('final step');
            });
            resolve('success');
        })
    }

    impute(data) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            let input_temp;
            await self.getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                input_temp = balancedata;
            });
            let param = {
                '$set': {
                    input_balance_amount: data.input_amount,
                    input_balance_amount_left: input_temp.balance_amount + data.input_amount,
                    input_balance_value_left: input_temp.balance_value + data.input_balance_value,
                }
            }
            await self.updateById(data._id, param).then(res => {
                console.log('final step');
            });
            resolve('success');
        })
    }

    async fee(data) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            let output_temp;

            await self.getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
                console.log("fee");
                output_temp = balancedata;
            });

            let param = {
                '$set': {
                    output_balance_amount: data.output_amount,
                    output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                    output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                    output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                }
            }

            param.$set.fee_value = output_temp.balance_value / output_temp.balance_amount * data.fee_amount;

            await self.updateById(data._id, param).then(res => {
                console.log('final step');
            });
            resolve('success');
        })
    }
    async getLastCalDate() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.dbo.collection("config_database").find({ "id": this.database_name }).toArray(function (err, result) {
                if (err) throw err;
                result.forEach(element => {
                    element.lastDate;
                    console.log(element.lastDate);
                    resolve(element.lastDate);
                });
            });
        });
    }

    getLatestDate(a, b) {
        let result = Math.max.apply(Math, a.filter(function (x) { return x <= b }))
        console.log(result);
        return result;
    }

    getAllPrice() {
        return new Promise((resolve, reject) => {

            this.dbo.collection("price_list").find().toArray(function (err, result) {
                if (err) throw err;

                priceResult = result;
                result.forEach(element => {
                    priceDateList.push(element.date);

                });
                resolve(result);
            })
        })
        // })
        console.log(priceResult);
    }
}

module.exports = data_calculation;



