const AUHBSDK = require('./HuobiAuMarket/hbsdk');
const config = require('config');
var MongoClient = require('mongodb').MongoClient;
// const url = config.mongodb.url;
// const database_name = config.mongodb.test_database;
// let symbols = require('./HuobiAuMarket/au_symbols.json');
// var name = "huobiau_3";


class huobiau_get_data {
    constructor(dbo,database, exchange, symbols) {
        this.symbols = symbols;
        this.database_name = database;
        this.name = exchange;
        this.huobiau = config.huobi[exchange];
        this.hbdk = new AUHBSDK({
            access_key: this.huobiau.access_key,
            secretkey: this.huobiau.secretkey,
            account_id: this.huobiau.account_id_pro,
            url: this.huobiau.url
        });
        this.totalhistory = [];
        this.last_trade_id;
        this.last_deposit_id;
        this.last_withdraw_id;
        this.dbo = dbo;
    }
    async get_data() {
        let self = this;
            await self.get_last_id(self.name).then(data => {
                console.log(self.name);
                if (data != null) {
                    self.last_deposit_id = data.last_deposit_id;
                    self.last_withdraw_id = data.last_withdraw_id;
                    self.last_trade_id = data.last_trade_id;
                } else {
                    self.last_deposit_id = 0;
                    self.last_withdraw_id = 0;
                    self.last_trade_id = 0;
                    // return new Promise((resolve, reject) => {
                    //     // console.log("thrid steps");
                    //     MongoClient.connect(url, function (err, db) {
                    //         if (err) throw err;
                    //         var dbo = db.db(config.mongodb.db);
                    //         dbo.collection(config.mongodb.config_database).insert({
                    //             "exchange": self.name,
                    //             "last_deposit_id": 0,
                    //             "last_withdraw_id": 0,
                    //             "last_trade_id": 0
                    //         }, function (err, result) {
                    //             if (err) throw err;
                    //             // console.log(result.name);
                    //             db.close();

                    //             resolve(result);
                    //         });
                    //     });
                    // });
                }
            }).then(async () => {
                await self.get_deposit_History().then(async data => {
                    console.log(data);
                });
            }).then(async () => {
                await self.get_withdraw_History().then(async data => {
                    console.log(data);
                });
            }).then(async () => {
                await self.get_trade_history().then(async (data) => {
                    console.log(data);
                    self.totalhistory.sort(self.compare);
                    console.log('final data:' + self.totalhistory);
                    if (self.totalhistory.length > 0) {
                        await self.insertManyMongodb(self.totalhistory).then(console.log);
                        await self.updateById({
                            "exchange": self.huobiau.name
                        }, {
                                '$set': {
                                    "last_trade_id": self.last_trade_id,
                                    "last_withdraw_id": self.last_withdraw_id,
                                    "last_deposit_id": self.last_deposit_id
                                }
                            });
                    }
                });
            })
    }


    async get_trade_history() {

        return new Promise(async (resolve, reject) => {

            let new_last_id = this.last_trade_id;
            for (let i = 0; i < this.symbols.length; i++) {
                // p = p.then(async()=>{
                await this.get_history_until_null(this.symbols[i], new_last_id);
                // })
            }
            resolve('get history done');
        })

    }

    get_history_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {

            console.log('get ' + symbol.symbol + ' start from ' + id);
            let each_last_id = id;
            await this.hbdk.get_history(symbol.symbol, each_last_id + 1).then(async data => {
                if (data) {
                    if (data.length > 0) {
                        console.log(data);
                        data.forEach(obj => {
                            let new_Data = {};
                            if (each_last_id < obj.id) {
                                each_last_id = obj.id;
                            }

                            new_Data.id = obj.id;
                            new_Data.date = obj['created-at'];
                            new_Data.price = parseFloat(obj.price);
                            new_Data.exchange = this.huobiau.name;

                            if (obj.type == 'sell-limit' || obj.type == 'sell-market') {
                                new_Data.side = "ask";
                                new_Data.input_currency = symbol.currency;
                                new_Data.input_amount = parseFloat(obj['filled-amount']) * parseFloat(obj.price);
                                new_Data.output_currency = symbol.instrument;
                                new_Data.output_amount = obj['filled-amount'];

                            } else {
                                new_Data.side = "bid";
                                new_Data.input_currency = symbol.instrument;
                                new_Data.input_amount = parseFloat(obj['filled-amount']);
                                new_Data.output_currency = symbol.currency;
                                new_Data.output_amount = parseFloat(obj['filled-amount']) * parseFloat(obj.price);
                            }

                            new_Data.fee_currency = new_Data.input_currency;
                            new_Data.fee_amount = parseFloat(obj['filled-fees']);

                            this.totalhistory.push(new_Data);

                        });
                        await this.get_history_until_null(symbol, each_last_id);
                        resolve();
                    } else {
                        console.log(symbol.symbol + ' finished');
                        if (this.last_trade_id < each_last_id) {
                            this.last_trade_id = each_last_id;
                        }
                        resolve('done');
                    }
                }
            }).catch(async (err) => {
                console.log(err);
                console.log('re catch');
                await this.get_history_until_null(symbol, id).then(console.log);
                resolve('re catch done');
            })
        })

    }

    async get_deposit_History() {
        return new Promise(async (resolve, reject) => {
            let symbolList = [];
            this.symbols.forEach(symbol => {
                if (!symbolList.includes(symbol.instrument)) {
                    symbolList.push(symbol.instrument)
                }
                if (!symbolList.includes(symbol.currency)) {
                    symbolList.push(symbol.currency)
                }
            })
            let new_last_id = this.last_deposit_id;
            for (let i = 0; i < symbolList.length; i++) {
                // p = p.then(async()=>{
                await this.get_deposit_history_until_null(symbolList[i], new_last_id);
                // })
            }
            resolve('get deposit history done');
        })
    }

    get_deposit_history_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {
            console.log('get ' + symbol + ' deposit start from ' + id);
            let each_last_id = id;
            await this.hbdk.get_deposit_history(symbol, each_last_id + 1, 100).then(async data => {
                if (data) {
                    console.log(data);
                    if (data.length > 0) {
                        console.log(data);
                        data.forEach(obj => {
                            if (obj.currency === "bcc") {
                                obj.currency = "bch";
                            }
                            if (obj.id == '1318') {
                                console.log('stop');
                            }
                            let new_Data = {};
                            if (each_last_id < obj.id) {
                                each_last_id = obj.id;
                            }
                            new_Data.id = obj.id;
                            new_Data.date = obj['updated-at'];
                            new_Data.exchange = this.huobiau.name;
                            new_Data.side = obj.type;
                            new_Data.address = obj.address;
                            new_Data.txId = obj['tx-hash'];
                            // if (new_Data.side == 'deposit') {
                            new_Data.input_currency = obj.currency;
                            new_Data.input_amount = obj.amount;
                            // } else {
                            //     new_Data.output_currency = obj.currency;
                            //     new_Data.output_amount = obj.amount;
                            // }
                            new_Data.fee_currency = obj.currency;
                            new_Data.fee_amount = obj.fee;
                            new_Data.status = obj.state == 'safe' ? 'complete' : 'Not complete';


                            this.totalhistory.push(new_Data);

                        });
                        await this.get_deposit_history_until_null(symbol, each_last_id);
                        resolve();
                    } else {
                        console.log(symbol + ' finished');
                        if (this.last_deposit_id < each_last_id) {
                            this.last_deposit_id = each_last_id;
                        }
                        resolve('done');
                    }
                }
            }).catch(async () => {
                console.log('re catch');
                await this.get_deposit_history_until_null(symbol, id).then(console.log);
                resolve('re catch done');
            })
        })
    }


    async get_withdraw_History() {
        return new Promise(async (resolve, reject) => {
            let symbolList = [];
            this.symbols.forEach(symbol => {
                if (!symbolList.includes(symbol.instrument)) {
                    symbolList.push(symbol.instrument)
                }
                if (!symbolList.includes(symbol.currency)) {
                    symbolList.push(symbol.currency)
                }
            })
            let new_last_id = this.last_withdraw_id;
            for (let i = 0; i < symbolList.length; i++) {
                // p = p.then(async()=>{
                await this.get_withdraw_until_null(symbolList[i], new_last_id);
                // })
            }
            resolve('get withdraw history done');
        })


    }

    get_withdraw_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {

            console.log('get ' + symbol + ' withdraw start from ' + id);
            let each_last_id = id;
            await this.hbdk.get_withdraw_history(symbol, each_last_id + 1, 100).then(async data => {
                if (data) {
                    console.log(data);
                    if (data.length > 0) {
                        console.log(data);
                        data.forEach(obj => {
                            if (obj.currency === "bcc") {
                                obj.currency = "bch";
                            }
                            if (obj.id == '36') {
                                console.log('stop');
                            }
                            let new_Data = {};
                            if (each_last_id < obj.id) {
                                each_last_id = obj.id;
                            }
                            new_Data.id = obj.id;
                            new_Data.date = obj['updated-at'];
                            new_Data.exchange = this.huobiau.name;
                            new_Data.side = obj.type;
                            new_Data.address = obj.address;
                            new_Data.txId = obj['tx-hash'];
                            // if (new_Data.side == 'withdraw') {
                            new_Data.output_currency = obj.currency;
                            new_Data.output_amount = obj.amount + obj.fee;
                            // }
                            new_Data.fee_currency = obj.currency;
                            new_Data.fee_amount = obj.fee;
                            new_Data.status = obj.state == 'confirmed' ? 'complete' : 'Not complete';


                            this.totalhistory.push(new_Data);

                        });
                        await this.get_withdraw_until_null(symbol, each_last_id);
                        resolve();
                    } else {
                        console.log(symbol + ' finished');
                        if (this.last_withdraw_id < each_last_id) {
                            this.last_withdraw_id = each_last_id;
                        }
                        resolve('done');
                    }
                }
            }).catch(async () => {
                console.log('re catch');
                await this.get_withdraw_until_null(symbol, id).then(console.log);
                resolve('re catch done');
            })
        })
    }

    insertManyMongodb(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");

                this.dbo.collection(self.database_name).insertMany(data, function (err, res) {
                    if (err) throw err;
                    console.log("Number of documents inserted: " + res.insertedCount);
                    resolve('done 1 time');
                });
            });
    }

    get_last_id(exchange) {
        return new Promise((resolve, reject) => {
            console.log("thrid steps");
                this.dbo.collection(config.mongodb.config_database).findOne({
                    "exchange": exchange
                }, function (err, result) {
                    if (err) throw err;
                    // console.log(result.name);
                    resolve(result);
                });
            });
    }

    updateById(query, set) {
        console.log(query, set);
        return new Promise((resolve, reject) => {
                this.dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                    if (err) throw err;
                    console.log("1 document updated");
                    resolve(res);
                });
            });
    }


    compare(a, b) {
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
        if (a.date == b.date) {
            if (a.id < b.id) {
                a.date -= 1;
                return -1
            } else {
                a.date += 1;
                return 1;
            }
        }
        return 0;
    }
}
module.exports = huobiau_get_data;
