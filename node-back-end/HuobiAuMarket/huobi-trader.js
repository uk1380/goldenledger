const hbsdk = require('./hbsdk');
const database_name = 'test_database';
// 按注释的步骤逐步放开注释，运行程序，验证接口
async function run() {
    // 准备工作，填写config/default.json中的:
    // access_key & secretkey, www.huobi.com上申请
    // uid 登陆后看自己的UID
    // trade_password 可以先不填，提现时需要

    // 第一步，获取account_id_pro
    let symbols = require('./symbols.json');
    let fullhistory = [];
    let allPromise = [];
    let delay = 0;
    symbols.forEach(symbol => {
        delay += 2000;
        const test = new Promise(resolve => setTimeout(resolve, delay)).then(() => hbsdk.get_history(symbol.symbol, '2018-6-1').then(data => {

            if (data) {
                data.forEach(obj => {
                    let new_Data = {};
                    new_Data.id = obj.id;
                    new_Data.date = obj['created-at'];
                    new_Data.price = obj.price;
                    new_Data.exchange = 'huobiau';

                    if(obj.type == 'sell-limit' || obj.type == 'sell-market'){
                        new_Data.side = "ask";
                        new_Data.input_currency = symbol.currency;
                        new_Data.input_amount = obj['filled-amount'] * obj.price;
                        new_Data.output_currency = symbol.instrument;
                        new_Data.output_amount = obj['filled-amount'];

                    }else {
                        new_Data.side = "bid";
                        new_Data.input_currency = symbol.instrument;
                        new_Data.input_amount = obj['filled-amount'];
                        new_Data.output_currency = symbol.currency;
                        new_Data.output_amount = obj['filled-amount'] * obj.price;
                    }

                    new_Data.fee_currency = new_Data.input_currency;
                    new_Data.fee_amount = obj['filled-fees'];

                    fullhistory.push(new_Data);
                });
            }

        }));
        allPromise.push(test);
        // hbsdk.get_history(symbol,'2018-6-1');
    })

    return Promise.all(allPromise).then(async data => {
        console.log("data: ", fullhistory)
        await insertManyMongodb(fullhistory).then(console.log);
        return fullhistory;
    })


    // hbsdk.get_history('etcbtc','2018-6-1');
    // setTimeout(hbsdk.get_history('elfbtc','2018-6-1'),1500);


    // hbsdk.get_symbols().then(data=>{
    //     symbols = data;
    //     const promises = symbols.map(symbol=>{
    //      hbsdk.get_history(symbol,'2018-6-1');
    //     })
    //     await Promise.all(promises);
    //     console.log('done');
    //     });


}

function insertManyMongodb(data) {
    return new Promise((resolve, reject) => {
        // console.log("thrid steps");

        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("acxliquiditydev01");
            dbo.collection(database_name).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                db.close();
                resolve('done 1 time');
            });
        });
    });
}
run().then(data=>{
    getHistory();
});

async function getHistory(){
    let symbols = require('./symbols.json');
    let symbolList = [];
    symbols.forEach(symbol=>{
        if(!symbolList.includes(symbol.instrument)){
            symbolList.push(symbol.instrument)
        }
        if(!symbolList.includes(symbol.currency)){
            symbolList.push(symbol.currency)
        }
    })
    let fullhistory = [];
    let allPromise = [];
    let delay = 0;
    symbolList.forEach(symbol => {
        delay += 2000;
        const test = new Promise(resolve => setTimeout(resolve, delay)).then(() => hbsdk.get_deposit_history(symbol,'0',100).then(data => {

            if (data) {
                data.forEach(obj => {
                    let new_Data = {};
                    new_Data.id = obj.id;
                    new_Data.date = obj['updated-at'];
                    new_Data.exchange = 'huobiau';
                    new_Data.side = obj.type;
                    new_Data.address = obj.address;
                    new_Data.txId = obj['tx-hash'];
                    if(new_Data.side == 'deposit'){
                        new_Data.input_currency = obj.currency;
                        new_Data.input_amount = obj.amount;
                    }else {
                        new_Data.output_currency = obj.currency;
                        new_Data.output_amount = obj.amount;
                    }
                    new_Data.fee_currency = obj.currency;
                    new_Data.fee_amount = obj.fee;
                    new_Data.status = obj.state == 'safe'? 'complete' : 'Not complete';
                    fullhistory.push(new_Data);
                });
            }

        }));
        allPromise.push(test);
        // hbsdk.get_history(symbol,'2018-6-1');
    })

    return Promise.all(allPromise).then(async data => {
        console.log("data: ", fullhistory)
        await insertManyMongodb(fullhistory).then(console.log);
        return fullhistory;
    })
    
}

