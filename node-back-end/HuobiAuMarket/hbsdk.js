// var config = require('config');
var CryptoJS = require('crypto-js');
var Promise = require('bluebird');
var moment = require('moment');
var HmacSHA256 = require('crypto-js/hmac-sha256')
var http = require('./framework/httpClient');
var request = require("request");

const DEFAULT_HEADERS = {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36"
}

class HB_SDK {
    constructor({
        access_key = '',
        secretkey = '',
        uid = '',
        url = "api-au-test1.spancer.cn",
        trade_password = "replace_me",
        account_id = "replace_me",
        protocol = 'https'
    } = {}) {
        this.config = {
            huobiau: {
                access_key,
                secretkey,
                account_id,
                url,
                trade_password,
                protocol,
            }
        }

        //  this.config.huobiau.url = this.config.huobiau.url;

    }


    // const this.config.huobiau.url = 'http://api-au-test1.spancer.cn'
    // const this.config.huobiau.url = 'api.huobiau.pro'; //备用地址



    get_auth() {
        var sign = this.config.huobiau.trade_password + 'hello, moto';
        var md5 = CryptoJS.MD5(sign).toString().toLowerCase();
        let ret = encodeURIComponent(JSON.stringify({
            assetPwd: md5
        }));
        return ret;
    }

    sign_sha(method, baseurl, path, data) {
        var pars = [];
        for (let item in data) {
            pars.push(item + "=" + encodeURIComponent(data[item]));
        }
        var p = pars.sort().join("&");
        var meta = [method, baseurl, path, p].join('\n');
        // console.log(meta);
        var hash = HmacSHA256(meta, this.config.huobiau.secretkey);
        var Signature = encodeURIComponent(CryptoJS.enc.Base64.stringify(hash));
        // console.log(`Signature: ${Signature}`);
        p += `&Signature=${Signature}`;
        // console.log(p);
        return p;
    }

    get_body() {
        return {
            AccessKeyId: this.config.huobiau.access_key,
            SignatureMethod: "HmacSHA256",
            SignatureVersion: 2,
            Timestamp: moment.utc().format('YYYY-MM-DDTHH:mm:ss'),
        };
    }

    call_api(method, path, payload, body) {
        return new Promise((resolve, reject) => {
            var account_id = this.config.huobiau.account_id;
            var url = `https://${this.config.huobiau.url}${path}?${payload}`;
            // console.log (url);
            var headers = DEFAULT_HEADERS;
            headers.AuthData = this.get_auth();

            if (method == 'GET') {
                http.get(url, {
                    timeout: 1000,
                    headers: headers
                }).then(data => {
                    let json = JSON.parse(data);
                    if (json.status == 'ok') {
                        // console.log(json.data);
                        resolve(json.data);
                    } else {
                        console.log('调用错误', json);
                        reject(json);
                    }
                }).catch(async ex => {
                    console.log(method, path, '异常', ex);
                    // await this.call_api(method, path, payload, body);
                    // await setTimeout(async ){ await this.call_api(method, path, payload, body);},1000);
                    reject('something wrong');
                });
            } else if (method == 'POST') {
                http.post(url, body, {
                    timeout: 1000,
                    headers: headers
                }).then(data => {
                    let json = JSON.parse(data);
                    if (json.status == 'ok') {
                        console.log(json.data);
                        resolve(json.data);
                    } else {
                        console.log('调用错误', json);
                        reject(json);
                    }
                }).catch(async ex => {
                    console.log(method, path, '异常', ex);
                    // await this.call_api(method, path, payload, body);
                    // await setTimeout(async ){ await this.call_api(method, path, payload, body);},1000);
                    reject('something wrong');
                });
            }
        });
    }

    apiGet(path, source) {
        return new Promise((resolve, reject) => {
            let urls = "https://" + this.config.huobiau.url + path;
            console.log(urls);
            var options = {
                url: urls,
                json: true
            };
            request.get(options, (err, res, body) => {
                if (err) {
                    console.log(source + " Error: " + err.message);
                    reject(err);
                } else {
                    if (res.statusCode == 200) {
                        resolve(body);
                    } else {
                        reject(res.statusCode);
                    }
                }
            });
        });
    }

    get_symbols() {
        var path = `/v1/common/symbols`
        // var url = `https://${this.config.huobiau.url}${path}`;
        return new Promise((resolve, reject) => {
            apiGet(path, "initMarkets").then(data => {
                // console.log(data.data);
                let symbols = [];
                let symbolList = data.data;
                for (let symbol of symbolList) {
                    symbols.push(symbol.symbol);
                }
                resolve(symbols);
            });
        });
    }
    get_deposit_history(coin, from, size) {
        var path = `/v1/query/deposit-withdraw`;
        var body = this.get_body();
        body.currency = coin;
        body.type = 'deposit';
        body.from = from;
        body.size = size;
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    get_withdraw_history(coin, from, size) {
        var path = `/v1/query/deposit-withdraw`;
        var body = this.get_body();
        body.currency = coin;
        body.type = 'withdraw';
        body.from = from;
        body.size = size;
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    get_account() {
        var path = `/v1/query/deposit-withdraw`;
        var body = this.get_body();
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    get_balance() {
        var account_id = this.config.huobiau.account_id;
        var path = `/v1/account/accounts/${account_id}/balance`;
        var body = this.get_body();
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    get_open_orders(symbol) {
        var path = `/v1/order/orders`;
        var body = this.get_body();
        body.symbol = symbol;
        body.states = 'submitted,partial-filled';
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    get_history(symbol, from) {
        var path = `/v1/order/matchresults`;
        var body = this.get_body();
        body.symbol = symbol;
        body.states = 'submitted,partial-filled';
        //date.now - 61 days;
        body['start-date'] = '2018-07-1';
        body['from'] = from;
        body.direct = 'prev';
        body.size = '100';
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    get_order(order_id) {
        var path = `/v1/order/orders/${order_id}`;
        var body = this.get_body();
        var payload = this.sign_sha('GET', this.config.huobiau.url, path, body);
        return this.call_api('GET', path, payload, body);
    }
    buy_limit(symbol, amount, price) {
        var path = '/v1/order/orders/place';
        var body = this.get_body();
        var payload = this.sign_sha('POST', this.config.huobiau.url, path, body);

        body["account-id"] = this.config.huobiau.account_id;
        body.type = "buy-limit";
        body.amount = amount;
        body.symbol = symbol;
        body.price = price;

        return this.call_api('POST', path, payload, body);
    }
    sell_limit(symbol, amount, price) {
        var path = '/v1/order/orders/place';
        var body = this.get_body();
        var payload = this.sign_sha('POST', this.config.huobiau.url, path, body);

        body["account-id"] = this.config.huobiau.account_id;
        body.type = "sell-limit";
        body.amount = amount;
        body.symbol = symbol;
        body.price = price;

        return this.call_api('POST', path, payload, body);
    }
    withdrawal(address, coin, amount, payment_id) {
        var path = `/v1/dw/withdraw/api/create`;
        var body = this.get_body();
        var payload = this.sign_sha('POST', this.config.huobiau.url, path, body);

        body.address = address;
        body.amount = amount;
        body.currency = coin;
        if (coin.toLowerCase() == 'xrp') {
            if (payment_id) {
                body['addr-tag'] = payment_id;
            } else {
                console.log('huobiau withdrawal', coin, 'no payment id provided, cancel withdrawal');
                return Promise.resolve(null);
            }
        }

        return this.call_api('POST', path, payload, body);
    }
}


module.exports = HB_SDK;