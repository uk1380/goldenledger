const config = require('config');
var ObjectId = require('mongodb').ObjectID;
let baseCurrency = 'aud';
// let database_name = config.mongodb.database_name;
let database_name = config.mongodb.database_liquidity;


var MongoClient = require('mongodb').MongoClient;
var url = config.mongodb.url;

// let data = require('./data.json');
// console.log(data);
var dbo;
// launch();

processs();


async function launch() {
    // for(let obj of data){
    await insertManyMongodb(data).then(datas => {
        console.log(datas);
    });

    // }
    // data.forEach(async (obj) => {

    // )});
}

function processs() {
    let allDb;
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        getAllDb().then(async (data) => {
            allDb = data.sort(compare);
            // console.log(allDb);
            for (let each of allDb) {
                await calculate(each).then(res => {
                    console.log(res);
                });
                console.log('done 1');
            }
            // allDb.forEach(async (each) => {
            //     console.log('first step');
            //     calculate(each);
            // });
            db.close();
        })
    })
}

function compare(a, b) {
    if (a.date < b.date)
        return -1;
    if (a.date > b.date)
        return 1;
    return 0;
}

function insertManyMongodb(data) {
    return new Promise((resolve, reject) => {
        console.log("thrid steps");

        dbo.collection(database_name).insertMany(data, function (err, res) {
            if (err) throw err;
            console.log("Number of documents inserted: " + res.insertedCount);
            db.close();
            resolve('done 1 time');
        });
        // });
    });
}

function updateById(query, set) {
    console.log('set', set.$set);
    if(typeof set.$set.output_balance_value_left !== "undefined"){
        if(isNaN(set.$set.output_balance_value_left)){
            console.log('failed',set);
            process.exit();
        }else {
            console.log('passed', set.$set.output_balance_value_left)
        }
    }else{
        console.log('value',set.$set.output_balance_value_left)
    }

    if(set.$set.input_balance_value_left){
        if(isNaN(set.$set.input_balance_value_left)){
                console.log('failed',set);
                process.exit();
    }else {
        console.log('passed', set.$set.input_balance_value_left)
    }
}else{
    console.log('value',set.input_balance_value_left)
}

    return new Promise((resolve, reject) => {
        dbo.collection(database_name).updateOne({
            '_id': new ObjectId(query)
        }, set, function (err, res) {
            if (err) throw err;
            console.log("1 document updated");
            // db.close();
            resolve(res);
        });
        // });
    })
}

function getBalance(exchangeName, coin, time) {


    return new Promise((resolve, reject) => {

        console.log("second step");

        let query1_result;
        let balance_amount = null;
        let balance_value = null;

        let query1 = {
            exchange: exchangeName,
            date: {
                $lt: time
            },
            $or: [{
                input_currency: coin
            }, {
                output_currency: coin
            }]
        };


        dbo.collection(database_name).find(query1).limit(1).sort({
            date: -1
        }).toArray(function (err, result) {
            if (err) throw err;
            query1_result = result;
            console.log(result);
            // db.close();

            if (query1_result.length == 0) {
                console.log('0000000');
            } else {

                if (query1_result[0].input_currency == coin) {
                    // last_balance_value = query1_result[0].input_balance_value;
                    balance_amount = query1_result[0].input_balance_amount_left;
                    balance_value = query1_result[0].input_balance_value_left;
                } else if (query1_result[0].output_currency == coin) {
                    // last_balance_value = query1_result[0].output_balance_value;
                    balance_amount = query1_result[0].output_balance_amount_left;
                    balance_value = query1_result[0].output_balance_value_left;
                }

            }

            if (isNaN(balance_value) || isNaN(balance_amount)) {
                console.log(balance_value);
                console.log(balance_amount);
            }

            balance_amount == null ? balance_amount = 0 : balance_amount;
            balance_value == null ? balance_value = 0 : balance_value;
            // balance_amount = balance_amount || 0;

            resolve({
                'balance_amount': balance_amount,
                'balance_value': balance_value
            });
        });

        // });
    });
}

function getDataById(id) {
    return new Promise((resolve, reject) => {
        dbo.collection(database_name).find({
            '_id': new ObjectId(id)
        }).toArray(function (err, result) {
            if (err) throw err;
            // db.close();
            // console.log(result);
            resolve(result);
        })
        // })
    })
}

function getAllDb() {
    return new Promise((resolve, reject) => {
        dbo.collection(database_name).find().toArray(function (err, result) {
            if (err) throw err;
            // db.close();
            console.log(result);
            resolve(result);
        })
        // })
    })
}

function calculate(data) {
    console.log('date',data.id);
    return new Promise((resolve, reject) => {
        console.log(data.date);
        if(data.date === 1532918522000 ){
            console.log('stop');
        }
        switch (data.side.toLowerCase()) {
            case 'deposit':
                // console.log(data.side);
                deposit(data).then(res => {
                    resolve(res);
                    // console.log(res);
                });

                break;
            case 'withdrawal':
            case 'withdraw':
                withdraw(data).then(res => {
                    resolve(res);
                })
                // console.log(data.side);
                // resolve(data.side);
                break;
            case 'lost':
                withdraw(data).then(res => {
                    resolve(res);
                })
                break;
            case 'trade':
            case 'bid':
            case 'ask':
                trade(data).then(res => {
                    resolve(data.side);
                })
                // console.log(data.side);
                break;
            case 'gift':
                gift(data).then(res => {
                    resolve(res);
                })
                break;
            default:
                console.log('unknow case ' + data.side);
                resolve(data.side);

        }
    })
}

async function deposit(data) {
    return new Promise(async (resolve, reject) => {
        // console.log(data.date);
        if (data.input_currency == baseCurrency) {
            console.log(data._id);

            let input_temp;
            await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                console.log("deposit base currency");
                input_temp = balancedata;
            });

            let param = {
                '$set': {
                    input_balance_amount: data.input_amount,
                    input_balance_value: data.input_amount,
                    input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                    input_balance_value_left: data.input_amount + input_temp.balance_value,
                }
            }

            param.$set.fee_value = data.fee_amount;

            await updateById(data._id, param).then(res => {
                console.log('final step');
            });
            resolve('success');
        } else {
            let withdrawData;
            if (data.source) {

                await getDataById(data.source).then(res => {
                    withdrawData = res;
                });
                let input_temp;
                await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                    console.log("deposit non base currency");
                    input_temp = balancedata;
                });

                let param = {
                    '$set': {
                        input_balance_amount: data.input_amount,
                        input_balance_value: withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.input_amount,
                        input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                        input_balance_value_left: withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.input_amount + input_temp.balance_value,
                    }
                }

                param.$set.fee_value = withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.fee_amount;

                await updateById(data._id, param).then(res => {
                    console.log('final step');
                });
                resolve('success');
            }else{
                
                let input_temp;
                await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
                    console.log("deposit non base currency");
                    input_temp = balancedata;
                });

                let param = {
                    '$set': {
                        input_balance_amount: data.input_amount,
                        input_balance_value: 0,
                        input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                        input_balance_value_left: input_temp.balance_value,
                    }
                }

                // param.$set.fee_value = withdrawData[0].output_balance_value / withdrawData[0].output_balance_amount * data.fee_amount;

                await updateById(data._id, param).then(res => {
                    console.log('final step');
                });
                resolve('not match');
            }
        }
    })
}

async function withdraw(data) {
    return new Promise(async (resolve, reject) => {
        let output_temp;

        await getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
            console.log("withdraw");
            output_temp = balancedata;
        });

        let param = {
            '$set': {
                output_balance_amount: data.output_amount,
                output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
            }
        }

        param.$set.fee_value = output_temp.balance_value / output_temp.balance_amount * data.fee_amount;

        await updateById(data._id, param).then(res => {
            console.log('final step');
        });
        resolve('success');
    })
}

function trade(data) {
    return new Promise(async (resolve, reject) => {

        // if (data.input_currency == baseCurrency){
        let input_temp, output_temp;
        await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
            console.log("trade get previous input");
            input_temp = balancedata;
        });

        await getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
            console.log("trade get previous output");
            output_temp = balancedata;
        });
        let param;
        if (data.input_currency == baseCurrency) {
            param = {
                '$set': {
                    input_balance_amount: data.input_amount,
                    input_balance_value: data.input_amount,
                    input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                    input_balance_value_left: input_temp.balance_value + data.input_amount,

                    output_balance_amount: data.output_amount,
                    output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                    output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                    output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,

                    realize_gain_loss: data.input_amount - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                }
            }
        } else {
            if (data.fee_currency == data.output_currency) {//fee and output currency 可以直接加减
                param = {
                    '$set': {
                        input_balance_amount: data.input_amount,
                        input_balance_value: (data.output_amount - data.fee_amount) * output_temp.balance_value / output_temp.balance_amount,
                        input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                        input_balance_value_left: input_temp.balance_value + (data.output_amount - data.fee_amount) * output_temp.balance_value / output_temp.balance_amount,

                        output_balance_amount: data.output_amount,
                        output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                        output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                        output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                    }
                }
            } else {
                param = {
                    '$set': {
                        input_balance_amount: data.input_amount,
                        input_balance_value: (data.output_amount) * output_temp.balance_value / output_temp.balance_amount * data.input_amount / (data.input_amount + data.fee_amount),
                        input_balance_amount_left: data.input_amount + input_temp.balance_amount,
                        input_balance_value_left: input_temp.balance_value + (data.output_amount) * output_temp.balance_value / output_temp.balance_amount * data.input_amount / (data.input_amount + data.fee_amount),

                        output_balance_amount: data.output_amount,
                        output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                        output_balance_amount_left: output_temp.balance_amount - data.output_amount,
                        output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
                    }
                }
            }
        }

        if (data.fee_currency) {
            // data.fee_amount? data.fee_amount: data.fee_amount = 0;
            if (data.fee_currency == data.input_currency) {
                param.$set.fee_value = param.$set.input_balance_value / param.$set.input_balance_amount * data.fee_amount;
            } else if (data.fee_currency == data.output_currency) {
                param.$set.fee_value = param.$set.output_balance_value / param.$set.output_balance_amount * data.fee_amount;
            } else {
                let temp;
                await getBalance(data.exchange, data.fee_currency, data.date).then(balancedata => {
                    temp = balancedata;
                })
                param.$set.fee_value = temp.balance_value / temp.balance_amount * data.fee_amount;
            }

            if (param.$set.fee_value == null) {
                console.log('stop');
            }
        }




        await updateById(data._id, param).then(res => {
            console.log('final step');
        });
        resolve('success');
        // }else {

        //     let input_temp, output_temp;
        //     await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {
        //         console.log("third step");
        //         input_temp = balancedata;
        //     });

        //     await getBalance(data.exchange, data.output_currency, data.date).then(balancedata => {
        //         console.log("third step");
        //         output_temp = balancedata;
        //     });

        //     let param = {
        //         '$set': {
        //             input_balance_amount: data.input_amount,
        //             input_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
        //             input_balance_amount_left: data.input_amount + input_temp.balance_amount,
        //             input_balance_value_left: input_temp.balance_value + data.output_amount * output_temp.balance_value / output_temp.balance_amount,

        //             output_balance_amount: data.output_amount,
        //             output_balance_value: data.output_amount * output_temp.balance_value / output_temp.balance_amount,
        //             output_balance_amount_left: output_temp.balance_amount - data.output_amount,
        //             output_balance_value_left: output_temp.balance_value - data.output_amount * output_temp.balance_value / output_temp.balance_amount,
        //         }
        //     }

        //     await updateById(data._id, param).then(res => {
        //         console.log('final step');
        //     });
        //     resolve('success');
        // }
    })
}

function gift(data) {
    return new Promise(async (resolve, reject) => {
        let input_temp;

        await getBalance(data.exchange, data.input_currency, data.date).then(balancedata => {

            input_temp = balancedata;
        });

        let param = {
            '$set': {
                input_balance_amount: data.input_amount,
                input_balance_value: 0,
                input_balance_amount_left: input_temp.balance_amount + data.input_amount,
                input_balance_value_left: input_temp.balance_value,
            }
        }

        await updateById(data._id, param).then(res => {
            console.log('final step');
        });
        resolve('success');
    })
}