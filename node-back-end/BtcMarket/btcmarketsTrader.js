"use strict";

const btcmarkets = require('btc-markets');
const request = require('request-promise');
const SocketIO = require('socket.io-client');
const fs = require("fs");



class Trader{
    constructor({instrument = "BTC", currency = "AUD", key, secret, apiEndPoint = 'https://api.btcmarkets.net', timeout = 2000, tradeFee=0} = {}){
        this.instrument = instrument.toUpperCase();
        this.key = key;
        this.secret = secret;
        
        this.symboles = require('./symbols.json');
        this.markets = this.symboles.map(s => {
            return {
                name: {
                    symbol: (s.instrument + s.currency).toLowerCase(),
                    instrument: s.instrument.toLowerCase(),
                    currency: s.currency.toLowerCase()
                },
                market: null
            }
        });

        this.apiEndPoint = apiEndPoint;
        this.timeout = timeout;
        this.currency = currency.toUpperCase();
        this.market = this.instrument + this.currency;
        this.client = new btcmarkets.default(key, secret);
        this.numberConverter = btcmarkets.default.numberConverter;
        this.accountsRecord = [];
        
        // require('./btcmarketAccount.json');
        this.tradeFee = tradeFee;
    }

    getMyAccount() {
        let self = this;
        fs.readFile('./btcmarketAccount.json', (err, data) => {
            if(err) { console.log(err); }
            else{ 
                try{
                    let jsonPairs = JSON.parse(data); 
                    self.accountsRecord = jsonPairs;
                }
                catch(err){
                    self.accountsRecord = [];
                }
            }
        });
        return new Promise((resolve, reject) => {
            let now = new Date();
            if(!self.accountsRecord.timestamp || self.accountsRecord.timestamp < now.getTime()){
                now.setSeconds(now.getSeconds() + 2);
                self.accountsRecord.timestamp = now.getTime();
                fs.writeFile("./btcmarketAccount.json", JSON.stringify(self.accountsRecord), err => { if(err){ console.log(err); } });
                self.client.getAccountBalances().then((data) => {
                    self.accountsRecord.accounts = data.map( acc => {
                        return {
                            currency: acc.currency, 
                            balance: Number(acc.balance)/this.numberConverter, 
                            locked: Number(acc.pendingFunds)/this.numberConverter
                        };
                    });
                    fs.writeFile("./btcmarketAccount.json", JSON.stringify(self.accountsRecord), err => { if(err){ console.log(err); } });
                    resolve(self.accountsRecord.accounts);
                }, (error)=>{
                    now.setSeconds(now.getSeconds() - 2);
                    self.accountsRecord.timestamp = now.getTime();
                    reject(error);
                });
            }
            else{
                resolve(self.accountsRecord.accounts);
            }
        });
    }

    getOrders(){
        let self = this;
        return new Promise((resolve, reject) => {
            self.client.getOpenOrders(self.instrument.toUpperCase(), self.currency.toUpperCase()).then((data) => {
                resolve(data.orders);
            }, (error) => {
                reject(error);
            });
        });
    }

    getOrder(orderid) {
        let orderIds = [Number(orderid)];
        return new Promise((resolve, reject) => {
            this.client.getOrderDetail(orderIds).then((data) => {
                if(data.orders[0]){
                    data.orders[0].trades = data.orders[0].trades.map(trade => {
                        return {
                            id: trade.id,
                            creationTime: trade.creationTime,
                            description: trade.description,
                            price: trade.price/this.numberConverter,
                            volume: trade.volume/this.numberConverter,
                            side: trade.side.toLowerCase(),
                            fee: trade.fee/this.numberConverter,
                            orderId: trade.orderId
                        }
                    });
                    resolve(data.orders[0]);
                }else{
                    resolve(null);
                } 
            }, (error) => {
                reject(error);
            });
        });
    }

    getMyTrades(since = null, limit = 100, instrument = this.instrument, currency = this.currency){
        //let trades = [];
        return new Promise((resolve, reject) => {
            this.client.getTradeHistory(instrument, currency, limit, since).then((data) => {
                let trades = data.trades.map( tra => {
                    let isBuyer = null;
                    let feeCurrency = null;
                    if(tra.side == "Ask"){
                        isBuyer = false;
                        feeCurrency = currency.toLowerCase();
                    }else if(tra.side == "Bid"){
                        isBuyer = true;
                        if (currency == 'AUD'){
                            feeCurrency = currency.toLowerCase();
                        }else {
                            feeCurrency = instrument.toLowerCase();
                        }
                    }
                     // Format To be confirmed
                    return {
                        id: tra.id, 
                        price: tra.price/this.numberConverter, 
                        qty: tra.volume/this.numberConverter, 
                        time: tra.creationTime, 
                        orderId: tra.orderId, 
                        isBuyer:isBuyer,
                        fee: tra.fee/this.numberConverter,
                        feeCurrency: feeCurrency.toLowerCase(),
                        side: tra.side.toLowerCase(),
                        exchange: 'btcmarket'
                    }
                });
                //resolve(trades.sort((a,b)=>{return b.id - a.id; } ));
                resolve(trades.reverse());
            }, (error) => {
                reject("BTCMarkets GetMyTrades Error " + error.message);
            });
        });
    }
    
    getAllTrades(since = null){
        let allTrades = [];
        let history = [];
        let index = 0;
        let delay = 0;

        this.symboles.forEach(symbol => {
            delay += 1500;
            const test =  new Promise(resolve => setTimeout(resolve,delay)).then( ()=> this.getMyTrades(since, 200, symbol.instrument, symbol.currency).then(data=>{
                // console.log(data);
                console.log(index++);
                if (data.length){
                    data.forEach(element => {
                        element.instrument = symbol.instrument.toLowerCase();
                        element.currency = symbol.currency.toLowerCase();
                    });
                   
                    history.push(data);
                }
            }))
            allTrades.push(test);
            // this.getMyTrades(since, 200, symbol.instrument, symbol.currency).then(data=>{
            //     allTrades.push(data);
            // })
        })

        return Promise.all(allTrades).then(data=>{
            return history;
        })

        // return allTrades;
    }

    placeOrder({side, volume, price = null}){
        let orderType = 'Market';
        if(price && price > 0){
            orderType = 'Limit';
            price *= this.numberConverter;
        }
        else{
            price = null;
        }
        // volume = parseFloat((volume*this.numberConverter).toFixed(2));
        volume = volume * this.numberConverter;
        if(side.toLowerCase() == 'buy'){ side = 'Bid'; }
        if(side.toLowerCase() == 'sell'){ side = 'Ask'; }
        return new Promise((resolve, reject) => {
            this.client.createOrder(this.instrument, this.currency, price, volume, side, orderType, null ).then((data) => {
                resolve(data);
            }, (error) => {
                reject(error.message);
            })
        })
    }

    // not tested
    placeMarketOrders(orders, callback) {
        orders.forEach((order) => {
            setTimeout(() => {
                this.placeOrder(order);
            }, idx * 11);
        });
    }

    getFundTransferHistory(limit = 200, since = null,indexForward = false){
        return new Promise((resolve, reject) => {
            this.client.withdrawHistory(limit, since, indexForward).then((data) => {
                let new_data = data.fundTransfers.map ( obj => {
                    let list  = {};
                    list.time = obj.creationTime;
                    list.id = obj.fundTransferId;
                    list.side = obj.transferType.toLowerCase();
                    list.currency = obj.currency.toLowerCase();
                    list.amount = obj.amount / this.numberConverter;
                    list.cryptoPaymentDetail = obj.cryptoPaymentDetail;
                    list.status = obj.status.toLowerCase();
                    list.exchange = 'btcmarket';
                    list.fee = obj.fee;

                    return list;
                })
                resolve(new_data);
            }, (error) => {
                reject("BTCMarkets GetFundhistory Error " + error.message);
            });
        });
    }
}


module.exports = { Trader }