const BTCM = require('./BtcMarket/btcmarketsTrader');
const config = require('config');

let database_name = config.mongodb.database_name;

var MongoClient = require('mongodb').MongoClient;
var url = config.mongodb.url;




var btcMarketsTrader = new BTCM.Trader({
    key: config.btcmarket.key,
    secret: config.btcmarket.secret
});

var last_deposit_and_withdraw_id, last_trade_id;


async function btcmarket(id,btclist) {

    new Promise(async(resolve, reject) => {
        // let btclist = [];
        btcMarketsTrader.getAllTrades(id).then(data => {
            data.forEach(async (each) => {
                // for (const each of data){
                each.forEach(async (each_data) => {
                    // for (const each_data of each){
                    btclist.push(each_data);

                })
            })
            // console.log(data);
            resolve(btclist);
        })
    }).then(async (btclist) => {
        btclist.sort(compare);
        let fulllist = [];
        for (let list of btclist) {

            let btcresult = btcmarketRebuildData(list);
            fulllist.push(btcresult);

            // await insertMongodb(btcresult).then(data => {
            //     console.log(data);
            // });
            // console.log("test2");
        }
        await insertManyMongodb(fulllist).then(data => {
            console.log(data);
        });
        await updateById({
            "exchange": 'btcmarket'
        }, {
            '$set': {
                "last_trade_id": last_trade_id + 1
            }
        });
        await updateById({
            "exchange": 'btcmarket'
        }, {
            '$set': {
                "last_deposit_and_withdraw_id": last_deposit_and_withdraw_id + 1
            }
        });
        console.log('done');
    })

}

function compare(a, b) {
    if (a.time < b.time)
        return -1;
    if (a.time > b.time)
        return 1;
    return 0;
}



function insertManyMongodb(data) {
    return new Promise((resolve, reject) => {
        // console.log("thrid steps");

        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db(config.mongodb.db);
            dbo.collection(database_name).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                db.close();
                resolve('done 1 time');
            });
        });
    });
}

function btcmarketRebuildData(data) {

    console.log("first step");

    let new_data = {};
    let input_temp;
    let out_temp;
    new_data.id = data.id;
    new_data.date = data.time;
    new_data.price = data.price;
    new_data.exchange = data.exchange;
    new_data.fee_amount = data.fee;
    new_data.side = data.side;

    if (data.side == 'bid') {
        last_trade_id = (data.id > last_trade_id) ? data.id : last_trade_id;
        console.log(last_trade_id);
        new_data.input_currency = data.instrument;
        new_data.input_amount = data.qty;

        new_data.output_currency = data.currency;
        new_data.output_amount = data.price * data.qty;
        new_data.fee_currency = data.feeCurrency;


    } else if (data.side == 'ask') {
        last_trade_id = (data.id > last_trade_id) ? data.id : last_trade_id;
        console.log(last_trade_id);
        new_data.input_currency = data.currency;
        new_data.input_amount = data.price * data.qty;

        new_data.output_currency = data.instrument;
        new_data.output_amount = data.qty;
        new_data.fee_currency = data.feeCurrency;

    } else if (data.side == 'deposit') {
        last_deposit_and_withdraw_id = (data.id > last_deposit_and_withdraw_id) ? data.id : last_deposit_and_withdraw_id;
        
        new_data.address = data.cryptoPaymentDetail!= null? data.cryptoPaymentDetail.address:null;
        new_data.txId = data.cryptoPaymentDetail!=null? data.cryptoPaymentDetail.txId:null;
        new_data.status = data.status;
        new_data.input_currency = data.currency;
        new_data.input_amount = data.amount;
        new_data.fee_currency = data.currency;
    } else if (data.side == 'withdraw') {

        last_deposit_and_withdraw_id = (data.id > last_deposit_and_withdraw_id) ? data.id : last_deposit_and_withdraw_id;

        new_data.address = data.cryptoPaymentDetail!= null? data.cryptoPaymentDetail.address:null;
        new_data.txId = data.cryptoPaymentDetail!=null? data.cryptoPaymentDetail.txId:null;
        new_data.status = data.status;
        new_data.output_currency = data.currency;
        new_data.output_amount = data.amount;
        new_data.fee_currency = data.currency;
    }

    if (new_data.fee_currency == new_data.input_currency) {
        new_data.input_amount -= new_data.fee_amount;
    } else if (new_data.fee_currency == new_data.output_currency) {
        new_data.output_amount += new_data.fee_amount;
    }

    return new_data;
}

function updateById(query, set) {
    console.log(query, set);
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db(config.mongodb.db);
            dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
                db.close();
                resolve(res);
            });
        });
    })
}

function get_last_id(exchange) {
    return new Promise((resolve, reject) => {
        // console.log("thrid steps");

        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db(config.mongodb.db);
            dbo.collection(config.mongodb.config_database).findOne({
                "exchange": exchange
            }, function (err, result) {
                if (err) throw err;
                // console.log(result.name);
                db.close();

                resolve(result);
            });
        });
    });
}

async function launch(){
    var btclist = [];
    await get_last_id('btcmarket').then(data =>{
        if(data != null){
            last_deposit_and_withdraw_id = data.last_deposit_and_withdraw_id;
            last_trade_id = data.last_trade_id;
        } else {
            last_deposit_and_withdraw_id = 0;
            last_trade_id = 0;
        }
    })
    
    await btcMarketsTrader.getFundTransferHistory(last_deposit_and_withdraw_id).then(async data => {
        console.log(data);
        data.forEach(obj => {
            btclist.push(obj);
        });
        await btcmarket(last_trade_id,btclist);
    })
}

launch();
