
const config = require('config');
var fs = require('fs');
const url = config.mongodb.url;

const MongoClient = require('mongodb').MongoClient;


const database_name = "testone_balance";

function insertData(){
  fs.readFile('./database_backup/'+database_name+'.json', 'utf8', function (err, data) {
    if (err) throw err;
    // console.log(data);
    var json = JSON.parse(data);
    MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db(config.mongodb.db);
        dbo.collection(database_name).insert(json, function(err, doc) {
            // console.log(data);
        if(err) throw err;
        db.close();
        });
    });
})  
}

// function insertIndex(){
//     MongoClient.connect(url,{ useNewUrlParser: true }, function (err, db) {
//         if (err) throw err;
//         var dbo = db.db("mydb");
//         dbo.collection("testone").find({"exchange":"huobipro_huobi_1"}).toArray(function(err, res) {
//              console.log(res);
//             //  var chunks = splitArrayIntoChunks(res, 5000)
//             //  for (var i=0;i< 16;i++){
//                 res.forEach(element => {
//                 //  console.log(element.exchange);
//                 //  console.log(element.date);
//                  uid = element.exchange + "_" + element.date;
//                  console.log(uid);
//                  dbo.collection("testone").update({"_id":element._id},{"$set":{"uid":uid}});
//              });
//             // }
//             // console.log(data);
//         db.close();
//         });
//     })
// }

// function splitArrayIntoChunks(arr, chunkLen){
//     var chunkList = []
//     var chunkCount = Math.ceil(arr.length/chunkLen)
//     for(var i = 0; i < chunkCount; i++){
//         chunkList.push(arr.splice(0, chunkLen))
//     }
//     return chunkList
// }

// var testArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
// var chunks = splitArrayIntoChunks(testArray, 3)
// for(var i = 0;i < 4; i++){
//    console.log(chunks[i]); 
// }


// insertIndex();
insertData();
