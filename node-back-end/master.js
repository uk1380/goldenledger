
const acx_acx_liquidity = require('./acx_liquidity');
const huobiau_huobi_liquidity = require('./Huobiau-get-data');
const acx_huobi_liquidity = require('./acx');
const huobipro = require('./Huobipro-get-data');
const config = require('config');
const binance = require('./binance-get-data');
const bi_deposit_withdraw = require('./binance-with-depo')

const MongoClient = require('mongodb').MongoClient;
const url = config.mongodb.url;

let symbols_huobiau_huobiau = require('./HuobiAuMarket/au_symbols.json');
let symbols_huobipro_huobiau = require('./HuobiMarket/symbols.json');
let symbols_huobipro_acx = require('./huobipro/symbols.json');
let symbols_acx_acx = require('./Acx/symbols.json');
let symbols_acx_huobi = require('./Acx/symbols.json');
let symbols_biance_acx = require('./binance/symbol.json');
let symbol_fuel_binance_acx = require('./binance/symbols_2.json')

// 命名规则：交易所名字 + group名字 （ + function）
//acx liquidity 有 acx12 huobipro? binance? ir?
//huobiau liquidity(testone) 有huobipro?, huobiau123,acx?,


// acx_acx_exchanges = ["acx_acx_1", "acx_acx_2"];
// huobiau_huobiau_exchanges = ["huobiau_huobi_1", "huobiau_huobi_2", "huobiau_huobi_3"];
// huobipro_huobiau_exchanges = "huobipro_huobi_1";
// huobipro_acx_exchanges = "huobipro_acx_1";
// acx_huobi_exchanges = "acx_huobi_1";
// binance_acx_exchanges = "binance_acx_2";

var database = "testone";
var database_2 = "acx_liquidity";
// var testdata = "testdata"; // TEST
// var testdata2 = "testdata2" // TEST
//acx acx 
function get_all_data(dbo) {
    // acx - acx 1 2 
    let ACX_ACX1 = new Promise(async (resolve, reject) => {
        // for (let element of acx_acx_exchanges){
        var acx_acx_1 = new acx_acx_liquidity(dbo, database_2, "acx_acx_1");
        await acx_acx_1.get_data();
        console.log("master done ACX_ACX1");
        // }
        resolve("acx-acx 1");
    });

    let ACX_ACX2 = new Promise(async (resolve, reject) => {
        var acx_acx_2 = new acx_acx_liquidity(dbo, database_2, "acx_acx_2");
        await acx_acx_2.get_data();
        console.log("master done ACX_ACX2");
        resolve("acx-acx 2");
    });

    let huobiau_huobi_1 = new Promise(async (resolve, reject) => {
        var huobiau_huobi1 = new huobiau_huobi_liquidity(dbo, database, "huobiau_huobi_1", symbols_huobiau_huobiau);
        await huobiau_huobi1.get_data();
        console.log("master done huobiau_huobi_1");
        resolve("huobiau_huobi_1 ");
    });
    let huobiau_huobi_2 = new Promise(async (resolve, reject) => {
        var huobiau_huobi2 = new huobiau_huobi_liquidity(dbo, database, "huobiau_huobi_2", symbols_huobiau_huobiau);
        await huobiau_huobi2.get_data();
        console.log("master done a");
        resolve("huobiau_huobi_2 ");
    });
    let huobiau_huobi_3 = new Promise(async (resolve, reject) => {
        var huobiau_huobi3 = new huobiau_huobi_liquidity(dbo, database, "huobiau_huobi_3", symbols_huobiau_huobiau);
        await huobiau_huobi3.get_data();
        console.log("master done huobiau_huobi_3");
        resolve("huobiau_huobi_3");
    });


    // // huobiau huobi 
    // const b = new Promise(async (resolve, reject) => {
    //     await huobiau_huobiau_exchanges.forEach(async (element) => {
    //         var huobiau_huobi = new huobiau_huobi_liquidity(dbo,database, element, symbols_huobiau_huobiau);
    //         await huobiau_huobi.get_data();
    //     }).then(() =>{
    //        resolve("huobiau-huobi"); 
    //     });
    // })

    /**
     * ExchangeName_LiquidityBotName
     */

    // // huobipro - acx
    let huobipro_acx_1 = new Promise(async (resolve, reject) => {
        var huobipro_acx = new huobiau_huobi_liquidity(dbo, database_2, 'huobipro_acx_1', symbols_huobipro_acx);
        await huobipro_acx.get_data();
        resolve("huobipro-acx");
    });

    // // huobipro - huobi
    let huobipro_huobi_1 = new Promise(async (resolve, reject) => {
        var huobipro_huobi = new huobiau_huobi_liquidity(dbo, database, 'huobipro_huobi_1', symbols_huobipro_huobiau);
        await huobipro_huobi.get_data();
        resolve("huobipro-huobi")
    });
    // acx--huobi
    let acx_huobi_1 = new Promise(async (resolve, reject) => {
        var acx_huobi = new acx_acx_liquidity(dbo, database, 'acx_huobi_1');
        await acx_huobi.get_data();
        resolve("acx_huobi");
    });


    // binance -- acx
    let binance_acx_2 = new Promise(async (resolve, reject) => {
        var binance_acx = new binance(dbo, database_2, 'binance_acx_2', symbols_biance_acx);
        await binance_acx.get_data();
        resolve("binance-acx");
    });

    // binance -acx -fuel
    let binance_acx_2_fuel = new Promise(async (resolve, reject) => {
        var binance_acx_fuel = new binance(dbo, database_2, 'binance_acx_2', symbol_fuel_binance_acx);
        await binance_acx_fuel.get_data();
        resolve("binance-acx-fuel");
    });

    // binance get deposit and withdraw
    let binance_acx_2_dp_wd = new Promise(async (resolve, reject) => {
        var binance_acx_deposit_withdraw = new bi_deposit_withdraw(dbo, database_2, 'binance_acx_2');
        await binance_acx_deposit_withdraw.get_data();
        resolve("binance-acx");
    });

    return Promise.all([ACX_ACX1, ACX_ACX2, huobiau_huobi_1, huobiau_huobi_2, huobiau_huobi_3, huobipro_acx_1, huobipro_huobi_1, acx_huobi_1, binance_acx_2, binance_acx_2_fuel, binance_acx_2_dp_wd]);
}

function aaa() {
    MongoClient.connect(url, { useNewUrlParser: true }, async function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        await get_all_data(dbo).then(values => {
            console.log(values);
        });
        db.close();
        console.log('closed');
    });
}
aaa();
