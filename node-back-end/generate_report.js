const config = require('config');
var ObjectId = require('mongodb').ObjectID;
let baseCurrency = 'aud';
// let database_name = config.mongodb.database_name;
// let database_name = config.mongodb.database_name;
// let database_name = config.mongodb.database_liquidity;

let config_database = "config_database";

var MongoClient = require('mongodb').MongoClient;

class generate_report {
    constructor(dbo, fromTime, toTime, database) {
        this.fromTime = fromTime;
        this.toTime = toTime;
        // this.database_name = "acx_liquidity";
        this.dbo = dbo;
        this.database_name = database;
        this.database_balance = this.database_name + "_balance";
    }
    g_report() {
        let self = this;
        let a = this.getInputCurrncy();
        let b = this.getOutputCurrency();
        let c = this.getExchanges();
        // console.log(b);
        // });
        return Promise.all([a, b, c]).then(async data => {
            console.log(data["2"]);//exchanges
            // data["2"] = ["acx_acx_1"];//testing
            let union = new Set([...data["0"], ...data["1"]]);
            console.log(union);
            let array = Array.from(union);
            // array = ["eth"];//testing
            console.log(array);
            let alldata = [];
            while (self.fromTime <= self.toTime) {
                let btcPrice = 1;
                await self.getBtcPrice("btc", self.fromTime).then(data => {
                    console.log(data);
                    if(data.price){
                      btcPrice = data.price;  
                    }
                });
                console.log(btcPrice);
                for (var e = 0; e < data["2"].length; e++) {
                    for (var c = 0; c < array.length; c++) {
                        console.log(data["2"][e]);
                        console.log(array[c]);
                        if (array[c] !== '' && array[c] !== 'aud' && array[c] !== 'btc') {
                            let lastdata;
                            let altPrice = 0;
                            await self.getLastTrade(data["2"][e], array[c], self.fromTime).then(data => {
                                lastdata = data;
                            });
                            await self.getBtcPrice(array[c], self.fromTime).then(data => {
                                console.log(data);
                                if(data){
                                    altPrice = data.price;
                                }
                            });
                            console.log(lastdata);
                            console.log(altPrice);
                            let balance_amount = 0;
                            let balance_value = 0;
                                for (var l = 0; l < lastdata.length; l++) {
                                    if (array[c] == lastdata[l].output_currency) {
                                        balance_amount = lastdata[l].output_balance_amount_left;
                                        balance_value = lastdata[l].output_balance_value_left;
                                    } else if (array[c] === lastdata[l].input_currency) {
                                        balance_amount = lastdata[l].input_balance_amount_left;
                                        balance_value = lastdata[l].input_balance_value_left;
                                    }
                                }
                                let market_value = balance_amount * altPrice * btcPrice;
                                let gainLoss = market_value - balance_value;
                                let coin_data = {
                                    'name': array[c],
                                    'date': self.fromTime,
                                    'price': altPrice * btcPrice,
                                    'market': data["2"][e],
                                    'balance_amount': balance_amount,
                                    'balance_value': balance_value,
                                    'market_value': market_value,
                                    'unrealized_gain_or_loss': gainLoss
                                }
                                console.log(coin_data);
                                alldata.push(coin_data);
                        } else if (array[c] === 'btc') {
                            let btc_lastdata;
                            let balance_amount = 0;
                            let balance_value = 0;
                            await self.getLastTrade(data["2"][e], array[c], self.fromTime).then(data => {
                                btc_lastdata = data;
                            });
                            for (var l = 0; l < btc_lastdata.length; l++) {
                                if (array[c] == btc_lastdata[l].output_currency) {
                                    balance_amount = btc_lastdata[l].output_balance_amount_left;
                                    balance_value = btc_lastdata[l].output_balance_value_left;
                                } else if (array[c] === btc_lastdata[l].input_currency) {
                                    balance_amount = btc_lastdata[l].input_balance_amount_left;
                                    balance_value = btc_lastdata[l].input_balance_value_left;
                                }
                            }
                            let market_value = balance_value;
                            let gainLoss = market_value - balance_value;
                            let coin_data = {
                                'name': array[c],
                                'date': self.fromTime,
                                'price': btcPrice,
                                'market': data["2"][e],
                                'balance_amount': balance_amount,
                                'balance_value': balance_value,
                                'market_value': market_value,
                                'unrealized_gain_or_loss': gainLoss
                            }
                            alldata.push(coin_data);
                        }
                        else if(array[c] == 'aud'){
                            let aud_lastdata;
                            let balance_amount = 0;
                            let balance_value = 0;
                            await self.getLastTrade(data["2"][e], array[c], self.fromTime).then(data => {
                                aud_lastdata = data;
                            });
                            for (var l = 0; l < aud_lastdata.length; l++) {
                                if (array[c] == aud_lastdata[l].output_currency) {
                                    balance_amount = aud_lastdata[l].output_balance_amount_left;
                                    balance_value = aud_lastdata[l].output_balance_value_left;
                                } else if (array[c] === aud_lastdata[l].input_currency) {
                                    balance_amount = aud_lastdata[l].input_balance_amount_left;
                                    balance_value = aud_lastdata[l].input_balance_value_left;
                                }
                            }
                            let market_value = balance_value;
                            let gainLoss = market_value - balance_value;
                            let coin_data = {
                                'name': array[c],
                                'date': self.fromTime,
                                'price': 1,
                                'market': data["2"][e],
                                'balance_amount': balance_amount,
                                'balance_value': balance_value,
                                'market_value': market_value,
                                'unrealized_gain_or_loss': gainLoss
                            }
                            alldata.push(coin_data);

                        }
                    }
                }
                self.fromTime = self.fromTime + 24 * 60 * 60 * 1000;
            }
            console.log("alldata is ");
            console.log(alldata);
            self.insertManyMongodb(alldata).then(console.log);

        });

    }

    getExchanges() {
        let self = this;
        return new Promise((resolve, reject) => {
            this.dbo.collection(self.database_name).distinct("exchange", {}, (function (err, result) {
                if (err) {
                    return console.log(err);
                }
                if (result) {
                    console.log(result);
                    resolve(result);
                }
            })
            )
        })
    }

    insertManyMongodb(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
            this.dbo.collection(self.database_balance).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                resolve('done 1 time');
            });
        });
    }

    getOutputCurrency() {
        let self = this;
        return new Promise((resolve, reject) => {
            this.dbo.collection(self.database_name).distinct("output_currency", {}, (function (err, result) {
                if (err) {
                    return console.log(err);
                }
                if (result) {
                    console.log(result);
                    resolve(result);
                }
            })
            )
        })
    }

    getLastTrade(e, c, fromTime) {
        let self = this;
        return new Promise((resolve, reject) => {
            self.dbo.collection(self.database_name).find(
                {
                    "exchange": e,
                    "date": { '$lte': fromTime },
                    "$or": [{ "input_currency": c }, { "output_currency": c }]
                })
                .limit(1).sort("date", -1).toArray(function (err, res) {
                    if (err) throw err;
                    console.log(res);
                    resolve(res);
                });
        })
    }

    getInputCurrncy() {
        let self = this;
        return new Promise((resolve, reject) => {
            this.dbo.collection(self.database_name).distinct("input_currency", {}, (function (err, result) {
                if (err) {
                    return console.log(err);
                }
                if (result) {
                    console.log(result);
                    resolve(result);
                }
            })
            )
        })
    }

    getBtcPrice(currency, time) {
        let self = this;
        return new Promise((resolve, reject) => {
            this.dbo.collection("price_list").findOne({ "name": currency, "date": time }, function (err, result) {
                if (err) throw err;
                // console.log(result);
                resolve(result);
            })
        })
    }

    insertOne(data) {


    }

    union_arrays(x, y) {
        var obj = {};
        for (var i = x.length - 1; i >= 0; --i)
            obj[x[i]] = x[i];
        for (var i = y.length - 1; i >= 0; --i)
            obj[y[i]] = y[i];
        var res = []
        for (var k in obj) {
            if (obj.hasOwnProperty(k))  // <-- optional
                res.push(obj[k]);
        }
        return res;
    }

    toObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
            rv[i] = arr[i];
        return rv;
    }
}
module.exports = generate_report;