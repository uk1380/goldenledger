const independentreserveTrader = require('./ir/ir') 
const config = require('config');
var MongoClient = require('mongodb').MongoClient;
const url = config.mongodb.url;
const database_name = config.mongodb.database_liquidity;
let symbols = require('./binance/symbol.json');
var ir = config.ir;
var name = ir.name;

var irt = new independentreserveTrader({
    access_key: ir.access_key,
    secret_key: ir.secretkey
});
var totalhistory = [];
var last_deposit_date,last_trade_id;
get_last_id('ir').then(data => {

        if (data != null) {
            last_deposit_date = data.last_deposit_date;
        } else {
            last_deposit_date = 1514764800000;//20180101 12:00
            last_trade_id = 0;
            return new Promise((resolve, reject) => {
                // console.log("thrid steps");
                MongoClient.connect(url, function (err, db) {
                    if (err) throw err;
                    var dbo = db.db(config.mongodb.db);
                    dbo.collection(config.mongodb.config_database).insert({
                        "exchange": name,
                        "last_withdraw_date":1514764800000,
                        "last_trade_id": 0
                    }, function (err, result) {
                        if (err) throw err;
                        // console.log(result.name);
                        db.close();
        
                        resolve(result);
                    });
                });
            });
        }
    })
    .then(async () => {
        await get_trade_history().then(async (data) => {
            console.log(data);
            totalhistory.sort(compare);
            console.log('final data:' + totalhistory);

                await updateById({
                    "exchange": 'binance'
                }, {
                    '$set': {
                        "last_trade_id": last_trade_id + 1
                    }
                });
            
        });
    })
    .then(async () => {
        let accountGuids = [];
        await get_my_account().then(data =>{
            // console.log(data);
            
            data.forEach(element => {
                accountGuids.push(element.AccountGuid);
            });
        });
        let datas = [];
        for(let i = 0; i < accountGuids.length; i++){
            await get_transaction_history(accountGuids[i]).then(async (data) =>{
                console.log("get_transaction_history" + data);
                if(!isEmpty(data)){
                    data.forEach(element =>{
                        datas.push(element);
                    })
                }
            });
        }
        console.log(datas);
        datas.forEach(element => {
            if(element.SettleTimestampUtc){
            var date = new Date(element.SettleTimestampUtc)
            element.SettleTimestampUtc = date.getTime();
            }
            let new_Data ={};
            if(element.PrimaryCurrencyCode === "Xbt"){
                element.PrimaryCurrencyCode = "btc";
            }
            if(element.SecondaryCurrencyCode === "Xbt"){
                element.SecondaryCurrencyCode = "btc";
            }
            if(element.CurrencyCode === "Xbt"){
                element.CurrencyCode = "btc";
            }
            if(element.Type === "Withdrawal" || element.Type === "WithdrawalFee"){
                new_Data.output_currency = element.CurrencyCode.toLowerCase();
                new_Data.output_amount = element.Debit;
                new_Data.date = element.SettleTimestampUtc;
                new_Data.exchange = 'ir';
                new_Data.side = "withdraw";

                
            }
            if(element.Type === "Deposit"){
                new_Data.input_currency = element.CurrencyCode.toLowerCase();
                new_Data.input_amount = element.Credit;
                new_Data.date = element.SettleTimestampUtc;
                new_Data.exchange = 'ir';
                new_Data.side = "deposit";

            }
            new_Data.fee_currency = "aud";
            new_Data.fee_amount = 0;
            totalhistory.push(new_Data);
        });
                totalhistory.sort(compare);
                console.log('final data:' + totalhistory);
                if (totalhistory.length > 0) {
                    await insertManyMongodb(totalhistory).then(console.log);
                    await updateById({
                        "exchange": 'binance'
                    }, {
                        '$set': {
                            "last_trade_id": last_trade_id + 1
                        }
                    });
                }
            })

function get_my_account(){
    return new Promise(async (resolve, reject) => {
        await irt.getMyAccount().then( data => {
            resolve(data);
        })
        
    })
}

async function get_trade_history() {

    return new Promise(async (resolve, reject) => {
        let new_last_id = last_trade_id;
        await get_trade_history_until_null(new_last_id);
        
        resolve('get history done');
    })
}
async function get_transaction_history(guid) {

    return new Promise(async (resolve, reject) => {
        let new_last_id = last_deposit_date;
        await get_transaction_history_until_null(guid,new_last_id).then(data =>{
            resolve(data);
        });
    });
}

function get_transaction_history_until_null(guid,id){
    return new Promise(async (resolve, reject) => {
        console.log("get startttttt from" + id);
        let each_last_id = id;
        txTypes = ["Deposit","Withdrawal","WithdrawalFee"];
        timestamp = 1331209044000
        fromtimestamp = new Date(timestamp);
        await irt.getMyTransactions(guid,fromtimestamp,txTypes).then(data =>{
            console.log(data);
            resolve(data);
        })
    })

}


function get_trade_history_until_null(id) {
    return new Promise(async (resolve, reject) => {
        console.log('get start from ' + id);
        let each_last_id = id;
        // instrument = symbol.instrument;
        // currency = symbol.currency;
        // pair = {
        //     instrument,
        //     currency
        // }
        
        await irt.getMyTrades().then(async data => {
            console.log("ddddddddddddddddddd")
            console.log(data);
            if (data) {
                if (data.Data.length > 0) {
                    data.Data.forEach(obj => {

                        console.log("each object is ")
                        console.log(obj);
                        if(obj.PrimaryCurrencyCode === "Xbt"){
                            obj.PrimaryCurrencyCode = "btc";
                        }
                        if(obj.SecondaryCurrencyCode === "Xbt"){
                            obj.SecondaryCurrencyCode = "btc";
                        }
                        let new_Data = {};
                        if (each_last_id < obj.id) {
                            each_last_id = obj.id;
                        }
                        if(obj.TradeTimestampUtc){
                            var date = new Date(obj.TradeTimestampUtc)
                            obj.TradeTimestampUtc = date.getTime();
                            }
                        new_Data.id = obj.TradeGuid;
                        new_Data.date = obj.TradeTimestampUtc;
                        new_Data.price = obj.Price;
                        new_Data.exchange = 'ir';
                        if(obj.OrderType.includes("Bid")){
                           new_Data.side = "bid";
                           new_Data.input_currency = obj.PrimaryCurrencyCode.toLowerCase();
                           new_Data.output_currency = obj.SecondaryCurrencyCode.toLowerCase();
                           new_Data.input_amount = obj.VolumeTraded;
                           new_Data.output_amount = obj.VolumeTraded * obj.Price * 1.0022
                           new_Data.fee_currency = "aud";
                           new_Data.fee_amount = obj.VolumeTraded * obj.Price * 0.0022;
                        } else{
                            new_Data.side = "ask";
                            new_Data.output_currency = obj.PrimaryCurrencyCode.toLowerCase();
                            new_Data.input_currency = obj.SecondaryCurrencyCode.toLowerCase();
                            new_Data.output_amount = obj.VolumeTraded;
                            new_Data.input_amount = obj.VolumeTraded * obj.Price - obj.VolumeTraded * obj.Price * 0.0022;
                            new_Data.fee_currency = "aud";
                            new_Data.fee_amount = obj.VolumeTraded * obj.Price * 0.0022;
                        }
                        


                        totalhistory.push(new_Data);

                    });
                    // await get_trade_history_until_null(symbol, each_last_id + 1);
                    resolve();
                } else {
                    // console.log(symbol.symbol + ' finished');
                    if (last_trade_id < each_last_id) {
                        last_trade_id = each_last_id;
                    }
                    resolve('done');
                }
            }
        })
        // .catch(async (err) => {
        //     console.log(err);
        //     console.log('re catch');
        //     await get_trade_history_until_null(symbol, id).then(console.log);
        //     resolve('re catch done');
        // })
        // await irt.
    })

}


function insertManyMongodb(data) {
    return new Promise((resolve, reject) => {
        // console.log("thrid steps");

        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("acxliquiditydev01");
            dbo.collection(database_name).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                db.close();
                resolve('done 1 time');
            });
        });
    });
}

function get_last_id(exchange) {
    return new Promise((resolve, reject) => {
        // console.log("thrid steps");

        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db(config.mongodb.db);
            dbo.collection(config.mongodb.config_database).findOne({
                "exchange": exchange
            }, function (err, result) {
                if (err) throw err;
                // console.log(result.name);
                db.close();

                resolve(result);
            });
        });
    });
}

function updateById(query, set) {
    console.log(query, set);
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db(config.mongodb.db);
            dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
                db.close();
                resolve(res);
            });
        });
    })
}


function compare(a, b) {
    if (a.date < b.date)
        return -1;
    if (a.date > b.date)
        return 1;
    if (a.date == b.date) {
        if (a.id < b.id) {
            a.date -= 1;
            return -1
        } else {
            a.date += 1;
            return 1;
        }
    }
    return 0;
}
function isEmpty(obj) { 
    for (var x in obj) { return false; }
    return true;
 }