var http = require('http');
var express = require('express');
var app = express();
var bodyParser = require("body-parser");
const cors = require('cors');
// const util = require('util')

const calulation = require('./dataProcess-test');
const ACX = require('acx');
const BTCM = require('./BtcMarket/btcmarketsTrader');
const mongo = require('mongodb');
const greport = require('./generate_report');
var async = require('async');
const config = require('config');
var ObjectId = require('mongodb').ObjectID;

let database_name = "initial";

let baseCurrency = 'aud';
app.use(cors());
app.use('/', express.static('../dist'));
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));




var MongoClient = require('mongodb').MongoClient;
var dbo;
let allDb;
let isConnected = false;
MongoClient.connect(config.mongodb.url, { useNewUrlParser: true }, function (err, db) {
    if (err) throw err;
    dbo = db.db(config.mongodb.db)
    isConnected = true;
    dbEventsHandler(db);
});
function dbEventsHandler(client) {
    client.on('error', (error) => {
        console.error("MongoDB error:", error);
    });
    client.on('close', () => {
        console.log('MongoDb closed');
        isConnected = false;
    });
    client.on('reconnect', () => {
        console.log('MongoDB reconnected');
        isConnected = true;
    });
}

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/index.htm', function (req, res) {
    res.sendFile(__dirname + "/public/" + "index.html");
})

app.get('/test.htm', async function (req, res) {
    let db_result;
    db_result = await getDbResults();

    res.json(db_result);
})

app.get('/listtestcollection.htm', async function (req, res) {

    console.log('step1');

    query = "";
    let db_result;
    db_result = await getAllBalance(query);
    res.json(db_result);
})

app.get('/getLastReportDate.htm', async function (req, res) {

    console.log('step1');

    query = "";
    let db_result;
    db_result = await getOneBalance(query);
    res.json(db_result);
})

app.get('/dataverify.htm', async function (req, res) {

    console.log('step1');

    let db_result;
    db_result = await dataverify();
    res.json(db_result);
})

app.get('/duplicateTime.htm', async function (req, res) {

    console.log('step1');

    let db_result;
    db_result = await duplicateTime();
    console.log(db_result);
    res.json(db_result);
})

app.get('/getDbName.htm', async function (req, res) {

    console.log('step1');

    dbname = database_name;
    console.log(dbname);
    res.json({ name: dbname });
})

app.get('/test1.htm', async function (req, res) {
    // console.log(req);
    console.log('step1');
    let query = req.query;
    query.input_currency = { '$ne': baseCurrency };
    query.output_currency = { '$ne': baseCurrency }
    query.source = { '$exists': false }
    console.log(query);
    let db_result;
    db_result = await getDbResults(query);
    res.json(db_result);
})

app.post('/dta.htm', function (req, res) {
    console.log(req.body);
    console.log(req.body[Object.keys(req.body)[0]]);
    database_name = req.body[Object.keys(req.body)[0]];
    res.send({ status: 'done' });
})

app.post('/calculation.htm', function (req, res) {
    console.log("callll");
    console.log(req.body.fromTime);
    let fromts = req.body.fromTime;
    let tots = req.body.toTime;
    console.log(req.body.toTime);
    var get_cal = new calulation(dbo, database_name, fromts, tots);
    get_cal.get_cal().then(() => {
        console.log("doneeeeeee");
        res.send({ status: 'success!' });
    })
})

app.post('/generateReport.htm', async function (req, res) {
    console.log("callll");
    console.log(req.body.fromTime);
    let refromts = req.body.fromTime;
    let retots = req.body.toTime;
    console.log(req.body.toTime);
    var myquery = { "date": { "$gte": refromts } };
    await removeBalance(myquery);
    var g_report = new greport(dbo, refromts, retots, database_name);
    g_report.g_report().then(() => {
        console.log("doneeeeeee");
        res.send({ status: 'success!' });
    })
})

app.post('/updateOne.htm', async function (req, res) {
    console.log(req.body.obj);
    let query = req.body.obj._id;
    delete req.body.obj._id
    obj = { '$set': req.body.obj };
    console.log(obj);
    await updateById(query, obj);
    res.send({ status: 'success' });
})

app.post('/searchCoin.htm', async function (req, res) {
    console.log(req.body.obj);
    let query = req.body.obj;
    // obj = {'$set' : req.body.obj};
    // console.log(obj);
    let d;
    await searchCoin(query).then(data => {
        d = data;
    });
    console.log(d);
    res.send(d);
})

app.post('/deleteById.htm', async function (req, res) {
    console.log(req);
    console.log(req.body);
    let query = req.body.query;
    await deleteById(query);
    res.send({ status: 'success' });
})


app.post('/updateById.htm', async function (req, res) {
    console.log(req.body);
    let query = req.body.query._id,
        obj = { '$set': req.body.obj };
    await updateById(query, obj).then(data => {
        res.send(data);

    });
})

app.post('/unsetById.htm', async function (req, res) {
    console.log(req.body);
    let query = req.body.query._id,
        obj = req.body.obj;
    await updateById(query, obj).then(data => {
        res.send(data);
    });
})

app.post('/addNewRecord.htm', async function (req, res) {
    console.log(req.body);
    await insertMongodb(req.body.obj);
    res.send({ status: 'success' });
})

app.post('/addManyRecord.htm', async function (req, res) {
    console.log(req.body);
    await insertManyMongodb(req.body);
    res.send({ status: 'success' });
})

app.get('/getCoinList.htm', async function (req, res) {
    let query = req.query.query;
    let db_result;
    db_result = await getDistinct(query);
    console.log(db_result);
    res.send(db_result);
})

var server = app.listen(8081, function () {

    var host = server.address().address
    var port = server.address().port

    console.log("应用实例，访问地址为 http://%s:%s", host, port)

})



// db.close();
function dataverify() {
    return new Promise((resolve, reject) => {
        getDbResults().then(data => {
            let depositSource;
            let depositDate;
            let de_wi_list = [];
            data.forEach(element => {
                if (element.side == "deposit" && element.hasOwnProperty("source")) {
                    console.log("element");
                    depositSource = element.source;
                    depositDate = element.date;
                    data.forEach(elementone => {
                        if (elementone.side === "withdraw") {
                            if (elementone.source == depositSource && elementone.date > depositDate) {
                                console.log("this withdraw record is later than the deposit time");
                                console.log(elementone);
                                console.log(element);
                                de_wi_list.push(element);
                                de_wi_list.push(elementone);

                            }
                        }
                    });
                }
                resolve(de_wi_list);
            });
        });
    });
}

function duplicateTime() {
    return new Promise((resolve, reject) => {
        let query1 = "exchange";
        let dupdata = [];
        getDistinct(query1).then(async res => {
            console.log(res);
            for (var k=0;k < res.length;k++) {
                console.log(res[k]);
                let query2 = { "exchange": res[k] };
                await getDbResults(query2).then(data => {
                    for (var i = 0; i < data.length - 1; i++) {
                        let m = i + 1;
                        if (data[i].date === data[m].date) {
                            dupdata.push(data[i]);
                            dupdata.push(data[m]);
                        }
                    }
                });
            }
        resolve(dupdata);
        });
        
    })
}

function compare(a, b) {
    if (a.time < b.time)
        return -1;
    if (a.time > b.time)
        return 1;
    return 0;
}

function insertMongodb(data) {
    return new Promise((resolve, reject) => {
        console.log("thrid steps");

        dbo.collection(database_name).insertOne(data, function (err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            resolve('done 1 time');
        });
    });

}
function insertManyMongodb(data) {
    return new Promise((resolve, reject) => {
        console.log("thrid steps");

        dbo.collection(database_name).insertMany(data, function (err, res) {
            if (err) throw err;
            console.log("all document inserted");
            resolve('done 1 time');
        });
    });

    // });
}

function getDbResults(query = "") {
    return new Promise((resolve, reject) => {
        if (isConnected) {
            dbo.collection(database_name).find(query).sort({ date: 1 }).toArray(function (err, res) {
                if (err) throw err;
                resolve(res);
            });
        }

    });
}

function getAllBalance(query = "") {
    let database_balance = database_name + "_balance";
    return new Promise((resolve, reject) => {
        dbo.collection(database_balance).find(query).sort({ date: -1 }).toArray(function (err, res) {
            if (err) throw err;

            resolve(res);
        });
    });
}

function getOneBalance(query = "") {
    let database_balance = database_name + "_balance";
    return new Promise((resolve, reject) => {
        dbo.collection(database_balance).find(query).sort({ date: -1 }).limit(1).toArray(function (err, res) {
            if (err) throw err;

            resolve(res);
        });
    });
}
function removeBalance(query) {
    let database_balance = database_name + "_balance";
    return new Promise((resolve, reject) => {
        dbo.collection(database_balance).deleteMany(query, function (err, res) {
            if (err) throw err;
            console.log("documents deleted");
            resolve(res);
        });
    });
}


function searchCoin(query) {
    console.log(query);
    return new Promise((resolve, reject) => {
        dbo.collection(database_name).find(query).sort({ date: -1 })
            .toArray(function (err, res) {
                if (err) throw err;
                console.log('data:' + res);
                resolve(res);
            })
    });
}

function getDistinct(colume_name) {
    return new Promise((resolve, reject) => {
        console.log(colume_name);
        dbo.collection(database_name).distinct(colume_name, function (err, res) {
            if (err) throw err;
            resolve(res);
        })
    });
}

function updateById(query, set) {
    console.log(set);
    return new Promise((resolve, reject) => {
        dbo.collection(database_name).updateOne({ '_id': new ObjectId(query) }, set, function (err, res) {
            if (err) throw err;
            console.log("1 document updated");
            resolve(res);
        });
    });
}

function deleteById(query) {
    return new Promise((resolve, reject) => {
        dbo.collection(database_name).deleteOne({ '_id': new ObjectId(query) }, function (err, res) {
            if (err) throw err;
            console.log("1 document delete");
            resolve(res);
        });
    });
}



