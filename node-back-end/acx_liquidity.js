const ACX = require('acx');
const symbols = require('./Acx/symbols.json');
const config = require('config');
// const database_name = config.mongodb.test_database;
// const url = config.mongodb.url;
const MongoClient = require('mongodb').MongoClient;
// const acx = config.acx_liquidity[name];
// var last_deposit_id;
// var last_withdraw_id;
// var last_trade_id;



class get_acx_liquidity_data {
    constructor(dbo, database, name) {
        this.database_name = database;
        this.name = name;
        this.acx = config.acx[name];
        this.fee_rate = this.acx.fee_rate;
        this.acxTrader = new ACX({
            access_key: this.acx.access_key,
            secret_key: this.acx.secret_key
        });
        this.last_deposit_id;
        this.last_withdraw_id;
        this.last_trade_id;
        this.totalhistory = [];
        this.totalboth = [];
        this.flag = false;
        this.dbo = dbo;

    }
    // var name = "acx_liquidity_2";
    async get_data() {
        let self = this;
        await self.get_last_id(self.name).then(data => {
            console.log(self.name);
            if (data != null) {
                self.last_deposit_id = data.last_deposit_id,
                    self.last_withdraw_id = data.last_withdraw_id,
                    self.last_trade_id = data.last_trade_id;
            } else {
                self.last_deposit_id = 0;
                self.last_withdraw_id = 0;
                self.last_trade_id = 0;
                // return new Promise((resolve, reject) => {
                //     // console.log("thrid steps");
                //     MongoClient.connect(url, function (err, db) {
                //         if (err) throw err;
                //         var dbo = db.db(config.mongodb.db);
                //         self.dbo.collection(config.mongodb.config_database).insert({
                //             "exchange": self.name,
                //             "last_deposit_id":0,
                //             "last_withdraw_id":0,
                //             "last_trade_id":0
                //         }, function (err, result) {
                //             if (err) throw err;
                //             // console.log(result.name);
                //             db.close();

                //             resolve(result);
                //         });
                //     });
                // });
            }
        })
            .then(async () => {
                await self.get_deposit_history();
            })
            .then(async () => {
                await self.get_withdraw_history();
            })
            .then(async () => {
                await self.get_trade_history().then(async (data) => {
                    console.log(data);
                    self.totalhistory.sort(self.compareID);
                    while (self.flag == false) {
                        self.flag = true;
                        self.totalhistory = self.custom_compare(self.totalhistory);
                    }
                    self.totalhistory.sort(self.compare);
                    console.log('final side both data');
                    console.log(self.totalboth);
                    console.log('final data:' + self.totalhistory);
                    if (self.totalhistory.length > 0) {
                        await self.insertManyMongodb(self.totalhistory).then(console.log);
                        await self.updateById({
                            "exchange": self.acx.name
                        }, {
                                '$set': {
                                    "last_trade_id": self.last_trade_id,
                                    'last_deposit_id': self.last_deposit_id,
                                    "last_withdraw_id": self.last_withdraw_id

                                }
                            });
                    }
                })
            })
        // });
    }



    //抓不同交易所的时候需要修改name



    async get_trade_history() {

        return new Promise(async (resolve, reject) => {

            let new_last_id = this.last_trade_id;
            for (let i = 0; i < symbols.length; i++) {
                // p = p.then(async()=>{
                await this.get_history_until_null(symbols[i], new_last_id);
                // })
            }
            resolve('get history done');
        })

    }

    async  get_history_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {
            console.log('get ' + symbol.symbol + ' start from ' + id);
            let each_last_id = id;
            // console.log(this.acxTrader);
            await this.acxTrader.getMyTrades({
                market: symbol.symbol,
                order_by: 'asc',
                limit: 200,
                from: id
            }).then(async data => {
                console.log(data);
                if (data) {
                    if (data.length > 0) {
                        data.forEach(obj => {
                            console.log(obj);
                            if (obj.id > each_last_id) {
                                each_last_id = obj.id;
                            }
                            let new_Data = {};
                            let new_bid = {};
                            let new_ask = {};
                            if (obj.side !== 'both') {
                                new_Data.id = obj.id;
                                new_Data.date = new Date(obj['created_at']).valueOf();
                                new_Data.price = parseFloat(obj.price);
                                new_Data.exchange = this.acx.name;
                                // if(obj.side !== 'both'){
                                if (obj.side == 'ask') {
                                    new_Data.side = "ask";
                                    new_Data.input_currency = symbol.currency;
                                    new_Data.input_amount = parseFloat(obj['funds']) - this.fee_rate * parseFloat(obj['funds']);
                                    new_Data.output_currency = symbol.instrument;
                                    new_Data.output_amount = parseFloat(obj['volume']);

                                } else if (obj.side == 'bid') {
                                    new_Data.side = "bid";
                                    new_Data.input_currency = symbol.instrument;
                                    new_Data.input_amount = parseFloat(obj['volume']) - this.fee_rate * parseFloat(obj['volume']);
                                    new_Data.output_currency = symbol.currency;
                                    new_Data.output_amount = parseFloat(obj['funds']);
                                } else {
                                    new_Data.side = obj.side;
                                }

                                new_Data.fee_currency = new_Data.input_currency;
                                new_Data.fee_amount = new_Data.input_amount * this.fee_rate / (1 - this.fee_rate);

                                this.totalhistory.push(new_Data);
                            } else {
                                this.totalboth.push(obj);
                                new_bid.side = "bid";
                                new_bid.input_currency = symbol.instrument;
                                new_bid.input_amount = parseFloat(obj['volume']) - this.fee_rate * parseFloat(obj['volume']);
                                new_bid.output_currency = symbol.currency;
                                new_bid.output_amount = parseFloat(obj['funds']);
                                new_bid.id = obj.id;
                                new_bid.date = new Date(obj['created_at']).valueOf();
                                new_bid.price = parseFloat(obj.price);
                                new_bid.exchange = this.acx.name;

                                new_ask.side = "ask";
                                new_ask.input_currency = symbol.currency;
                                new_ask.input_amount = parseFloat(obj['funds']) - this.fee_rate * parseFloat(obj['funds']);
                                new_ask.output_currency = symbol.instrument;
                                new_ask.output_amount = parseFloat(obj['volume']);
                                new_ask.id = obj.id;
                                new_ask.date = new Date(obj['created_at']).valueOf();
                                new_ask.price = parseFloat(obj.price);
                                new_ask.exchange = this.acx.name;

                                this.totalhistory.push(new_bid);
                                this.totalhistory.push(new_ask);
                            }
                            // }else{
                            //     this.totalboth.push(obj);
                            // }
                        });
                        await this.get_history_until_null(symbol, each_last_id);
                        resolve();
                    } else {
                        console.log(symbol.symbol + ' finished');
                        if (this.last_trade_id < each_last_id) {
                            this.last_trade_id = each_last_id;
                        }
                        resolve();
                    }
                }
            }).catch(async (err) => {
                console.log(err);
                await this.get_history_until_null(symbol, id);
                // resolve();
            })
        })
    }


    async get_deposit_history() {
        console.log('deposit start');

        this.acxTrader.getDeposits({
            state: 'accepted',
            limit: 100
        }).then(async data => {
            console.log('deposit end');

            if (data.length > 0) {
                console.log('process deposit data');
                let rebulid_Data = data.filter(obj => {
                    return obj.id > this.last_deposit_id;
                }).map(obj => {
                    if (obj.id > this.last_deposit_id) {
                        this.last_deposit_id = obj.id;
                    }
                    let objtxId;
                    if (obj.hasOwnProperty("txid")) {
                        objtxId = obj.txid;
                    }
                    if (obj.hasOwnProperty("transaction_hash")) {
                        objtxId = obj.transaction_hash;
                    }
                    let new_Data = {
                        id: obj.id,
                        date: new Date(obj['done_at']).valueOf(),
                        exchange: this.acx.name,
                        side: 'deposit',
                        input_currency: obj.currency,
                        txId: objtxId,
                        input_amount: parseFloat(obj.amount),
                        fee_amount: parseFloat(obj.fee),
                        fee_currency: obj.currency
                    }

                    return new_Data;
                    // }else {
                    //     return false;
                    // }
                })

                if (rebulid_Data.length > 0) {
                    rebulid_Data.forEach(obj => {
                        this.totalhistory.push(obj);
                    });
                    console.log('deposit: ' + rebulid_Data);
                    // await insertManyMongodb(rebulid_Data).then(console.log);
                }
            }
        });

    }

    async get_withdraw_history() {
        console.log('withdraw start');
        this.acxTrader.getWithdraws({
            state: 'done',
            limit: 100
        }).then(async data => {
            console.log('withdraw end');
            if (data.length > 0) {
                console.log('process withdraw data');
                let rebulid_Data = data.filter(obj => {
                    return obj.id > this.last_withdraw_id;
                }).map(obj => {

                    if (obj.id > this.last_withdraw_id) {
                        this.last_withdraw_id = obj.id;
                    }
                    let objtxId;
                    if (obj.hasOwnProperty("txid")) {
                        objtxId = obj.txid;
                    }
                    if (obj.hasOwnProperty("transaction_hash")) {
                        objtxId = obj.transaction_hash;
                    }
                    let new_Data = {
                        id: obj.id,
                        date: new Date(obj['created_at']).valueOf(),
                        exchange: this.acx.name,
                        side: 'withdraw',
                        output_currency: obj.currency,
                        txId: objtxId,
                        output_amount: parseFloat(obj.amount) + parseFloat(obj.fee),
                        fee_amount: parseFloat(obj.fee),
                        fee_currency: obj.currency
                    }
                    return new_Data;
                })
                if (rebulid_Data.length > 0) {
                    rebulid_Data.forEach(obj => {
                        this.totalhistory.push(obj);
                    });
                    console.log('withdraw: ' + rebulid_Data);
                    // await insertManyMongodb(rebulid_Data).then(console.log);
                }
            }
        });

    }

    get_last_id(exchange) {
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
            this.dbo.collection(config.mongodb.config_database).findOne({
                "exchange": exchange
            }, function (err, result) {
                if (err) throw err;
                // console.log(result.name);
                resolve(result);
            });
        });
    }

    updateById(query, set) {
        console.log(query, set);
        return new Promise((resolve, reject) => {
            // MongoClient.connect(url, { useNewUrlParser: true },function (err, db) {
            //     if (err) throw err;
            //     var dbo = db.db(config.mongodb.db);
            this.dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
                // db.close();
                resolve(res);
            });
        });
        // })
    }

    insertManyMongodb(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
            this.dbo.collection(self.database_name).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                resolve('done 1 time');
            });
        });
    }

    compare(a, b) {
        console.log(a.id + " : " + b.id);
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
        if (a.date == b.date) {
            if (a.id < b.id) {
                a.date -= 1;
                return -1
            } else {
                a.date += 1;
                return 1;
            }
        }
        return 0;
    }

    compareID(a, b) {
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
        if (a.id < b.id)
            return -1;
        if (a.id > b.id)
            return 1;
        return 0;
    }

    custom_compare(data) {
        var self = this;
        for (let i = 0; i < data.length - 1; i++) {
            try {

                if (data[i].date >= data[i + 1].date) {
                    self.flag = false;
                    data[i + 1].date = data[i].date + 1;
                }

            } catch (err) {
                console.log(err + data[i].id);
            }
        }
        return data;
    }
}


module.exports = get_acx_liquidity_data;
