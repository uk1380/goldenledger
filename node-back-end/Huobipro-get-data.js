const hbsdk = require('./HuobiMarket/hbsdk');
const config = require('config');
var MongoClient = require('mongodb').MongoClient;
const url = config.mongodb.url;
// const database_name = config.mongodb.database_name;
let symbols = require('./HuobiMarket/symbols.json');

var totalhistory = [];
var flag = false;
var last_deposit_id;
var last_withdraw_id;
var last_trade_id;

class huobipro_get_data{
    constructor(database,exchange){
        this.database_name = database;
        this.exchange = exchange;
    }
    get_data(){
        this.get_last_id('huobipro11').then(data => {//huobipro

            if (data != null) {
                last_deposit_id = data.last_deposit_id,
                last_withdraw_id = data.last_withdraw_id
                last_trade_id = data.last_trade_id;
            } else {
                last_deposit_id = 0,
                last_withdraw_id = 0,
                last_trade_id = 0;
            }
        }).then(async ()=>{
                await this.get_deposit_History().then(async data => {
                    console.log(data);
                    await this.updateById({
                        "exchange": this.exchange
                    }, {
                        '$set': {
                            "last_deposit_id": last_deposit_id + 1
                        }
                    });
                });
                await this.get_withdraw_History().then(async data => {
                    console.log(data);
                    await this.updateById({
                        "exchange":this.exchange
                    }, {
                        '$set': {
                            "last_withdraw_id": last_withdraw_id + 1
                        }
                    });
                });
        
        })
        .then(async () => {
            await this.get_trade_history().then(async(data) => {
                console.log(data);
                totalhistory.sort(compareID);
                while(flag == false){
                    flag = true;
                    totalhistory = this.custom_compare(totalhistory);
                }
                totalhistory.sort(this.compare);
                console.log('final data:' + totalhistory);
                if (totalhistory.length > 0) {
                    await this.insertManyMongodb(totalhistory).then(console.log);
                }
                await this.updateById({
                    "exchange":this.exchange
                }, {
                    '$set': {
                        "last_trade_id": last_trade_id + 1
                    }
                });
            });
        })
    }

    async get_trade_history() {

        return new Promise(async (resolve, reject) => {
    
            let new_last_id = last_trade_id;
            for (let i = 0; i < symbols.length; i++) {
                // p = p.then(async()=>{
                await this.get_history_until_null(symbols[i], new_last_id);
                // })
            }
            resolve('get history done');
        })
    
    }
    
    get_history_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {
    
            console.log('get ' + symbol.symbol + ' start from ' + id);
            let each_last_id = id;
            await hbsdk.get_history(symbol.symbol, id).then(async data => {
                if (data) {
                    console.log(data);
                    if (data.length > 0) {
                        console.log(data);
                        data.forEach(obj => {
                            let new_Data = {};
                            if (each_last_id < obj.id) {
                                each_last_id = obj.id;
                            }
    
                            new_Data.id = obj.id;
                            new_Data.date = obj['created-at'];
                            new_Data.price = parseFloat(obj.price);
                            new_Data.exchange =this.exchange;
    
                            if (obj.type == 'sell-limit' || obj.type == 'sell-market') {
                                new_Data.side = "ask";
                                new_Data.input_currency = symbol.currency;
                                new_Data.input_amount = parseFloat(obj['filled-amount']) * parseFloat(obj.price);
                                new_Data.output_currency = symbol.instrument;
                                new_Data.output_amount = obj['filled-amount'];
    
                            } else {
                                new_Data.side = "bid";
                                new_Data.input_currency = symbol.instrument;
                                new_Data.input_amount = parseFloat(obj['filled-amount']);
                                new_Data.output_currency = symbol.currency;
                                new_Data.output_amount = parseFloat(obj['filled-amount']) * parseFloat(obj.price);
                            }
    
                            new_Data.fee_currency = new_Data.input_currency;
                            new_Data.fee_amount = parseFloat(obj['filled-fees']);
    
                            totalhistory.push(new_Data);
    
                        });
                        await get_history_until_null(symbol, each_last_id + 1);
                        resolve();
                    } else {
                        console.log(symbol.symbol + ' finished');
                        if (last_trade_id < each_last_id) {
                            last_trade_id = each_last_id;
                        }
                        resolve('done');
                    }
                }
            }).catch(async () => {
                console.log('re catch');
                await get_history_until_null(symbol, id).then(console.log);
                resolve('re catch done');
            })
        })
    
    }
    
    async get_deposit_History() {
    
            return new Promise(async (resolve, reject) => {
                let symbolList = [];
                symbols.forEach(symbol => {
                    if (!symbolList.includes(symbol.instrument)) {
                        symbolList.push(symbol.instrument)
                    }
                    if (!symbolList.includes(symbol.currency)) {
                        symbolList.push(symbol.currency)
                    }
                })
                let new_last_id = last_deposit_id;
                for (let i = 0; i < symbolList.length; i++) {
                    await get_deposit_history_until_null(symbolList[i], new_last_id);
                }
                resolve('get deposit history done');
            })
        
    
    }
    
    get_deposit_history_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {
    
            console.log('get ' + symbol + ' deposit start from ' + id);
            let each_last_id = id;
            await hbsdk.get_deposit_history(symbol, each_last_id, 100).then(async data => {
                if (data) {
                    console.log(data);
                    if (data.length > 0) {
                        console.log(data);
                        data.forEach(obj => {
                            if(obj.currency === "bcc"){
                                obj.currency = "bch";
                            }
                            let new_Data = {};
                            if (each_last_id < obj.id) {
                                each_last_id = obj.id;
                            }
                            new_Data.id = obj.id;
                            new_Data.date = obj['updated-at'];
                            new_Data.exchange =this.exchange;
                            new_Data.side = obj.type;
                            new_Data.address = obj.address;
                            new_Data.txId = obj['tx-hash'];
                            // if (new_Data.side == 'deposit') {
                                new_Data.input_currency = obj.currency;
                                new_Data.input_amount = obj.amount;
                            // } else {
                            //     new_Data.output_currency = obj.currency;
                            //     new_Data.output_amount = obj.amount;
                            // }
                            new_Data.fee_currency = obj.currency;
                            new_Data.fee_amount = obj.fee;
                            new_Data.status = obj.state == 'safe' ? 'complete' : 'Not complete';
    
    
                            totalhistory.push(new_Data);
    
                        });
                        await get_deposit_history_until_null(symbol, each_last_id + 1);
                        resolve();
                    } else {
                        console.log(symbol + ' finished');
                        if (last_deposit_id < each_last_id) {
                            last_deposit_id = each_last_id;
                        }
                        resolve('done');
                    }
                }
            }).catch(async () => {
                console.log('re catch');
                await get_deposit_history_until_null(symbol, id).then(console.log);
                resolve('re catch done');
            })
        })
    }
    
    
    async get_withdraw_History() {
    
        return new Promise(async (resolve, reject) => {
            let symbolList = [];
            symbols.forEach(symbol => {
                if (!symbolList.includes(symbol.instrument)) {
                    symbolList.push(symbol.instrument)
                }
                if (!symbolList.includes(symbol.currency)) {
                    symbolList.push(symbol.currency)
                }
            })
            let new_last_id = last_withdraw_id;
            for (let i = 0; i < symbolList.length; i++) {
                // p = p.then(async()=>{
                await get_withdraw_until_null(symbolList[i], new_last_id);
                // })
            }
            resolve('get withdraw history done');
        })
    
    
    }
    
    get_withdraw_until_null(symbol, id) {
    return new Promise(async (resolve, reject) => {
    
        console.log('get ' + symbol + ' withdraw start from ' + id);
        let each_last_id = id;
        await hbsdk.get_withdraw_history(symbol, each_last_id, 100).then(async data => {
            if (data) {
                console.log(data);
                if (data.length > 0) {
                    console.log(data);
                    data.forEach(obj => {
                        if(obj.currency === "bcc"){
                            obj.currency = "bch";
                        }
                        let new_Data = {};
                        if (each_last_id < obj.id) {
                            each_last_id = obj.id;
                        }
                        new_Data.id = obj.id;
                        new_Data.date = obj['updated-at'];
                        new_Data.exchange =this.exchange;
                        new_Data.side = obj.type;
                        new_Data.address = obj.address;
                        new_Data.txId = obj['tx-hash'];
                        // if (new_Data.side == 'withdraw') {
                            new_Data.output_currency = obj.currency;
                            new_Data.output_amount = obj.amount + obj.fee;
                        // }
                        new_Data.fee_currency = obj.currency;
                        new_Data.fee_amount = obj.fee;
                        new_Data.status = obj.state == 'confirmed' ? 'complete' : 'Not complete';
    
    
                        totalhistory.push(new_Data);
    
                    });
                    await get_withdraw_until_null(symbol, each_last_id + 1);
                    resolve();
                } else {
                    console.log(symbol + ' finished');
                    if (last_withdraw_id < each_last_id) {
                        last_withdraw_id = each_last_id;
                    }
                    resolve('done');
                }
            }
        }).catch(async () => {
            console.log('re catch');
            await get_withdraw_until_null(symbol, id).then(console.log);
            resolve('re catch done');
        })
    })
    }
    
    
    
    insertManyMongodb(data) {
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
    
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("mydb");
                dbo.collection(this.database_name).insertMany(data, function (err, res) {
                    if (err) throw err;
                    console.log("Number of documents inserted: " + res.insertedCount);
                    db.close();
                    resolve('done 1 time');
                });
            });
        });
    }
    
    get_last_id(exchange) {
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
    
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db(config.mongodb.db);
                dbo.collection(config.mongodb.config_database).findOne({
                    "exchange":this.exchange
                }, function (err, result) {
                    if (err) throw err;
                    // console.log(result.name);
                    db.close();
    
                    resolve(result);
                });
            });
        });
    }
    
    updateById(query, set) {
        console.log(query, set);
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db(config.mongodb.db);
                dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                    if (err) throw err;
                    console.log("1 document updated");
                    db.close();
                    resolve(res);
                });
            });
        })
    }
    
    compareID(a, b) {
        if (a.id < b.id)
            return -1;
        if (a.id > b.id)
            return 1;
        return 0;
    }
    
    custom_compare(data){
        for(let i =0; i<data.length-1;i++){
            try {
                if(data[i].side != 'deposit' && data[i].side != 'withdraw'){
                    if(data[i].date >= data[i+1].date){
                        flag = false;
                        if(data[i].id < data[i+1].id){
                            data[i].date -= 1;
                        }else{
                            data[i+1].date += 1;
                        }
                    }
                }
            }catch(err){
                console.log(err + data[i].id);
            }
        }
        return data;
    }
    
    compare(a, b) {
        console.log(a.id + " : " + b.id);
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
            if (a.date == b.date) { 
                flag = false;
                if (a.id < b.id) {
                    a.date -= 1;
                    return -1
                } else {
                    a.date += 1;
                    return 1;
                }
            }
        return 0;
    }
}
module.exports = huobipro_get_data;





