
const Binance = require('node-binance-api');

class BinanceTrader {
    constructor({access_key,secret_key}={}) {
        this.key = access_key;
        this.secret = secret_key;
        // this.productsData = require("./products.json");
        this.binance = new Binance().options({
            APIKEY: this.key,
            APISECRET: this.secret,
            useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
            test: false // If you want to use sandbox mode where orders are simulated
        });
    }
    getMyAccount(){
        let self =this;
        return new Promise((resolve, reject) => {
            self.binance.balance((error, balances) => {
                if (error) { reject(error) }
                let temp = [];
                let filtedData = self.filterObjValue(balances,obj=>{ return Number(obj.available)>0; });
                Object.keys(filtedData).forEach(key=>{
                    let acc={
                        currency:key,
                        balance:filtedData[key].available,
                        locked:filtedData[key].onOrder
                    }
                    temp.push(acc);
                })
                let account ={ Binance:temp }
                resolve(account)
            });
        })   
    }
    getBlance(){
        let self =this;
        return new Promise((resolve, reject) => {
        binance.balance((error, balances) => {
            console.log("balances()", balances);
            console.log("ETH balance: ", balances.ETH.available);
          });
        })
    }
    // getMyTrades(pair,limit,fromId) {
    //     let self = this;
    //     if (self.isEmpty(pair)) throw Error('getMyTrades: Invalid pair');
    //     // let pair = pair.toUpperCase()
    //     return new Promise((resolve, reject) => {
    //         self.binance.trades(pair.toUpperCase(),callback => {
    //             console.log
    //             if (error) { reject(error.body) }
    //             if (!self.isEmpty(trades)) {
    //                 let myTrades = trades.map(trade => {
    //                     return {
    //                         id: trade.id,
    //                         price: Number(trade.price),
    //                         volume: Number(trade.qty),
    //                         funds: Number(trade.price) * Number(trade.qty),
    //                         market: symbol.toLowerCase(),
    //                         created_at: trade.time,
    //                         commission: Number(trade.commission),
    //                         commissionAsset: trade.commissionAsset,
    //                         trend: null,
    //                         side: trade.isBuyer == true ? 'bid' : 'ask',
    //                         order_id: trade.orderId
    //                     }
    //                 })
    //                 resolve(myTrades)
    //             }
    //         },{limit, fromId});
    //     })
    // }

    getMyTrades(pair,callamount,id){
        let self = this;
        return new Promise((resolve, reject) => {
        self.binance.trades(pair.toUpperCase(), (error, trades, symbol) => {
            if (error) { reject(error.body) }
            resolve(trades);

          },{limit:callamount,fromId:id});
        });
    }
    
    getwithdrawHistory(date){
        console.log(date);
        let self = this;
        return new Promise((resolve, reject) => {
        self.binance.withdrawHistory((error, response) => {
            if (error) { reject(error.body) }
            resolve(response);
            console.log('response',response);
          },date);
        });
    }

    getDepositHistory(date){
        console.log(date);
        let self = this;
        return new Promise((resolve, reject) => {
        self.binance.depositHistory((error, response) => {
            if (error) { reject(error.body) }
            resolve(response);
            console.log('response',response);
          },date);
        });
    }


    getOrders(pair = {}) {
        let self = this
        return new Promise((resolve, reject) => {
            self.binance.openOrders(false, (error, openOrders) => {
                if (error) { reject(error) }
                if (openOrders.length > 0) {
                    let orders = openOrders.map(order => {
                        return {
                            id: order.orderId,
                            side: (order.side == 'BUY') ? 'bid' : 'ask',
                            ord_type: order.type.toLowerCase(),
                            price: Number(order.price),
                            avg_price: null,
                            state: order.status,
                            market: order.symbol.toLowerCase(),
                            created_at: order.time,
                            volume: Number(order.origQty),
                            remaining_volume: Number(order.origQty) - Number(order.executedQty),
                            executed_volume: Number(order.executedQty),
                            trades_count: null,
                        }
                    })
                    if(!self.isEmpty(pair)){
                        resolve(orders.filter(order=>{
                            return order.market == pair.name;
                        }))
                    }
                    resolve(orders)
                } else(resolve([]))
            });
        })
    }

    getOrderById(params = {}) {
        let self = this;
        if (self.isEmpty(params)) throw Error('getgetOrderById: Invalid order params');
        let pair = params.market.name.toUpperCase();
        let order_id = Number(params.id);
        return new Promise((resolve, reject) => {
            self.binance.orderStatus(pair, order_id, (error, orderStatus, symbol) => {
                if (error) { reject(error) }
                if (!self.isEmpty(orderStatus)) {
                    let order = {
                        id: orderStatus.orderId,
                        side: (orderStatus.side == 'BUY') ? 'bid' : 'ask',
                        ord_type: orderStatus.type.toLowerCase(),
                        price: Number(orderStatus.price),
                        avg_price: null,
                        state: orderStatus.status,
                        market: symbol.toLowerCase(),
                        created_at: orderStatus.time,
                        volume: Number(orderStatus.origQty),
                        remaining_volume: Number(orderStatus.origQty) - Number(orderStatus.executedQty),
                        executed_volume: Number(orderStatus.executedQty),
                        trades_count: null,
                        trades: []
                    }
                    resolve(order);
                } else { resolve([]);}
            });
        })

    }

    deleteOrder(params={}) {
        let self = this;
        if (self.isEmpty(params)) throw Error('deleteOrder: Invalid order id');
        let pair = params.market.name.toUpperCase();
        return new Promise((resolve, reject) => {
			self.binance.cancel(pair, params.id, (error, response, symbol) => {
                if (error) { reject(error.body)}
                self.getOrderById(params).then(data=>{
                    resolve(data)
                },err=>{reject(err)})

                // self.binance.orderStatus(symbol, params.id, (error, orderStatus, symbol) => {
                //     if (error) { reject(error)}
                //     let order = {
                //         id: orderStatus.orderId,
                //         side: (orderStatus.side == 'BUY') ? 'bid' : 'ask',
                //         ord_type: orderStatus.type.toLowerCase(),
                //         price: orderStatus.time,
                //         avg_price: null,
                //         state: orderStatus.status,
                //         market: symbol,
                //         created_at: orderStatus.time,
                //         volume: orderStatus.origQty,
                //         remaining_volume: Number(orderStatus.origQty) - Number(orderStatus.executedQty),
                //         executed_volume: Number(orderStatus.executedQty),
                //         trades_count: null,
                //         trades: null
                //     }
                //     resolve(order);
                // })         
              });
		})
    }


    placeOrder({  market, side, price, volume,type } = {}) {
        let self = this;
        let param = {
            side: (side == 'bid') ? 'BUY' : 'SELL',
            symbol: market.name.toUpperCase(),
            quantity: Number(volume),
            price: (type == "market") ? 0 : Number(price),
            flags: type.toUpperCase(),
        }
        return new Promise((resolve, reject) => {
            self.binance.order(param.side, param.symbol, param.quantity, param.price, { type: param.flags }, (error, response) => {
                if (error) { reject(error) }
                if (!self.isEmpty(response)) {
                    let order = {
                        id: response.orderId,
                        side: (response.side == 'SELL') ? 'ask' : 'bid',
                        ord_type: response.type.toLowerCase(),
                        price: response.price,
                        avg_price: null,
                        state: response.status,
                        market: response.symbol.toLowerCase(),
                        created_at: response.transactTime,
                        volume: response.origQty,
                        remaining_volume: Number(response.origQty) - Number(response.executedQty),
                        executed_volume: response.executedQty,
                        trades_count: null
                    }
                    resolve(order)
                }
            });
        })
    }
    
    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    filterObjValue(obj, matcher, opt) {
        if (!this.isObject(obj)) {
            throw Error(obj + 'Not an object type');
        }
        let result = {};
        let keys = Object.keys(obj);
        for(let k of keys) {
            if (matcher && matcher(obj[k])) {
                if (opt) { result[k] = opt(obj[k]); }
                else { result[k] = obj[k]; }
            }
        }
        return result
    }
    isObject(value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }
    
    errorHandle(err){
        
    }

}


module.exports = BinanceTrader