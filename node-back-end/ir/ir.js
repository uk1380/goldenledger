const IR = require('independentreserve');


class independentreserveTrader {
	constructor({access_key,secret_key} = {}) {
		this.API_KEY = access_key;
		this.PRIV_KEY = secret_key;
		this.market = this.instrument + this.currency;
		this.independentResv = new IR(this.API_KEY, this.PRIV_KEY);
		this.ACCOUNTS = [];
		this.TRADES = [];
	}

	getMyAccount() {
		let self = this;
		return new Promise((resolve, reject) => {
			let accounts = self.ACCOUNTS.map(d => {
				return {
					currency: self.checkSymbol(d.CurrencyCode),
					balance: d.TotalBalance,
					locked: d.TotalBalance - d.AvailableBalance
				}
			})
			
			self.independentResv.getAccounts((err, data) => {
				console.log(data);
				if(err){
					let message = ''
					if(err.name == 400){
						message = 'Bad Request.'
					}
					else{
						message = err.message;
					}
					reject('Independent Reserve getAccounts ' + err.name + ' ' + message);
				}
				else if(data && ! data.Message){
					let accounts = data.map(d => {
							return {
								currency: self.checkSymbol(d.CurrencyCode),
								balance: d.TotalBalance,
								locked: d.TotalBalance - d.AvailableBalance
							}
						})
					resolve(data);
				}
				
			})
		})
	}

	getMyTransactions(guid,fromTimestamp,txTypes){
		console.log(guid);
		console.log(fromTimestamp);
		console.log(txTypes);
		let self = this;
		let ress = [];
		return new Promise( (resolve, reject) => {
			
			let totalpage;
			let toTimestamp = new Date(1534402524000);
			self.independentResv.getTransactions(guid, fromTimestamp,toTimestamp,1,50,txTypes,async function(err, data){
				console.log(data.TotalItems);
				totalpage = data.TotalPages;
			
			console.log(totalpage);
			for (let i = 1; i <= totalpage; i++){
				await self.getOneTransaction(i,guid,fromTimestamp,txTypes).then(res =>{
					res.Data.forEach(element => {
						ress.push(element);
					});
				})
			}
			console.log(ress);
			resolve(ress);
			});
		});	
	}

	getOneTransaction(i,guid,fromTimestamp,txTypes){
		let toTimestamp = new Date(1534402524000);

		let self = this;
		return new Promise( (resolve, reject) => {
			self.independentResv.getTransactions(guid, fromTimestamp,toTimestamp,i,50,txTypes, function(err, data){
				if(err){
					let message = ''
					if(err.name == 400){
						message = 'Bad Request.'
					}
					else{
						message = err.message;
					}
					reject('Independent Reserve getAccounts ' + err.name + ' ' + message);
				}
			resolve(data);
		});
	});
}
	

	//Retrieves a page of a specified size, containing trades which were executed against your orders.
	getMyTrades() {
		let self = this;
		return new Promise((resolve, reject) => {
			self.independentResv.getTrades(1, 50, function(err, data){
				if(err){
					let message = ''
					if(err.name == 400){
						message = 'Bad Request.'
					}
					else{
						message = err.message;
					}
					reject('Independent Reserve getTrades ' + err.name + ' ' + message); 
				}
				else {
					// let market = self.checkPair(pair);
					// let temp = data.Data.filter(d => { 
						
					// 	return d.PrimaryCurrencyCode.toUpperCase() == market.instrument.toUpperCase() && d.SecondaryCurrencyCode.toUpperCase() == market.currency.toUpperCase();
					// });
					// let result = temp.map(t => {
					// 	let sidePattern = self.checkSidePattern(t.OrderType)
					// 	// return {
					// 	// 	id: t.TradeGuid,
					// 	// 	price: t.Price,
					// 	// 	volume: t.VolumeTraded,
					// 	// 	funds: (parseFloat(t.VolumeTraded) * parseFloat(t.Price)),
					// 	// 	market: self.checkSymbol((t.PrimaryCurrencyCode + t.SecondaryCurrencyCode)),
					// 	// 	created_at: t.OrderTimestampUtc,
					// 	// 	trend: null,
					// 	// 	side: sidePattern.side,
					// 	// 	type: sidePattern.type,
					// 	// 	order_id: t.OrderGuid
					// 	// }
					// 	let isBuyer = null;
					// 	if(sidePattern.side == "ask"){
					// 		isBuyer = false;
					// 	}else if(sidePattern.side == "bid"){
					// 		isBuyer = true;
					// 	}
					// 	return {
					// 		id: t.TradeGuid, 
					// 		price: t.Price, 
					// 		qty: t.VolumeTraded, 
					// 		time: (new Date(t.TradeTimestampUtc)).getTime(), 
					// 		orderId: t.OrderGuid, 
					// 		orderGuid: t.OrderGuid, 
					// 		isBuyer:isBuyer,
					// 		side: sidePattern.side,
					// 		commission: 0
					// 	}
					// })
					resolve(data);
				}
			})
		})
	}
	getMyFee(){
	// 	return new Promise((resolve, reject) => {
	// 		self.independentResv.GetBrokerageFees(1, 50, function(err, data){
	// 			if(err){
	// 				let message = ''
	// 				if(err.name == 400){
	// 					message = 'Bad Request.'
	// 				}
	// 				else{
	// 					message = err.message;
	// 				}
	// 				reject('Independent Reserve getTrades ' + err.name + ' ' + message); 
	// 			}
	// 			else {

	// }
}

	// Retrieves a page of a specified size, with your currently Open and Partially Filled orders.
	getOrders(pair = this.pair) {
		let self = this;
		let params = {
			primaryCurrencyCode: '',
			secondaryCurrencyCode: '',
		}
		if (!self.isEmpty(pair)) {
			let market = self.checkPair(pair);
			params.primaryCurrencyCode = market.instrument;
			params.secondaryCurrencyCode = market.currency;
		}
		return new Promise((resolve, reject) => {
			self.independentResv.getOpenOrders(params, primaryCurrencyCode, params.secondaryCurrencyCode, 1, 50, (data, err) => {
				let result = data.Data.map(d => {
					let sidePattern = checkSidePattern(d.OrderType)
					return {
						id: d.OrderGuid,
						side: sidePattern.side,
						ord_type: sidePattern.type,
						price: d.Price,
						avg_price: d.AvgPrice,
						state: d.Status,
						market: checkSymbol((d.PrimaryCurrencyCode + d.SecondaryCurrencyCode)),
						created_at: d.CreatedTimestampUtc,
						volume: d.Volume,
						remaining_volume: d.Outstanding,
						executed_volume: (parseFloat(d.Volume) - parseFloat(d.Outstanding)),
						trades_count: null
					}
				})
				resolve(result);
				if (err) { console.log(err);reject(err); };
			})
		})
	}
	// Retrieves details about a single order.
	getOrderById(id) {
		let self = this;
		if (!id) throw Error('getOrderById: Invalid order id');
		return new Promise((resolve, reject) => {
			self.independentResv.getOrderDetails(id, (err, data) => {
				if(err){
					let message = ''
					if(err.name == 400){
						message = 'Bad Request.'
					}
					else{
						message = err.message;
					}
					reject('Independent Reserve getOrderById ' + err.name + message); 
				}
				else if(data && !data.Message){
					console.log(data);
					let sidePattern = self.checkSidePattern(data.Type)
					let result = {
						id: data.OrderGuid,
						side: sidePattern.side,
						ord_type: sidePattern.type,
						price: data.Price,
						avg_price: data.AvgPrice,
						state: data.Status,
						market: self.checkSymbol((data.PrimaryCurrencyCode + data.SecondaryCurrencyCode)),
						created_at: data.CreatedTimestampUtc,
						volume: data.VolumeOrdered,
						remaining_volume: (parseFloat(data.VolumeOrdered) - parseFloat(data.VolumeFilled)),
						executed_volume: data.VolumeFilled,
						trades_count: null,
						trades: []
					}
					resolve(result);
				}
			})
		})
	}

	deleteOrder(id) {
		let self = this;
		if (!id) throw Error('getOrderById: Invalid order id');
		return new Promise((resolve, reject) => {
			self.independentResv.cancelOrder(id, (err, data) => {
				if(data && !data.Message){
					let sidePattern = self.checkSidePattern(data.Type)
					let result = {
						id: data.OrderGuid,
						side: sidePattern.side,
						ord_type: sidePattern.type,
						price: data.Price,
						avg_price: data.AvgPrice,
						state: data.Status,
						market: self.checkSymbol((data.PrimaryCurrencyCode + data.SecondaryCurrencyCode)),
						created_at: data.CreatedTimestampUtc,
						volume: data.VolumeOrdered,
						remaining_volume: (parseFloat(data.VolumeOrdered) - parseFloat(data.VolumeFilled)),
						executed_volume: data.VolumeFilled,
						trades_count: null,
						trades: []
					}
				
					resolve(result);
				}
				if (err) { console.log(err); reject(err); };
			})
		})
	}

	placeOrder({ market = undefined, side = undefined, price = undefined, volume = undefined, type = 'market' } = {}) {
		let self = this;
		let pattern = '';
		if (side.toLowerCase() == 'bid' || side.toLowerCase() == 'buy') {
			if (type.toLowerCase() == 'limit') { pattern = 'LimitBid' }
			else if (type.toLowerCase() == 'market') { pattern = 'MarketBid' }
		}
		else if (side.toLowerCase() == 'ask' || side.toLowerCase() == 'sell') {
			if (type.toLowerCase() == 'limit') { pattern = 'LimitOffer' }
			else if (type.toLowerCase() == 'market') { pattern = 'MarketOffer' }
		}
		let pair = self.checkPair(market);
		volume = Number(volume.toFixed(8));
		return new Promise((resolve, reject) => {
			self.independentResv.placeOrder(pair.instrument, pair.currency, pattern, price, volume, (err, data) => {
				if(err){
					let message = ''
					if(err.name == 400){
						message = 'Bad Request.'
					}
					else{
						message = err.message;
					}
					reject('Independent Reserve placeOrder ' + err.name + message); 
				}
				else if(data && !data.Message){
					// console.log(data);
					let sidePattern = self.checkSidePattern(data.Type)
					let result = {
						orderGuid: data.OrderGuid,
						// id: data.OrderGuid,
						side: sidePattern.side,
						ord_type: sidePattern.type,
						price: data.Price,
						avg_price: data.AvgPrice,
						state: data.Status,
						market: self.checkSymbol((data.PrimaryCurrencyCode + data.SecondaryCurrencyCode)),
						created_at: data.CreatedTimestampUtc,
						volume: data.VolumeOrdered,
						remaining_volume: (parseFloat(data.VolumeOrdered) - parseFloat(data.VolumeFilled)),
						executed_volume: data.VolumeFilled,
						trades_count: null,
						trades: []
					}
					resolve(result);
				}
			})
		})
	}

	isEmpty(obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key))
				return false;
		}
		return true;
	}

	checkSidePattern(data) {
		if (!data) { throw Error('Invalid side and pattern'); }
		let result = {};
		(data.includes('Limit')) ? result.type = 'limit' :result.type = 'market';
		(data.includes('Bid')) ? result.side = 'bid': result.side = 'ask';
		return result
	}
	
	checkPair(pair) {
		let instrument, currency;
		if(pair.instrument.toLowerCase() === 'btc'){
			instrument = 'Xbt';
		}else{
			instrument = pair.instrument;
		}
		if(pair.currency.toLowerCase() === 'btc'){
			currency = 'Xbt';
		}else{
			currency = pair.currency;
		}
		return {
			instrument: this.toTitleCase(instrument),
			currency: this.toTitleCase(currency)
		}
		
	}

	checkSymbol(symbol) {
		if (symbol.includes('Xbt')) {
			return symbol.replace('Xbt', 'btc').toLowerCase();
		}
		return symbol.toLowerCase();
	}

	jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	toTitleCase(str) {
		return str.replace(
			/\w\S*/g,
			function(txt) {
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			}
		);
	}


}
module.exports = independentreserveTrader;