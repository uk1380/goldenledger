const ACX = require('acx');
const symbols = require('./Acx/symbols.json');
const config = require('config');

const database_name = config.mongodb.database_name;
const url = config.mongodb.url;
const MongoClient = require('mongodb').MongoClient;

var last_deposit_id,
    last_withdraw_id,
    last_trade_id;
var name = config.acx.name;
var acx = config.acx;
const fee_rate = acx.fee_rate;


const acxTrader = new ACX({
    access_key: acx.access_key,
    secret_key: acx.secret_key
});

// console.log(config);
// acxTrader.getMyTrades({market: 'dashbtc', limit: 200}).then(data => {
//     // insertMongodb(data);
//     console.log(data);
// })

var totalhistory = [];
var flag = false;
class  acx_get_data{
    get_data(){
        get_last_id(name).then(data => {
            if (data != null) {
                last_deposit_id = data.last_deposit_id,
                    last_withdraw_id = data.last_withdraw_id,
                    last_trade_id = data.last_trade_id;
            } else {
                last_deposit_id = 0,
                last_withdraw_id = 0,
                last_trade_id = 0;
                return new Promise((resolve, reject) => {
                    // console.log("thrid steps");
                    MongoClient.connect(url, function (err, db) {
                        if (err) throw err;
                        var dbo = db.db(config.mongodb.db);
                        dbo.collection(config.mongodb.config_database).insert({
                            "exchange": name,
                            "last_deposit_id":0,
                            "last_withdraw_id":0,
                            "last_trade_id":0
                        }, function (err, result) {
                            if (err) throw err;
                            // console.log(result.name);
                            db.close();
            
                            resolve(result);
                        });
                    });
                });
            }
        }).then(async () => {
            await get_deposit_history();
        
        }).then(async () => {
            await get_withdraw_history();
        }).then(async() => {
            await get_trade_history().then(async (data) => {
                console.log(data);
                totalhistory.sort(compareID);
                while(flag == false){
                    flag = true;
                    totalhistory = custom_compare(totalhistory);
                }
        
                totalhistory.sort(compare);
                
                console.log('final data:' + totalhistory);
                if (totalhistory.length > 0) {
                    insertManyMongodb(totalhistory).then(console.log);
                    updateById({
                        "exchange": acx.name
                    }, {
                        '$set': {
                            "last_trade_id": last_trade_id + 1
                        }
                    });
                }
            });})
    }
    async get_trade_history() {

        return new Promise(async (resolve, reject) => {
    
            let new_last_id = last_trade_id;
            for (let i = 0; i < symbols.length; i++) {
                // p = p.then(async()=>{
                await get_history_until_null(symbols[i], new_last_id);
                // })
            }
            resolve('get history done');
        })
    
    }
    
    async get_history_until_null(symbol, id) {
        return new Promise(async (resolve, reject) => {
        console.log('get ' + symbol.symbol + ' start from ' + id);
        let each_last_id = id;
        await acxTrader.getMyTrades({
            market: symbol.symbol,
            order_by:'asc',
            limit: 200,
            from: id
        }).then(async data => {
            console.log(data);
    
            if (data) {
    
                if (data.length > 0) {
                    data.forEach(obj => {
                        if (obj.id > each_last_id) {
                            each_last_id = obj.id;
                        }
                        let new_Data = {};
                        new_Data.id = obj.id;
                        new_Data.date = new Date(obj['created_at']).valueOf();
                        new_Data.price = parseFloat(obj.price);
                        new_Data.exchange = acx.name;
    
                        if (obj.side == 'ask') {
                            new_Data.side = "ask";
                            new_Data.input_currency = symbol.currency;
                            new_Data.input_amount = parseFloat(obj['funds']) - fee_rate * parseFloat(obj['funds']);
                            new_Data.output_currency = symbol.instrument;
                            new_Data.output_amount = parseFloat(obj['volume']);
    
                        } else {
                            new_Data.side = "bid";
                            new_Data.input_currency = symbol.instrument;
                            new_Data.input_amount = parseFloat(obj['volume']) - fee_rate * parseFloat(obj['volume']);
                            new_Data.output_currency = symbol.currency;
                            new_Data.output_amount = parseFloat(obj['funds']);
                        }
    
                        new_Data.fee_currency = new_Data.input_currency;
                        new_Data.fee_amount = new_Data.input_amount * fee_rate /(1 - fee_rate);
    
                        totalhistory.push(new_Data);
    
                    });
                    await get_history_until_null(symbol, each_last_id);
                    resolve();
                } else {
                    console.log(symbol.symbol + ' finished');
                    if (last_trade_id < each_last_id) {
                        last_trade_id = each_last_id;
                    }
                    resolve();
                }
            }
        }).catch(async () => {
            await get_history_until_null(symbol, id);
            // resolve();
        })
    })
    }
    
    
    async get_deposit_history() {
        console.log('deposit start');
        await acxTrader.getDeposits({
            state: 'accepted',
            limit: 100
        }).then(async data => {
            console.log('deposit end');
    
            if (data.length > 0) {
                console.log('process deposit data');
                let rebulid_Data = data.filter(obj => {
                    return obj.id > last_deposit_id;
                }).map(obj => {
                    if (obj.id > last_deposit_id) {
                        last_deposit_id = obj.id;
                    }
                    let objtxId;                
                    if(obj.hasOwnProperty("txid")){
                         objtxId = obj.txid;
                    }
                    if(obj.hasOwnProperty("transaction_hash")){
                         objtxId = obj.transaction_hash;
                    }
                    let new_Data = {
                        id: obj.id,
                        date: new Date(obj['done_at']).valueOf(),
                        exchange: acx.name,
                        side: 'deposit',
                        input_currency: obj.currency,
                        txId: objtxId,
                        input_amount: parseFloat(obj.amount),
                        fee_amount: parseFloat(obj.fee),
                        fee_currency: obj.currency
                    }
                    return new_Data;
                    // }else {
                    //     return false;
                    // }
                })
    
                if (rebulid_Data.length > 0) {
                    rebulid_Data.forEach(obj => {
                        totalhistory.push(obj);
                    });
                    console.log('deposit: ' + rebulid_Data);
                    // await insertManyMongodb(rebulid_Data).then(console.log);
                    await updateById({
                        "exchange": "acx"
                    }, {
                        '$set': {
                            'last_deposit_id': last_deposit_id
                        }
                    });
                }
            }
        });
    
    }
    
    async get_withdraw_history() {
        console.log('withdraw start');
        await acxTrader.getWithdraws({
            state: 'done',
            limit:100
        }).then(async data => {
            console.log('withdraw end');
            if (data.length > 0) {
                console.log('process withdraw data');
                let rebulid_Data = data.filter(obj => {
                    return obj.id > last_withdraw_id;
                }).map(obj => {
    
                    if (obj.id > last_withdraw_id) {
                        last_withdraw_id = obj.id;
                    }
                    let objtxId;                
                    if(obj.hasOwnProperty("txid")){
                         objtxId = obj.txid;
                    }
                    if(obj.hasOwnProperty("transaction_hash")){
                         objtxId = obj.transaction_hash;
                    }
                    let new_Data = {
                        id: obj.id,
                        date: new Date(obj['created_at']).valueOf(),
                        exchange: acx.name,
                        side: 'withdraw',
                        output_currency: obj.currency,
                        txId: objtxId,
                        output_amount: parseFloat(obj.amount) + parseFloat(obj.fee),
                        fee_amount: parseFloat(obj.fee),
                        fee_currency: obj.currency
                    }
                    return new_Data;
                })
                if (rebulid_Data.length > 0) {
                    rebulid_Data.forEach(obj => {
                        totalhistory.push(obj);
                    });
                    console.log('withdraw: ' + rebulid_Data);
                    // await insertManyMongodb(rebulid_Data).then(console.log);
                    await updateById({
                        "exchange": "acx"
                    }, {
                        '$set': {
                            "last_withdraw_id": last_withdraw_id
                        }
                    }).then(console.log);
                }
    
            }
        });
    
    }
    
    get_last_id(exchange) {
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
    
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db(config.mongodb.db);
                dbo.collection(config.mongodb.config_database).findOne({
                    "exchange": exchange
                }, function (err, result) {
                    if (err) throw err;
                    // console.log(result.name);
                    db.close();
    
                    resolve(result);
                });
            });
        });
    }
    
    updateById(query, set) {
        console.log(query, set);
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db(config.mongodb.db);
                dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                    if (err) throw err;
                    console.log("1 document updated");
                    db.close();
                    resolve(res);
                });
            });
        })
    }
    
    insertManyMongodb(data) {
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");
    
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("mydb");
                dbo.collection(database_name).insertMany(data, function (err, res) {
                    if (err) throw err;
                    console.log("Number of documents inserted: " + res.insertedCount);
                    db.close();
                    resolve('done 1 time');
                });
            });
        });
    }
    
    compare(a, b) {
        console.log(a.id + " : " + b.id);
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
            if (a.date == b.date) { 
                flag = false;
                if (a.id < b.id) {
                    a.date -= 1;
                    return -1
                } else {
                    a.date += 1;
                    return 1;
                }
            }
        return 0;
    }
    
    compareID(a, b) {
        if (a.id < b.id)
            return -1;
        if (a.id > b.id)
            return 1;
        return 0;
    }
    
    custom_compare(data){
        for(let i =0; i<data.length-1;i++){
            try {
                if(data[i].side != 'deposit' && data[i].side != 'withdraw'){
                    if(data[i].date >= data[i+1].date){
                        flag = false;
                        if(data[i].id < data[i+1].id){
                            data[i].date -= 1;
                        }else{
                            data[i+1].date += 1;
                        }
                    }
                }
            }catch(err){
                console.log(err + data[i].id);
            }
        }
        return data;
    }
}
module.exports = acx_get_data;


