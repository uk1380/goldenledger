const binanceTrade = require('./binance/binance')
const config = require('config');
var MongoClient = require('mongodb').MongoClient;
const url = config.mongodb.url;
// const database_name = config.mongodb.database_name;
let symbols = require('./binance/symbol.json');
var binance = config.binance;
// var name = binance.name;


class binance_get_data {
    constructor(dbo, database, exchange, symbols) {
        this.symbols = symbols;
        this.database_name = database;
        this.name = exchange;
        this.binance = config.binance[exchange];
        this.bt = new binanceTrade({
            access_key: this.binance.access_key,
            secret_key: this.binance.secretkey
        });
        this.totalhistory = [];
        this.last_deposit_date;
        this.last_deposit_date;
        this.last_trade_id;
        this.dbo = dbo;
    }

    async get_data() {
        let self = this;
        console.log(self.symbols["0"].symbol);
        await self.get_last_id(self.name,self.symbols["0"].symbol).then(data => {
            if (data != null) {
                self.last_deposit_date = data.last_deposit_date;
                self.last_withdraw_date = data.last_withdraw_date;
                self.last_trade_id = data.last_trade_id;
            } else {
                self.last_deposit_date = 0;
                self.last_withdraw_date = 0;
                self.last_trade_id = 0;
                // return new Promise((resolve, reject) => {
                //     // console.log("thrid steps");
                //     self.dbo.collection(config.mongodb.config_database).insert({
                //         "exchange": self.name,
                //         "last_deposit_date": 0,
                //         "last_withdraw_date": 0,
                //         "last_trade_id": 0,
                //         "symbol": self.symbols["0"].symbol
                //     }, function (err, result) {
                //         if (err) throw err;
                //         // console.log(result.name);
                //         resolve(result);
                //     });
                // });
            }
        // }).then(async () => {
        //     await self.get_deposit_History().then(async data => {
        //         console.log(data);
        //     });
        //     await self.get_withdraw_History().then(async data => {
        //         console.log(data);
        //     });
        }).then(async () => {
            await self.get_trade_history().then(async (data) => {
                console.log(data);
                self.totalhistory.sort(self.compare);
                console.log('final data:' + self.totalhistory);
                if (self.totalhistory.length > 0) {
                    await self.insertManyMongodb(self.totalhistory).then(console.log);
                    await self.updateById({
                        "exchange": self.name,
                        "symbol": self.symbols["0"].symbol
                    }, {
                            '$set': {
                                "last_trade_id": self.last_trade_id,
                            }
                        });
                }
            });
        })
    }

    async get_trade_history() {

        return new Promise(async (resolve, reject) => {
            let new_last_id = this.last_trade_id;
            for (let i = 0; i < this.symbols.length; i++) {
                // p = p.then(async()=>{
                //gei symbol last trade id
                await this.get_trade_history_until_null(this.symbols[i], new_last_id);
                // })
            }
            resolve('get history done');
        })

    }

    get_trade_history_until_null(symbol, id) {
        var limit = 500;
        let self = this;
        return new Promise(async (resolve, reject) => {
            console.log('get ' + symbol.symbol + ' start from ' + id);
            let each_last_id = id;
            await this.bt.getMyTrades(symbol.symbol, limit, each_last_id + 1).then(async data => {
                if (data) {
                    if (data.length > 0) {
                        data.forEach(obj => {
                            console.log("each object is ")
                            console.log(obj);
                            let new_Data = {};
                            let fee_new_data = {};
                            if (each_last_id < obj.id) {
                                each_last_id = obj.id;
                            }
                            new_Data.id = obj.id;
                            new_Data.date = obj.time;
                            new_Data.price = parseFloat(obj.price);
                            new_Data.exchange = self.name;

                            if (obj.isBuyer == true) {
                                new_Data.side = "bid";
                                new_Data.input_currency = symbol.instrument;
                                new_Data.input_amount = parseFloat(obj.qty) - parseFloat(obj.commission);
                                new_Data.output_currency = symbol.currency;
                                new_Data.output_amount = parseFloat(obj.qty) * parseFloat(obj.price);

                            } else {
                                new_Data.side = "ask";
                                new_Data.input_currency = symbol.currency;
                                if (obj.commissionAsset.toLowerCase() == "usdt") {
                                    new_Data.input_amount = parseFloat(obj.qty) * parseFloat(obj.price) - parseFloat(obj.commission);
                                }
                                else {
                                    new_Data.input_amount = parseFloat(obj.qty) * parseFloat(obj.price);
                                }
                                new_Data.output_currency = symbol.instrument;
                                new_Data.output_amount = parseFloat(obj.qty);
                            }
                            if (obj.commissionAsset.toLowerCase() !== "bnb") {
                                new_Data.fee_currency = obj.commissionAsset.toLowerCase();
                                new_Data.fee_amount = parseFloat(obj.commission);
                            } else {
                                fee_new_data.output_currency = "bnb";
                                fee_new_data.output_amount = parseFloat(obj.commission);
                                fee_new_data.id = obj.id;
                                fee_new_data.date = obj.time;
                                fee_new_data.exchange = self.name;
                                fee_new_data.side = "fee";
                                fee_new_data.fee_currency = "bnb";
                                fee_new_data.fee_amount = 0;

                                new_Data.fee_currency = "bnb";
                                new_Data.fee_amount = 0;
                            }
                            console.log(fee_new_data);
                            this.totalhistory.push(new_Data);
                            if (!this.isEmpty(fee_new_data)) {
                                this.totalhistory.push(fee_new_data);
                            }
                        });
                        await this.get_trade_history_until_null(symbol, each_last_id);
                        resolve();
                    } else {
                        console.log(symbol.symbol + ' finished');
                        if (this.last_trade_id < each_last_id) {
                            this.last_trade_id = each_last_id;
                        }
                        resolve('done');
                    }
                }
            }).catch(async (err) => {
                console.log(err);
                console.log('re catch');
                await this.get_trade_history_until_null(symbol, id).then(console.log);
                resolve('re catch done');
            })
        })

    }

    async get_withdraw_History() {
        return new Promise(async (resolve, reject) => {
            await this.get_withdraw_History_until_null();

            resolve('get withdraw history done');
        })
    }

    get_withdraw_History_until_null() {
        // asset = this.symbols
        return new Promise(async (resolve, reject) => {
            console.log('get withdraw start from ' + this.last_withdraw_date);

            await this.bt.getwithdrawHistory(this.last_withdraw_date).then(async data => {
                if (data) {
                    if (data.withdrawList.length > 0) {
                        console.log(data);
                        data.withdrawList.forEach(obj => {
                            let new_Data = {};
                            if (obj.status == 6) {
                                console.log(obj);
                                if (this.last_withdraw_date < obj.applyTime) {
                                    this.last_withdraw_date = obj.applyTime;
                                }
                                new_Data.id = obj.id;
                                new_Data.date = obj.applyTime;
                                new_Data.exchange = self.name;
                                new_Data.txId = obj.txId;
                                new_Data.side = "withdraw";
                                new_Data.address = obj.address;
                                new_Data.output_currency = obj.asset.toLowerCase();
                                new_Data.output_amount = obj.amount;
                                new_Data.fee_currency = obj.asset.toLowerCase();
                                new_Data.fee_amount = 0;//??
                                new_Data.status = "Complete";

                            }
                            if (obj.status === 4 || obj.status === 2) {
                                console.log(obj);
                                if (this.last_withdraw_date < obj.applyTime) {
                                    this.last_withdraw_date = obj.applyTime;
                                }
                                new_Data.id = obj.id;
                                new_Data.date = obj.applyTime;
                                new_Data.exchange = self.name;
                                new_Data.txId = obj.txId;
                                new_Data.side = "withdraw";
                                new_Data.address = obj.address;
                                new_Data.output_currency = obj.asset.toLowerCase();
                                new_Data.output_amount = obj.amount;
                                new_Data.fee_currency = obj.asset.toLowerCase();
                                new_Data.fee_amount = 0;//??
                                new_Data.status = "Not complete"
                            }
                            console.log(new_Data);
                            this.totalhistory.push(new_Data);
                        });

                    }
                    resolve();
                }
            }).catch(async () => {
                console.log('re catch');
                await this.get_withdraw_History_until_null().then(console.log);
                resolve('re catch done');
            })
        })
    }

    async get_deposit_History() {
        return new Promise(async (resolve, reject) => {
            await this.get_deposit_History_until_null();

            resolve('get deposit history done');
        })
    }

    get_deposit_History_until_null(asset) {
        let self = this;
        asset = self.symbols["0"].instrument;
        
        return new Promise(async (resolve, reject) => {
            console.log('get deposit start from ' + self.last_deposit_date);
            console.log(asset);
            await this.bt.getDepositHistory(asset,self.last_deposit_date).then(async data => {
                if (data) {
                    if (data.depositList.length > 0) {
                        console.log(data);
                        data.depositList.forEach(obj => {
                            if (obj.status == 1) {
                                console.log(obj);
                                if (this.last_deposit_date < obj.insertTime) {
                                    this.last_deposit_date = obj.insertTime;
                                }
                                let new_Data = {};

                                new_Data.date = obj.insertTime;
                                new_Data.exchange = self.name;
                                new_Data.txId = obj.txId;
                                new_Data.side = "deposit";
                                new_Data.address = obj.address;
                                new_Data.input_currency = obj.asset.toLowerCase();
                                new_Data.input_amount = obj.amount;
                                new_Data.fee_currency = obj.asset.toLowerCase();
                                new_Data.fee_amount = 0;//??
                                console.log(new_Data);
                                this.totalhistory.push(new_Data);
                            }
                        });
                    }
                    resolve();
                }
            }).catch(async (err) => {
                console.log('re catch');
                console.log(err);
                await this.get_deposit_History_until_null().then(console.log);
                resolve('re catch done');
            })
        })
    }


    insertManyMongodb(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            self.dbo.collection(self.database_name).insertMany(data, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                resolve('done 1 time');
            });
        });
    }

    get_last_id(exchange,symbol) {
        let self = this;
        return new Promise((resolve, reject) => {
            // console.log("thrid steps");

            self.dbo.collection(config.mongodb.config_database).findOne({
                "exchange": exchange,"symbol":symbol
            }, function (err, result) {
                if (err) throw err;
                resolve(result);
            });
        });
    }

    updateById(query, set) {
        let self = this;
        console.log(query, set);
        return new Promise((resolve, reject) => {
            self.dbo.collection(config.mongodb.config_database).updateOne(query, set, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
                resolve(res);
            });
        });
    }


    compare(a, b) {
        if (a.date < b.date)
            return -1;
        if (a.date > b.date)
            return 1;
        if (a.date == b.date) {
            if (a.id < b.id) {
                a.date -= 1;
                return -1
            } else {
                a.date += 1;
                return 1;
            }
        }
        return 0;
    }

    isEmpty(obj) {
        for (var x in obj) { return false; }
        return true;
    }

}

module.exports = binance_get_data;





// var totalhistory = [];
// var last_trade_id,last_deposit_date,last_withdraw_date;



