const config = require('config');
var ObjectId = require('mongodb').ObjectID;
let baseCurrency = 'aud';
// let database_name = config.mongodb.database_name;
let database_name = config.mongodb.database_name;

collection = "testone";
// collection = "testone";

var MongoClient = require('mongodb').MongoClient;
var url = config.mongodb.url;

// let data = require('./data.json');
// console.log(data);
var dbo;
// launch();
// processs();

findduplicate();
depositWithdrawVerify();




function processs() {
    let allDb;
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        getAllDb().then(data=>{
            data.forEach(element => {
                if(element.hasOwnProperty("output_amount")){
                dbo.collection(collection).updateOne({"_id":element._id},{"$set":{"output_amount":parseFloat(element.output_amount)}});
                }
            });
        });
    })
}

function getAllDb() {
    return new Promise((resolve, reject) => {
        dbo.collection(collection).find().toArray(function (err, result) {
            if (err) throw err;
            // console.log(result);
            resolve(result);
        })
    })
}
function getDb(exchange) {
    return new Promise((resolve, reject) => {
        dbo.collection(collection).find({"exchange":exchange}).toArray(function (err, result) {
            if (err) throw err;
            // console.log(result);
            resolve(result);
        })
    })
}

async function findduplicate(){
    let allDb;
    // let exchanges = ["acx_liquidlty_1","acx_liquidity_2","binance","btcmarket","conversion","ir"];
    let noBM_exchanges = ["acx_acx_1","acx_acx_2","binance","conversion","ir","huobipro_acx_1","btcmarket_acx_1"];
    let exchanges = ["acx_huobi_1","huobipro_huobi_1","huobiau_huobi_1","huobiau_huobi_2","huobiau_huobi_3","btcmarket_huobi_1"];
    // let exchanges = ["huobiau_1","huobiau_2","huobiau_3","acx","btcmarket","huobipro","conversion"]
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        exchanges.forEach(ex=>{
            console.log(ex);
            getDb(ex).then(data=>{
                let dateArray = [];
                data.forEach(element => {
                    dateArray.push(element.date);
                });
                // console.log(dateArray);
                var sorted_arr = dateArray.sort(); // You can define the comparing function here. 
                                            // JS by default uses a crappy string compare.
                var results = [];
                for (var i = 0; i < dateArray.length - 1; i++) {
                    if (sorted_arr[i + 1] == sorted_arr[i]) {
                        results.push(sorted_arr[i]);
                    }
                }
                console.log(ex + " the duplicated date is shown below ")
                console.log(results);
    
            }).then( () => {getDb(ex).then(data =>{
                let idArray = [];
                data.forEach(element => {
                    if(!isNaN(element.id)){
                        idArray.push(element.id);
                    }
                });
                var sort_array = idArray.sort();
                var duplicateIds = [];
                for (var i = 0; i < idArray.length - 1; i++) {
                    if (sort_array[i + 1] == sort_array[i]) {
                        duplicateIds.push(sort_array[i]);
                    }
                }
                if (!isEmpty(duplicateIds)){
                    console.log("the duplicated Id is shown below");
                    console.log(duplicateIds); 
                }
                
                // var dupResult = [];
                duplicateIds.forEach(element => {
                    // console.log("each element with duplicated id is : ")
                    // console.log(element);
                    
                    exchanges.forEach(ex =>{
                    dbo.collection("testdata").find({"id":element,"exchange":ex}).toArray(function (err, result) {
                            if(!isEmpty(result)){
                                console.log("the number of duplicated data is " + result.length);
                                console.log(result); 
                            }
                    }); 
                    })
                });
            });
        });
    })

    });
}

function depositWithdrawVerify(){
    let allDb;
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        getAllDb().then(data=>{
            let depositSource;
            let depositDate;
            data.forEach(element =>{
                if (element.side == "deposit" && element.hasOwnProperty("source")){
                    depositSource = element.source;
                    depositDate = element.date;
                    data.forEach(elementone =>{
                        if(elementone.side === "withdraw"){
                            if(elementone.source == depositSource && elementone.date > depositDate){
                            console.log("this withdraw record is later than the deposit time");
                            console.log(elementone);
                        }
                        }

                        // else{
                        //     console.log("okok");
                        // }
                    });
                }
                // else{
                //     console.log("not match yet")
                // }
            });
        });
    });
}

function checkBCC(){
    let allDb;
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        getAllDb().then(data=>{
            data.forEach(element=>{
                if (element.input_currency.toLowerCase() === "bcc" || element.output_currency.toLowerCase() ==="bcc"){
                    console.log("The currency name should be changed to BCH")
                    console.log(element);
                }

            });
        });

    });
}

function deleteRecord(){
    let allDb;
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        dbo = db.db(config.mongodb.db);
        getAllDb().then(data=>{
            data.forEach(element=>{
                if (element.hasOwnProperty("status" && element.status === "Not complete")){
                    console.log("this record should be removed")
                    console.log(element);
                }
            });
        });

    });
}

function isEmpty(obj) { 
    for (var x in obj) { return false; }
    return true;
}

